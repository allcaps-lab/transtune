<?php

namespace Statamic\SiteHelpers;

use Statamic\Addons\Cars\Car;
use Statamic\Extend\Controller as AbstractController;
use Statamic\API\Entry;
use Illuminate\Http\Request;
use Statamic\API\Cache;

class CarController extends AbstractController
{
    private $matrix = [
        'chiptuning' => 'chiptuning_stages',
        'automaat-spoelen' => 'rinse_automatic_prices',
        'flush-transmission' => 'rinse_automatic_prices',
        'automaat-revisie' => 'transmission_revision',
        'transmission-revision' => 'transmission_revision',
        'koppelomvormer' => 'torque_convert',
        'torque-convert' => 'torque_convert',
        'mechatronic' => 'mechatronic',
        'walnutblasting' => 'walnutblasting',
        'stuurhuis-spoelen' => 'flush_steering',
        'flush-steering' => 'flush_steering'
    ];


    /**
     * Get all cars from the given type
     *
     * @return mixed
     */
    public function xml(Request $request)
    {
        $type = $request->get('type', 'chiptuning');
        $data = $this->data($type);
        $content = view('xml', [
            'xmlHeader' => '<?xml version="1.0" encoding="UTF-8"?>',
            'data' => $data
        ])->render();

        return response($content)->header('Content-Type', 'text/xml')   ;
    }
    /**
     * Get all cars from the given type
     *
     * @return mixed
     */
   public function json(Request $request)
   {
       $type = $request->get('type', 'chiptuning');
        return $this->data($type);
   }

    /**
     * Fast return from cache or else find the cars and  map them
     *
     * @param [type] $type
     * @return void
     */
   private function data($type)
   {
        if (Cache::has("json-cars-{$type}")) {
            return Cache::get("json-cars-{$type}");
        } else {
            $cars = Car::whereNotNull($this->matrix[$type])
                ->get(['model', 'brand_name', 'generation', 'type', 'slug'])
                ->map(function($model) use ($type) {
                    return [
                        'model' => $model->model,
                        'brand' =>$model->brand_name,
                        'generation' => $model->generation,
                        'type' => $model->type,
                        'url' => url("{$type}/cars/{$model->slug}")
                    ];
                })->sortBy(function($data) {
                    return ucfirst($data['brand']) . '-' . $data['model'] . '-' . $data['generation'] . '-' . $data['type'];
                })->values();

            Cache::put("json-cars-{$type}", $cars, 10080);
            return $cars;
        }
    }

    /**
     * Returns specific car by id
     *
     * @param Request $request
     * @return Car
     */
   public function get(Request $request)
   {
       return Car::findOrFail($request->query('id'));
   }
}
