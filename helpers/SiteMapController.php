<?php

namespace Statamic\SiteHelpers;

use Statamic\Addons\Cars\Car;
use Statamic\Extend\Controller as AbstractController;
use Statamic\API\Entry;
use Statamic\API\URL;
use Statamic\Addons\SeoPro\Settings as StatamicSettings;
use Statamic\API\Cache;

/**
 * Easy Sitemap controller
 * The public functions are mapped in routes.yaml
 */
class SiteMapController extends AbstractController
{
    /**
     * @var array
     */
    private $matrix = [
        'chiptuning' => 'chiptuning_stages',
        'automaat-spoelen' => 'rinse_automatic_prices',
        'flush-transmission' => 'rinse_automatic_prices',
        'automaat-revisie' => 'transmission_revision',
        'transmission-revision' => 'transmission_revision',
        'koppelomvormer' => 'torque_convert',
        'torque-convert' => 'torque_convert',
        'mechatronic' => 'mechatronic',
        'walnutblasting' => 'walnutblasting',
        'stuurhuis-spoelen' => 'flush_steering',
        'flush-steering' => 'flush_steering'
    ];

    /**
     * @param $data
     * @param $type
     * @return mixed
     * @throws \Throwable
     */
    protected function xml($data, $type)
    {
        if (Cache::has("sitemap-{$type}")) {
            $content = Cache::get("sitemap-{$type}");
        } else {
            $content = view('sitemap_xml', [
                'xmlHeader' => '<?xml version="1.0" encoding="UTF-8"?>',
                'data' => $data
            ])->render();
            Cache::put("sitemap-{$type}", $content, StatamicSettings::load()->get('sitemap_cache_length'));
        }

        return response($content)->header('Content-Type', 'text/xml')   ;
    }

    /*
     * Public methods can be found in the routes.yaml
     *
     *
     */

    public function chiptuning()
    {
        return $this->xml($this->data('chiptuning'), 'chiptuning');
    }

    public function automaatSpoelen()
    {
        return $this->xml($this->data('automaat-spoelen'), 'automaat-spoelen');
    }

    public function automaatRevisie()
    {
        return $this->xml($this->data('automaat-revisie'), 'automaat-revisie');
    }

    public function koppelomvormer()
    {
        return $this->xml($this->data('koppelomvormer'), 'koppelomvormer');
    }

    public function mechatronic()
    {
        return $this->xml($this->data('mechatronic'), 'mechatronic');
    }

    public function walnutblasting()
    {
        return $this->xml($this->data('walnutblasting'), 'walnutblasting');
    }

    public function stuurhuisSpoelen()
    {
        return $this->xml($this->data('stuurhuis-spoelen'), 'stuurhuis-spoelen');
    }

    /** Get the type of car */
    private function data($type)
    {
        $cars = Car::whereNotNull($this->matrix[$type])->get();

        $data = $this->getData($cars, ['brand_name'], $type);
        $data = $data->merge($this->getData($cars, ['brand_name', 'model'], $type));
        $data = $data->merge($this->getData($cars, ['brand_name', 'model', 'generation'], $type));
        $data = $data->merge($this->getData($cars, ['brand_name', 'model', 'generation', 'type'], $type));

        return $data->sort()->values();
    }

    private function getData($cars, array $which, $type)
    {
        return $cars->reduce(function($collection, $entry) use ($which, $type){
            $string = '';
            foreach ($which as $property) {
                $string .= slugify($entry->$property)."-";
            }
            $string = trim($string, "-");
            if (!$collection->has($string)) {
                $collection->put($string, [
                    'loc' => URL::makeAbsolute("/{$type}/cars/{$string}"),
                    'lastmod' => $entry->updated_at->format("Y-m-d"),
                    'changefreq' => 'weekly',
                    'priority' => 0.5
                ]);
            }

            return $collection;
        }, collect());
    }
}
