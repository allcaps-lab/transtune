<?php

return [
  'model' => "<h2>Stuurhuis spoelen <strong>:slug</strong></h2>
  Alle moderne auto's hebben stuurbekrachtiging, en dat geldt dus ook voor uw <strong>:slug</strong>. Problemen met de stuurbekrachtiging zijn herkenbaar door zwaar of onregelmatig sturen, maar het kan ook zijn dat de stuurhuis een raar geluid maakt. Kortom, alles dat anders is dan normaal kan duiden op een probleem in de stuurbekrachtiging.
  Heeft uw <strong>:slug</strong> ook deze problemen? Voorkom hoge kosten door dure reparaties! Veel problemen kunnen worden opgelost met stuurhuis spoelen. Dit houdt in dat we het complete hydraulische systeem doorspoelen en voorzien van nieuwe, originele olie van eersteklas kwaliteit.
  Met onze jarenlange kennis zijn we in staat snel de oorzaak van een probleem te vinden en een oplossing te vinden. Wij kunnen het stuurhuis van uw <strong>:slug</strong> spoelen.
  Wilt u hier meer over weten? Plan direct een afspraak online! U vindt ons in Deurne vlakbij Eindhoven. We verhelpen problemen met de stuurbekrachtiging van uw <strong>:slug</strong> voor klanten uit heel Nederland en België.",

  'generation' => "<h2>Stuurhuis spoelen <strong>:slug</strong></h2>
  Heeft uw <strong>:slug</strong> last van zwaar sturen? Maakt de stuurbekrachtiging vreemde geluiden? Dan is het tijd om het stuurhuis te spoelen.
  Problemen met de stuurbekrachtiging ontstaan vaak ongemerkt door slijtage van de verschillende onderdelen in het hydraulische systeem. Hierdoor raakt de hydraulische olie vervuild en verstoppen de verschillende openingen en kleppen van het systeem. Door het stuurhuis te spoelen van uw <strong>:slug</strong> verwijderen we deze verontreiniging en werkt de stuurbekrachtiging weer perfect. En u voorkomt hoge kosten door dure reparaties!
  Wil u ook het stuurhuis van uw <strong>:slug</strong> laten spoelen. Neem dan direct contact op met Transtune. Met onze jarenlange kennis zijn we in staat snel de oorzaak van een probleem te vinden en een oplossing te vinden.
  Wij kunnen het stuurhuis van uw <strong>:slug</strong> spoelen. Wilt u hier meer over weten? Plan direct een afspraak online! U vindt ons in Deurne vlakbij Eindhoven. We verhelpen problemen met de stuurbekrachtiging voor klanten uit heel Nederland en België.",

  "type" => "<h2>Stuurhuis spoelen <strong>:slug</strong></h2>
  <p>Geen enkele stuurbekrachtiging heeft geheimen voor de specialisten van Transtune. Laat daarom het stuurhuis van uw <strong>:slug</strong> spoelen bij Transtune! Met onze jarenlange technische kennis en professionele werkplaats helpen wij u zo snel mogelijk weer op weg bij problemen met het stuurhuis.</p>
  <p>
  Reparaties aan het stuurhuis kan hoge kosten met zich meebrengen. U kunt deze kosten voorkomen door het stuurhuis van uw <strong>:slug</strong> te laten spoelen bij Transtune. Dit houdt in dat we het complete hydraulische systeem doorspoelen en voorzien van nieuwe, originele olie van eersteklas kwaliteit.
  De autospecialisten van Transtune vinden snel de oorzaak van een probleem en hebben altijd een oplossing. Wij kunnen het stuurhuis van uw <strong>:slug</strong> spoelen. Wilt u hier meer over weten? Plan direct een afspraak online! U vindt ons in Deurne vlakbij Eindhoven. We verhelpen problemen met de stuurbekrachtiging voor klanten uit heel Nederland en België.</p>",
  "slug" => "<h2>Stuurhuis spoelen <strong>:brand :model :generation :type</strong> </h2>
  De stuurbekrachtiging  is een complex systeem met een stuurhuis, stuurpomp, koeler en  drukleidingen. Het systeem is gevuld met hydraulische olie die onder druk de stuurbeweging overbrengt op de voorwielen. Bij Transtune kunnen wij het stuurhuis van uw <strong>:brand :model :generation :type</strong>  spoelen.
  Problemen met de stuurbekrachtiging ontstaan vaak ongemerkt door slijtage van de verschillende onderdelen in het hydraulische systeem. Hierdoor raakt de hydraulische olie vervuild en verstoppen de verschillende openingen en kleppen van het systeem. Door het stuurhuis te spoelen verwijderen we deze verontreiniging en werkt de stuurbekrachtiging van uw <strong>:brand :model :generation :type</strong>  weer perfect!
  Met stuurhuis spoelen voorkomt u reparaties aan het stuurhuis en voorkomt u hoge kosten voor uw <strong>:brand :model :generation :type</strong> . De stuurbekrachtiging doorspoelen betekent dat we het complete hydraulische systeem spoelen en voorzien van nieuwe, originele olie van eersteklas kwaliteit.
  Wilt u hier meer over weten? Plan direct een afspraak online! U vindt ons in Deurne vlakbij Eindhoven. We verhelpen problemen met de stuurbekrachtiging voor klanten uit heel Nederland en België.
  "
];