<?php

return [
  'model' => '<h2>Automaat revisie voor <strong>:slug</strong> </h2>
  Heeft u een <strong>:slug</strong>? Bent u op zoek naar het beste adres voor een automaat revisie? Neem dan contact op met Transtune in Deurne! Wij zijn dé revisiespecialist van Brabant en omstreken. Onze klanten komen uit heel Nederland en zelfs uit België om de automatische transmissie van hun <strong>:slug</strong> door ons te laten reviseren.
  Met Transtune kiest u voor kwaliteit en betrouwbaarheid. Een revisie van de automatische versnellingsbak van uw <strong>:slug</strong> betekent dat deze als nieuw onze werkplaats verlaat. We vervangen het complete binnenwerk en voorzien de versnellingsbak na revisie van originele olie. Niet voor niets krijgt u bij Transtune twee jaar garantie op een revisie. Veel langer dus dan de één jaar garantie die u bij veel andere aanbieders krijgt!
  Transtune heeft meer dan 30 jaar ervaring met het reviseren van automatische versnellingsbakken, ook van <strong>:slug</strong>. We reviseren ongeveer 350 automaatbakken per jaar. Revisie gebeurt altijd in eigen beheer, in onze speciaal uitgeruste werkplaats.
  Wilt u de automaat van uw <strong>:slug</strong> laten reviseren? Plan direct een afspraak online! U vindt ons in Deurne vlakbij Eindhoven. We verhelpen transmissieproblemen voor klanten uit heel Nederland en België.',

  'generation' => "<h2>Automaat revisie voor <strong>:slug</strong></h2>
  Voor een automaat revisie voor uw <strong>:slug</strong> gaat u naar Transtune in Deurne. Bij Transtune beschikken we over topkwaliteit apparatuur voor diagnose en revisie, speciaal voor uw <strong>:slug</strong>
  Voor elk fabrikaat automatische versnellingsbak hebben we de juiste oplossing. We durven zelfs zo ver te gaan dat we kwaliteit leveren boven dealerniveau! Onze specialisten reviseren ongeveer 350 automatische transmissies per jaar en hebben samen meer dan 30 jaar ervaring in het reviseren van versnellingsbakken, waaronder de <strong>:slug</strong>
  Bij Transtune staan we voor betaalbare kwaliteit. Dit betekent dat we altijd eerst kosteloos een diagnose uitvoeren van de versnellingsbak van uw <strong>:slug</strong>. Een goede diagnose is namelijk het halve werk en kan u veel geld besparen door onnodige reparaties te voorkomen. Zo kunnen wij de automaat ook spoelen in plaats van reviseren, of een software-update uitvoeren voor een soepeler schakelgedrag. Voor specifieke transmissies hebben we hiervoor zelfs onze eigen software ontwikkeld!
  Wilt u de automaat van uw <strong>:slug</strong> laten reviseren? Plan direct een afspraak online! U vindt ons in Deurne vlakbij Eindhoven. We verhelpen transmissieproblemen voor klanten uit heel Nederland en België.

  ",

  "type" => "<h2>Automaat revisie voor <strong>:slug</strong> </h2>
  Automaat kapot? Wanneer de automatische transmissie van uw <strong>:slug</strong> slecht schakelt dan is het wellicht tijd voor een revisie. Bij Transtune kunnen wij dat voor u verzorgen, tegen betaalbare kosten en van de allerbeste kwaliteit!
  Een automaat revisie voor uw <strong>:slug</strong> pakken we bij Transtune grondig aan. We demonteren de volledige versnellingsbak en vervangen versleten onderdelen. In totaal zijn er wel 400 onderdelen in de automatische transmissie van uw <strong>:slug</strong>. Ze worden allemaal geïnspecteerd en vervangen, of we doen een upgrade met bijvoorbeeld onderdelen van Sonnax. Ook worden alle onderdelen gewassen en gestraald voordat we de complete transmissie  weer monteren
  Na revisie is de versnellingsbak van uw <strong>:slug</strong> weer in volledige nieuwstaat. U ontvangt dan ook een bijbehorend garantiebewijs voor 2 jaar, want bij Transtune staan we volledig achter ons product!
  Wilt u de automaat van uw <strong>:slug</strong> laten reviseren? Plan direct een afspraak online! U vindt ons in Deurne vlakbij Eindhoven. We verhelpen transmissieproblemen voor klanten uit heel Nederland en België.
  ",
  "slug" => "<h2>Automaat revisie voor <strong>:brand</strong> <strong>:model</strong> <strong>:generation</strong> <strong>:type</strong></h2>
  Bij Transtune hebben we veel ervaring met het reviseren van <strong>:brand</strong> versnellingsbakken, en ook voor de <strong>:model</strong> met de <strong>:type</strong> motor hebben wij de juiste oplossing!
  De combinatie van automatische transmissie en motor brengt altijd specifieke uitdagingen met zich mee. Zeker wanneer uw <strong>:brand</strong> is voorzien van een sterke benzine- of dieselmotor kan het gebeuren dat de versnellingsbak eerder slijt, vooral bij zware belading en een sportieve rijstijl van de bestuurder. En ook wat betreft het modeljaar zoals de <strong>:generation</strong> geldt hetzelfde. Een specifiek modeljaar en motor kan herkenbare problemen met de versnellingsbak geven.
  Gelukkig weten we bij Transtune precies welke problemen er bij welke combinaties optreden, en hoe wij ze kunnen verhelpen! Transtune is namelijk dé revisiespecialist voor <strong>:brand</strong>. U kunt dus zeker ook bij ons terecht voor een revisie van de automatische transmissie van uw  <strong>:model</strong> <strong>:generation</strong> met de <strong>:type</strong> motor
  Wilt u de automaat van uw <strong>:brand</strong> <strong>:generation</strong> <strong>:type</strong> laten reviseren? Plan direct een afspraak online! U vindt ons in Deurne vlakbij Eindhoven. We verhelpen transmissieproblemen voor klanten uit heel Nederland en België.
  "
];