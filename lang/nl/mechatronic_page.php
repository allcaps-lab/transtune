<?php

return [
  'model' => '<h2>Mechatronic vervangen <strong>:slug</strong></h2>
  De Mechatronic is de elektronische regelunit van DSG versnellingsbakken (ook wel bekend als S-tronic, DCT, PDK en Powershift). Ook vinden we Mechatronics in veel automatische transmissies met een koppelomvormer zoals die van ZF, Aisin AW, GM en Jatco.
  De Mechatronic is het brein van de automatische transmissie, en zorgt ervoor dat elke versnelling altijd op het juiste moment beschikbaar is. Maar wat als er iets misgaat met de Mechatronic van uw <strong>:slug</strong>? Dan is het tijd om contact op te nemen met Transtune!
  De specialisten van Transtune kunnen de Mechatronic van uw <strong>:slug</strong> vervangen voor een geheel nieuw exemplaar. Ook is het mogelijk het systeem te reviseren, te modificeren of te voorzien van nieuwe software.
  Afhankelijk van het probleem adviseren wij u over de beste oplossing. Een Mechatronic is namelijk een ingewikkeld stuk techniek met sensoren, relais en actuatoren. In combinatie met hydraulische oliedruk en aangestuurd door een computer (de Transmission Control Unit) zorgen zij ervoor dat de transmissie van uw <strong>:slug</strong> goed functioneert.
  Wilt u de Mechatronic van uw <strong>:slug</strong> laten vervangen, reviseren of repareren? Plan direct een afspraak online! U vindt ons in Deurne vlakbij Eindhoven. We verhelpen transmissieproblemen voor klanten uit heel Nederland en België.',

  'generation' => "<h2>Mechatronic vervangen <strong>:slug</strong></h2>
  Gebruikt uw <strong>:slug</strong> auto nog maar een paar versnellingen? Geeft de auto foutcodes? Dan zijn er wellicht problemen met de Mechatronic.
  De Mechatronic is de centrale regelunit van een DSG automatische versnellingsbak. Deze transmissie beschikt over 2 koppelingen en is vooral bekend geworden door de toepassing ervan in Duitse automerken als Volkswagen, BMW en Mercedes-Benz. Daarnaast worden veel automaatbakken met een koppelomvormer, zoals die van ZF, Aisin AW, GM en Jatco, aangestuurd door een Mechatronic. Ook uw <strong>:slug</strong> kan dus voorzien zijn van een Mechatronic.
  In principe is een Mechatronic zeer betrouwbaar, maar er kunnen zeker ook problemen ontstaan. Dat kunnen elektronische, softwarematige problemen zijn maar ook de hardware kan het begeven. De specialisten van Transtune kunnen de Mechatronic van uw <strong>:slug</strong> vervangen door een nieuw exemplaar. Daarnaast is het mogelijk het systeem te repareren, reviseren of te voorzien van nieuwe software. Afhankelijk van het probleem zullen wij de meest kostengunstige oplossing voorstellen.
  Wilt u de Mechatronic van uw <strong>:slug</strong> laten vervangen, repareren of reviseren? Plan direct een afspraak online! U vindt ons in Deurne vlakbij Eindhoven. We verhelpen transmissieproblemen voor klanten uit heel Nederland en België.
",

  "type" => "<h2>Mechatronic vervangen <strong>:slug</strong></h2>
  Is uw Mechatronic kapot? Schakelt de transmissie van uw <strong>:slug</strong> niet goed meer. Neem voor het vervangen contact op met Transtune in Deurne. Wij zijn dé transmissiespecialist van Nederland!
  Bij Transtune kunnen wij de Mechatronic van uw <strong>:slug</strong> vervangen. Bij vervangen van de Mechatronic monteren wij een nieuw exemplaar. Ook is het mogelijk de Mechatronic te reviseren, een deelreparatie uit te voeren of een software-update uit te voeren zodat uw <strong>:slug</strong> weer schakelt als vanouds. Hierbij kiezen wij altijd voor de meest efficiënte en kostengunstige oplossing.
  Wilt u de Mechatronic van uw <strong>:slug</strong> laten vervangen? Wilt u meer weten over Mechatronic reviseren of repareren? Plan direct een afspraak online! U vindt ons in Deurne vlakbij Eindhoven. We verhelpen transmissieproblemen voor klanten uit heel Nederland en België.
  ",
  "slug" => "<h2>Mechatronic vervangen <strong>:brand</strong> <strong>:model</strong> <strong>:generation</strong> <strong>:type</strong></h2>
  De Mechatronic is de regeleenheid die de DSG transmissie van uw <strong>:brand :model :generation :type</strong> aanstuurt (ook wel bekend als S-tronic, DCT, PDK en Powershift). Ook vinden we Mechatronics in veel automatische transmissies met een koppelomvormer zoals die van ZF, Aisin AW, GM en Jatco.
  Het systeem bestaat simpel gezegd uit een behuizing met een aantal sensoren, relais en actuatoren die door een centrale computer worden aangestuurd om met behulp van hydraulische druk de versnellingsbak aan te sturen.
  Een Mechatronic is in principe betrouwbaar, maar er zijn ook problemen bekend waardoor het systeem vervangen of gerepareerd moet worden. Daarnaast kan de software voor storingen zorgen
  Heeft uw <strong>:brand :model :generation :type</strong> ook problemen met de Mechatronic? Wilt u de Mechatronic van uw <strong>:brand :model :generation :type</strong> laten vervangen? Wilt u meer weten over reviseren, repareren of een software-update laten uitvoeren?
  Plan direct een afspraak online! U vindt ons in Deurne vlakbij Eindhoven. We verhelpen transmissieproblemen voor klanten uit heel Nederland en België.
  "
];