<?php

return [
  'model' => '<h2>Chiptuning <strong>:slug</strong> </h2>
  Transtune is dé specialist voor chiptuning van uw <strong><strong>:slug</strong>.</strong> We staan voor 100 procent kwaliteit en betrouwbaarheid. Bij ons krijgt het motormanagement van uw auto geen standaard softwarepakketje  van de plank maar op maat geschreven programmatuur die perfect past bij uw <strong><strong>:slug</strong>.</strong></br>
  Kiest u ook voor betrouwbare zekerheid? Laat uw <strong><strong>:slug</strong></strong> daarom tunen door Transtune in Brabant. U kunt bij ons terecht voor chiptuning van elke <strong><strong>:slug</strong></strong> met benzinemotor of diesel. Uw auto laten chippen door onze experts biedt vele voordelen: meer souplesse, meer vermogen, een veiliger rijgedrag en desgewenst een lager brandstofgebruik met eco tuning.</br>
  Gaat u voor  meer vermogen uit uw <strong><strong>:slug</strong>?</strong> Met onze chiptuning op maat is een toename haalbaar van 20 tot 40 procent, zowel voor een benzine auto als een diesel. Na chiptuning rijdt uw <strong><strong>:slug</strong></strong> aanmerkelijk soepeler en gretiger!
  Met chiptuning kan je ook zuiniger rijden. Dat is eco chiptuning waarbij we uw <strong><strong>:slug</strong></strong> zo programmeren dat deze het meest efficiënt omgaat met brandstof. Bij een normale rijstijl is een brandstofbesparing mogelijk tussen 5 en 20 procent!</br>
  Meer weten? Bel of mail ons. </br> Of maak direct online een afspraak voor chiptuning van uw <strong><strong>:slug</strong></strong>',

  'generation' => "<h2>Chiptuning <strong>:slug</strong></h2>
  Een <strong>:slug</strong> is een auto om trots op te zijn. Maar het kan natuurlijk altijd beter! Heeft u er wel eens aan gedacht om uw <strong>:slug</strong> te chiptunen? Bij een betrouwbaar adres waar je altijd netjes wordt geholpen? Bij Transtune in Deurne bent u verzekerd van de beste service en de beste kwaliteit voor het chiptunen van uw <strong>:slug</strong>.<br/>
  We zijn gespecialiseerd in het chippen van alle Europese merken (met uitzondering van de Franse merken). Ook voor uw <strong>:slug</strong> hebben we dus de beste oplossing paraat. Het chiptunen van de auto gebeurt  niet met standaard software. Nee, bij ons krijgt u chiptuning op maat. Speciaal geschreven software die perfect past bij uw <strong>:slug</strong> en die de benzinemotor of diesel beter laat presteren.<br/>
  Met onze chiptuning op maat is het mogelijk om 20 tot 40 procent meer vermogen uit de motor te halen. Dit geldt voor benzine auto's én diesels. Na chiptuning rijdt uw <strong>:slug</strong> aanmerkelijk soepeler en reageert deze spontaner op het gaspedaal.<br/>
  En er zijn meer voordelen. Wie juist zuiniger wil rijden kan ook kiezen voor eco chiptuning. Met eco chiptuning programmeren we uw <strong>:slug</strong>  zo dat deze het meest efficiënt omgaat met brandstof. Bij een normale rijstijl is een brandstofbesparing mogelijk tussen 5 en 20 procent!<br/>
  Meer weten? Bel of mail ons. Of maak direct online een afspraak voor chiptuning van uw <strong>:slug</strong>
  ",

  "type" => "<h2>Chiptuning <strong>:slug</strong> </h2>
  Bent u op zoek naar een betrouwbaar adres voor chiptuning van uw <strong>:slug</strong>? Bij Transtune in Deurne zijn we gespecialiseerd in chiptuning van alle Europese automerken (uitgezonderd Frans). Dat geldt voor alle modellen en vrijwel alle generaties. Dus ook wanneer uw auto al op leeftijd is en u wilt deze chiptunen dan is dat vaak mogelijk!

  Bij chiptuning draait alles om software. Het optimaliseren van de software zorgt ervoor dat de benzine motor of diesel van uw <strong>:slug</strong> soepeler presteert en meer vermogen levert. Dat doen we met chiptuning op maat. Bij Transtune geloven we niet in standaardsoftware. We geloven wél in het schrijven van programmatuur speciaal voor uw <strong>:slug</strong>. Het resultaat is een auto die beter rijdt, en dat veilig binnen de voorschriften van de autofabrikant.
  Uw <strong>:slug</strong> laten chippen levert tussen 20 en 40 procent meer motorvermogen op. Ook is het mogelijk te kiezen voor eco tuning. Deze vorm van chiptuning maakt uw auto bij een normale rijstijl 5 tot 20 procent zuiniger!
  Meer weten over de voordelen van chiptuning? Bel of mail ons. Of maak direct online een afspraak voor chiptuning van uw <strong>:slug</strong>
  ",
  "slug" => "<h2>Automotoren hebben geen geheimen voor de tuningspecialisten van Transtune! </h2>
  Transtune levert altijd chiptuning op maat, met zelf ontwikkelde software. Ook de <strong>:slug</strong> motor van uw <strong>:slug</strong> kunnen wij dus tunen!
  Met Transtune krijgt u betrouwbare kwaliteit. Daarom kiezen we ook niet voor standaardsoftware: tuning van een benzine auto of diesel vergt specifieke merkenkennis. Bij Transtune weten we precies welke diesel of benzinemotor er onder de motorkap van uw <strong>:slug</strong> ligt, en welke programmatuur daar het beste bij past.
  Na het tunen van de <strong>:slug</strong> motor kunt zult u merken dat deze meer souplesse biedt, meer trekkracht en meer vermogen. Een vermogenswinst tussen 20 en 40 procent is mogelijk. Maar wat we belangrijker vinden is dat uw <strong>:slug</strong> na het tunen gewoon veel lekkerder rijdt! En u weet zeker dat het veilig is gebeurd. We respecteren de begrenzingen van de <strong>:slug</strong> motor én u krijgt na het tunen van uw <strong>:slug</strong> een levenslange softwaregarantie.
  Meer weten? Bel of mail ons. Of maak direct online een afspraak voor chiptuning van uw <strong>:slug</strong>
  "
];