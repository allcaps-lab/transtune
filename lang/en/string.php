<?php
return [
  'contact' => [
    'title' => 'Contact us',
    'name' => 'Name | Your Name',
    'email' => 'Email | Your Email',
    'subject' => 'Subject | Your Subject',
    'message' => 'message | Enter your message',
    'success' => 'Form was submitted successfully.',
    'send' => 'Send form',
    'chiptuning' => "Give us your brand and model and we'll keep you informed",
    'brand_model' => 'Your brand and model.'
  ],
  'page_not_found' => "Page not found",
  'back' => 'Back',
  'choice' => 'Choose your',
  'chiptuning' => 'Chiptuning | Chippen',
  'automaat-spoelen' => 'Flush Transmission | Flushing',
  'automaat-revisie' => 'Revise Transmission | Revising',
  'koppelomvormer' => 'Torque converter | Convert Torque',
  'mechatronic' => 'Mechatronic | Read Mechatronic',
  'stuurhuis-spoelen' => 'Flush steering wheel | Flushing',
  'walnutblasting' => 'Walnutblasting | Blasting',
];