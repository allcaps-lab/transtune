# Transtune Statamice Site Content

## Installation
1. Install Statamic and replace the site folder with this folder

2. Install database with the .sql

3. fill `.env ` with credentials
  ```
APP_ENV=
DB_HOST=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
  ```

4. For automatic updating of Carsys activities run the `CarsysTasks` (see [here](https://docs.statamic.com/addons/classes/tasks#starting) how)

## Development
1. Go to the `themes/transtune` folder and run `yarn`
2. Then execute `yarn run watch`

## Custom plugins
* Cars
* Carsys

## Carsys
The carsys plugin is a simple wrapper around the Carsys api. A place where car companies can keep an account of their schedules and the activities they provide.

It has 3 endpoints:
 1. `GET /!/carsys/index`: returns the availability of the garage for the coming weeks
 2. `GET /!/carsys/replacements`: returns the available replacements options a garage provides
 3. `POST /!/carsys/appointment`: Make a new appointment with the garage

 ## Cars
 The cars plugins defines the `Car` model (Eloquent) and the tag to retrieve those models