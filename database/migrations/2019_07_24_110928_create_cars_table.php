<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Statamic\API\Entry;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model');
            $table->string('generation');
            $table->string('brand_name');
            $table->string('type');
            $table->string('slug')->unique();
            $table->json('chiptuning_stages')->nullable();
            $table->integer('chiptuning_hp_before')->nullable();
            $table->integer('chiptuning_torque_before')->nullable();
            $table->json('chiptuning_prices')->nullable();
            $table->json('rinse_automatic_prices')->nullable();
            $table->json('transmission_revision')->nullable();
            $table->json('torque_convert')->nullable();
            $table->json('mechatronic')->nullable();
            $table->json('walnutblasting')->nullable();
            $table->json('flush_steering')->nullable();
            $table->timestamps();
        });

        Entry::whereCollection('cars')->sortBy('brand')->each(function($entry) {
            $data = $entry->data();
            unset($data['id']);
            $data['brand_name'] = $data['brand'];

            $brand = \Statamic\API\Term::create(slugify($data['brand']))->taxonomy('brand')->get();
            $brand->set('title', $data['brand']);
            $brand->save();

            $car = \Statamic\Addons\Cars\Car::create($data);

            $car->saveOrFail();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
