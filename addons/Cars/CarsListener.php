<?php

namespace Statamic\Addons\Cars;

use Statamic\API\Nav;
use Statamic\Extend\Listener;

class CarsListener extends Listener
{
    /**
     * The events to be listened for, and the methods to call.
     *
     * @var array
     */
    public $events = [
        'cp.nav.created' => 'add'
    ];

    /**
     * Add Nav item to admin panel
     *
     */
    public function add($nav)
    {
        $nav->addTo(
            'content',
            Nav::item('Cars')
                ->route('cars.index')
                ->icon('cog')
        );
    }
}
