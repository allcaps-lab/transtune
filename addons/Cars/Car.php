<?php

namespace Statamic\Addons\Cars;

use Illuminate\Database\Eloquent\Model;
use Statamic\API\Taxonomy;
use Statamic\API\Term;

class Car extends Model
{
    protected $fillable = [
        'model',
        'brand_name',
        'generation',
        'type',
        'slug',
        'chiptuning_stages',
        'chiptuning_hp_before',
        'chiptuning_torque_before',
        'chiptuning_prices',
        'rinse_automatic_prices',
        'transmission_revision',
        'torque_convert',
        'mechatronic',
        'walnutblasting',
        'flush_steering',
    ];

    protected $casts = [
        'chiptuning_stages' => 'array',
        'chiptuning_prices' => 'array',
        'rinse_automatic_prices' => 'array',
        'transmission_revision' => 'array',
        'torque_convert' => 'array',
        'mechatronic' => 'array',
        'walnutblasting' => 'array',
        'flush_steering' => 'array',
    ];

    protected $appends = ['brand'];

    /**
     * Return the brand of a Car
     *
     * @return Term
     */
    public function getBrandAttribute()
    {
        return Term::whereSlug(slugify($this->brand_name), 'brand');
    }
}