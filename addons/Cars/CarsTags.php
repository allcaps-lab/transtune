<?php

namespace Statamic\Addons\Cars;

use Statamic\Extend\Tags;
use Statamic\Addons\Cars\Car;
use Statamic\API\Str;

class CarsTags extends Tags
{
    private $matrix = [
        'chiptuning' => 'chiptuning_stages',
        'automaat-spoelen' => 'rinse_automatic_prices',
        'flush-transmission' => 'rinse_automatic_prices',
        'automaat-revisie' => 'transmission_revision',
        'transmission-revision' => 'transmission_revision',
        'koppelomvormer' => 'torque_convert',
        'torque-convert' => 'torque_convert',
        'mechatronic' => 'mechatronic',
        'walnutblasting' => 'walnutblasting',
        'stuurhuis-spoelen' => 'flush_steering',
        'flush-steering' => 'flush_steering'
    ];

    /**
     * The {{ cars }} tag
     *
     * @return mixed
     */
    public function index()
    {
        // Early Return
        $data = Car::whereNotNull($this->matrix[$this->get('partial')])->where('slug', 'like', "%" . $this->get('slug'). "%")->get();
        if ($data->count() === 0) {
            if ($this->get('partial') === 'chiptuning') {
                return redirect("/chiptuning-contact");
            }
            abort(404);
        }

        // Found the exact match (audi-tt-s-all-20-tfsi-306hp)
        if ($data->count() === 1) {
            $data->map(function($entry) {
                $type = 'slug';
                $entry->subject = $type;
                $entry->uri = slugify($entry->getAttribute($type));
                return $entry;
            });
        } else {
            // audi-tt (24 results)
            $data = $data->reduce(function ($acc, $entry) {
                // Explode the slug ([audi, tt])
                $slugParts = explode('-', $this->get('slug'));
                // Get length (2)
                $length = count($slugParts);

                // Set type and entryCheck
                $type = 'model';
                $found = 'brand';

                // Find loose matches
                for ($i = $length; $i > 0; $i--) {
                    // subject = tt
                    // brand = audo { ! no match }
                    if (Str::contains(slugify($entry->getAttribute('brand')), $slugParts[$i - 1]) && strlen($slugParts[$i - 1]) > 1) {
                        break;
                    }

                    // Subject = tt
                    // Model = tt (s) (rs) {match so we break}
                    if (Str::contains(slugify($entry->getAttribute('model')), $slugParts[$i - 1])) {
                        $found = 'model';
                        $type = 'generation';
                        break;
                    }
                    if (Str::contains(slugify($entry->getAttribute('generation')), $slugParts[$i - 1])) {
                        $found = 'generation';
                        $type = 'type';
                        break;
                    }
                    if (Str::contains(slugify($entry->getAttribute('type')), $slugParts[$i - 1])) {
                        $found = 'type';
                        $type = 'slug';
                        break;
                    }
                    // if not found remove part
                    array_pop($slugParts);
                }

                // Filter hard
                // Remove slug in slugparts that doesn't exist in de found array
                // Ex: tt-rs, tt-s, tt
                $foundArr = explode('-', slugify($entry->getAttribute($found)));

                // Only return the unique values from slugparts [tt]
                $slugParts = array_unique(array_filter($slugParts, function ($part) use ($foundArr) {
                    return in_array($part, $foundArr);
                }));

                // If the found count is not the same as the total slugparts return
                // [tt] !== [tt] (so contintue)
                // [tt, s] !== [tt] (break)
                // [tt, rs] !== [tt] (break)
                if (count(array_unique($foundArr)) !== count($slugParts)) {
                    return $acc;
                }

                // If the collection already has the same uri don't do anything
                // else push to the collection
                if ($acc->contains('uri', slugify($entry->getAttribute($type))) === false) {
                    $entry->subject = $type;
                    $entry->uri = slugify($entry->getAttribute($type));
                    $entry->title = ($type === 'slug') ? $entry->id : $entry->getAttribute($type);

                    $acc->push($entry);
                }

                return $acc;
            }, collect());
        }

        return array_merge(
            [
                'count' => $data->count(),
                'subject' => $data->first()->subject,
                'carCollection' => $data->toArray(),
            ],
            $this->formatSelector($data->first()->subject, $data->first())
        );
    }

    /**
     * @param $type
     * @param $data
     * @return array
     */
    private function formatSelector($type, $data)
    {
        $sequence = [
            'brand' => 1,
            'model' => 2,
            'generation' => 3,
            'type' => 4,
            'slug' => 5
        ];

        return [
          'brand' => $sequence[$type] > 1 ? $data->brand->get('title') : null,
          'model' => $sequence[$type] > 2 ? $data->model : null,
          'generation' => $sequence[$type] > 3 ? $data->generation : null,
          'type' => $sequence[$type] > 4 ? $data->type : null
        ];
    }
}
