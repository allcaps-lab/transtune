<?php

namespace Statamic\Addons\Cars;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Statamic\API\Fieldset;
use Statamic\CP\Publish\ProcessesFields;
use Statamic\CP\Publish\ValidationBuilder;
use Statamic\Extend\Controller;

class CarsController extends Controller
{
    use ProcessesFields;

    /**
     * Maps to your route definition in routes.yaml
     *
     * @return mixed
     */
    public function index()
    {
        return $this->view('index', [
            'title' => 'Cars',
            'cars' => Car::all()
        ]);
    }

    public function create()
    {
        return $this->view('create', [
            'title' => 'New Car',
            'data' => $this->prepareData([])
        ]);
    }

    /**
     * The form to edit an existing product
     */
    public function edit($id)
    {
        $car = Car::find($id);

        $data = $car->toArray();
        $data['brand'] = $car->brand->id();

        return $this->view('edit', [
            'title' => $car->title,
            'car' => $car,
            'data' => $this->prepareData($data)
        ]);
    }

    public function store(Request $request)
    {
        $validator = $this->validator();

        if ($validator->fails()) {
            return ['success' => false, 'errors' => $validator->errors()->toArray()];
        }

        $data = $this->processFields($this->fieldset(), $request->fields);

        $data['slug'] = $this->slugifyCar($data);

        $car = Car::create($data);

        return $this->successResponse($car->id);
    }

    private function slugifyCar($data)
    {
        return slugify("${data['model']} ${data['brand']} ${data['generation']} ${data['type']}");
    }

    /**
     * Endpoint for updating an existing product
     */
    public function update(Request $request, $id)
    {
        $car = Car::find($id);
        $validator = $this->validator();
        if ($validator->fails()) {
            return ['success' => false, 'errors' => $validator->errors()->toArray()];
        }
        $data = $this->processFields($this->fieldset(), $request->fields);
        // Statamic's fieldtype processing would result in any 'null' values being stripped out from $data so they aren't saved in files.
        // If $data is missing a key, it would simply be ignored when using Eloquent's update() method, leaving any existing data in
        // the database. Because of this, we'll merge in any nulls so that the appropriate values get removed from the DB row.
        $data = $this->mergeNulls($data);
        $car->update($data);
        return $this->successResponse($id);
    }

    private function validator()
    {
        $validationBuilder = new ValidationBuilder(request()->fields, $this->fieldset());

        $validationBuilder->build();

        return Validator::make(
            request()->all(),
            $validationBuilder->rules(),
            [],
            $validationBuilder->attributes()
        );
    }

    /**
     * Prepare data to be used in the publish form.
     * It will add nulls (or appropriate default values) for any fields defined in the fieldset
     * that haven't been provided in the array. Vue needs the values to exist for reactivity.
     */
    private function prepareData($data)
    {
        // Map Taxonomies to their id

        return $this->preProcessWithBlankFields($this->fieldset(), $data);
    }
    /**
     * Get the fieldset.
     */
    private function fieldset()
    {
        return Fieldset::get('car');
    }

    /**
     * Merge in null for every field in the fieldset that was not in the given array.
     */
    private function mergeNulls($data)
    {
        return collect($this->fieldset()->inlinedFields())
            ->keys()
            ->mapWithKeys(function ($field) {
                return [$field => null];
            })->merge($data)->all();
    }
    /**
     * The response to be returned for a successful save.
     */
    private function successResponse($id)
    {
        $message = 'Car saved';
        // Actions that trigger an actual redirect should add the message into the session's flash data.
        if (! request()->continue || request()->new) {
            $this->success($message);
        }
        return [
            'success'  => true,
            'redirect' => request()->continue
                ? route('cars.edit', ['car' => $id])
                : route('cars.index'),
            'message' => $message
        ];
    }
}
