@extends('layout')

@section('content')

    <script>
      Statamic.Publish = {
        contentData: {!! json_encode($data) !!},
      };
    </script>

    <publish
            fieldset-name="car"
            :is-new="true"
            title="New Car"
            submit-url="{{ route('cars.store') }}"
    ></publish>

@endsection