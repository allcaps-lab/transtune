@extends('layout')

@section('content')

    <script>
      Statamic.Publish = {
        contentData: {!! json_encode($data) !!},
      };
    </script>

    <publish
            fieldset-name="car"
            id="{{ $car->id }}"
            title="{{ $car->title }}"
    ></publish>

@endsection