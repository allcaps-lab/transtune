<?php
namespace Statamic\Addons\Carsys;

use Statamic\Extend\Extensible;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Statamic\API\Entry;
use Statamic\API\Taxonomy;
use GuzzleHttp\RequestOptions;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class CarsysAPI
{
  use Extensible;

  /**
   * Endpoint to connect to carsys planner
   *
   * @var string
   */
  protected $endpoint = "https://api.garage.software/v1/public/%s/planner/";

  /**
   * Guzzle http client
   *
   * @var Client
   */
  protected $client;


  /**
   * Settings from carsys
   *
   * @var array
   */
  protected $settings;

  /**
   * Constructor
   */
  public function __construct()
  {
    // Get unique code from customer
    $csCode = bin2hex($this->getConfig('cs_code'));

    // Set up http client
    $this->client = new Client([
      'base_uri' => sprintf($this->endpoint, $csCode),
      'headers' => [
        'Application' => $this->getConfig('api_key')
      ]
    ]);

    // Set up settings
    $response = $this->client->get('settings');
    $this->settings = $this->decodeBody($response);
  }

  /**
   * Decode a response body and return the data
   *
   * @param Response $response
   * @return array
   */
  protected function decodeBody(Response $response)
  {
    return json_decode($response->getBody())->data;
  }

  /**
   * Returns the settings
   *
   * @return array
   */
  public function settings()
  {
    return $this->settings;
  }

  /**
   * Get the currently assigned activities
   *
   * @return array
   */
  public function activities()
  {
    $response = $this->client->get("activities");
    return $this->decodeBody($response);
  }

  /**
   * Format replacement for consuming the front-end
   *
   * @param \stdClass $replacement
   * @return array
   */
  protected function formatReplacements(\stdClass $replacement)
  {
    return [
      'title' => $replacement->Extra_info,
      'uuid' => $replacement->Uuid
    ];
  }

  /**
   * Get the replacement transports
   *
   * @return array
   */
  public function replacements()
  {
    $data = $this->decodeBody($this->client->get('replacement_transports'));

    return array_map(function(\stdClass $replacement) {
      return $this->formatReplacements($replacement);
    }, $data);
  }

  /**
   * Format the day for front-end consuming
   *
   * @param \stdClass $day
   * @return array
   */
  protected function formatDay(\stdClass $day)
  {
      // Only show properties that we want the outside to see.
      $status = ($day->Status === 'disabled') ? 'unavailable' : $day->Status;

      // In the front end we use disabled for days that we can't use.
      if (!in_array(Carbon::createFromFormat('Y-m-d', $day->Date)->dayOfWeek, $this->settings->shop_opening_days)) {
        $status = 'disabled';
      }

      return [
          "date" => $day->Date,
          "available_minutes" => $day->Available_minutes,
          "planned_minutes" => $day->Planned_minutes,
          "status" => $status,
      ];
  }

  /**
   * Returns all coming days with their status
   *
   * @return array
   */
  public function availability()
  {
    $response = $this->client->get('availability/default');
    $data = $this->decodeBody($response);

    // Collect the days returned and put them into their own nice
    // multidimensional array
    $states = array_reduce($data, function($states, \stdClass $day) {
      $day = $this->formatDay($day);
      switch ($day['status']) {
        case 'available':
          $states['available']['dates'][] = $day['date'];
          break;
        case 'partially_available':
          $states['partial']['dates'][] = $day['date'];
          break;
        case 'unavailable':
          $states['unavailable']['dates'][] = $day['date'];
        case 'disabled':
          $states['disabled']['dates'][] = $day['date'];
          break;
      }

      $states['max_date'] = $day['date'];
      return $states;
    }, [
      "unavailable" => ["dates" => []],
      "partial" => ["dates" => []],
      "available" => ["dates" => []],
      "max_date" => false,
    ]);

    return $states;
  }

  /**
   * Format the given options to a readable string
   *
   * @param $options
   * @param $quote
   * @return string
   */
  protected function formatOptions($options, $quote)
  {
    $options = array_filter($options, function($option) {
      if (strpos($option, 'chip')) {
        return Taxonomy::handleExists($option);
      }
      return true;
    });

    $string = "Gekozen opties: " . implode(", ", array_map(function($option) {
      return str_replace("options/", "", $option);
    }, $options));

    if ($quote) {
      $string = $string . "/r/n Offerte aangevraagd";
    }

    return $string;
  }

  /**
   * Check if activties provided actually exist in transtune
   *
   * @param $activities
   * @return void
   */
  protected function checkActivities($activities)
  {
    return array_filter($activities, function($activity) {
      return Entry::exists($activity);
    });
  }

  /**
   * reformat appointments for carsys to understand
   *
   * @param array $appointment
   * @return array|bool
   */
  protected function formatAppointment(array $appointment)
  {
    $activities = $this->checkActivities($appointment['activities']);

    // Fast return
    if (empty($activities)) {
      return false;
    }

    return [
      "request_date" => date('Y-m-d', strtotime($appointment['request_date'])),
      "remarks" => $this->formatOptions($appointment['options'], $appointment['quote']),
      'is_foreign_license' => $appointment['foreign'] ?? false,
      "license" => $appointment['carData']['license'],
      "mileage" => (int)$appointment['carData']['mileage'],
      "vehicle_brand" => $appointment['carData']['vehicle_brand'],
      "vehicle_model" => $appointment['carData']['vehicle_model'],
      "firstname" => $appointment['contact']['firstname'],
      "lastname" => $appointment['contact']['lastname'],
      "email" => $appointment['contact']['email'],
      "phone" => $appointment['contact']['phone'],
      "title" => $appointment['contact']['title'],
      "activity_uuids" => $activities,
      "replacement_transport_uuid" => $appointment['replacement_transport'],
      "send_confirmation_email" => true
    ];
  }

  /**
   * Create the appointment
   *
   * @param $appointment
   * @return array|bool
   */
  public function createAppointment($appointment)
  {
    $appointment = $this->formatAppointment($appointment);

    if ($appointment) {
      Log::info("appointment {$appointment['vehicle_brand']} - {$appointment['vehicle_model']}");
      $response = $this->client->post('appointments', [
        RequestOptions::JSON => $appointment
      ]);

      return $this->decodeBody($response);
    }
    return false;
  }
}