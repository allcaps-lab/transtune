<?php

namespace Statamic\Addons\Carsys;

use GuzzleHttp\Client;
use Statamic\Extend\Controller;

class CarsysController extends Controller
{
    /** var CarsysAPI */
    protected $carSys;
    public function __construct()
    {
        $this->carSys = new CarsysAPI();
    }
    /**
     * Maps to your route definition in routes.yaml
     *
     */
    public function getIndex()
    {
        return response()->json($this->carSys->availability());
    }

    /**
     * Get replacements
     *
     */
    public function getReplacements()
    {
        return response()->json($this->carSys->replacements());
    }

    /**
     * Create an appointment
     *
     */
    public function postAppointment()
    {
        $this->validate(request(), [
            'options.*' => 'string',
            'activities' => 'required|array',
            'activities.*' => 'string',
            'request_date' => 'required|date',
            'carData.license' => 'required',
            'carData.mileage' => 'required',
            'carData.vehicle_model' => 'required',
            'carData.vehicle_brand' => 'required',
            'contact.firstname' => 'required',
            'contact.lastname' => 'required',
            'contact.email' => 'required|email',
            'contact.phone' => 'required',
            'contact.title' => 'required',
            'replacement_transport' => 'required|string'
        ]);

        $response = $this->carSys->createAppointment(
            request()->only(['options', 'activities', 'request_date', 'carData', 'contact', 'replacement_transport', 'quote'])
        );

        if ($response) {
            return response()->json($response);
        }

        return response()->json(['message' => 'something went wrong'], 422);
    }
}
