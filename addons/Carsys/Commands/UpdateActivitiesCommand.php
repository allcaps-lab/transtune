<?php

namespace Statamic\Addons\Carsys\Commands;

use Statamic\Extend\Command;
use Statamic\Addons\Carsys\CarsysAPI;
use Statamic\API\Entry;
use Statamic\API\Str;
use Statamic\API\Collection;

class UpdateActivitiesCommand extends Command
{
  /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'carsys:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates all carsys activities.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $api = new CarsysAPI();
        $activities = collect($api->activities());

        if (Collection::handleExists('activities')) {
            // Reduce activities when encountered
            Collection::whereHandle('activities')->entries()->each(function($entry, $index) use ($activities) {
                $activity = $activities->where('Uuid', $index)->first();
                if ($activity) {
                    $entry->set('title', $activity->Name);
                    $entry->set('extra', $activity->Extra_info);
                    $entry->set('duration', $activity->Estimated_duration);
                    $entry->save();
                } else {
                    $entry->set('active', false);
                    $entry->save();
                }
            });
        }

        foreach ($activities->slice(Collection::whereHandle('activities')->entries()->count()) as $activity) {
            Entry::create(Str::slug($activity->Name))
                ->collection('activities')
                ->with([
                    'id' => $activity->Uuid,
                    'title' => $activity->Name,
                    'duration' => $activity->Estimated_duration
                ])->save();
        }

        $this->info("All activities updated");
    }
}