<?php

namespace Statamic\Addons\Carsys;

use Statamic\Extend\Tasks;
use Illuminate\Console\Scheduling\Schedule;

class CarsysTasks extends Tasks
{
    /**
     * Define the task schedule
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    public function schedule(Schedule $schedule)
    {
        $schedule->command('carsys:update')->daily();
    }
}
