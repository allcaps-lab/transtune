items:
  -
    type: item
    cta_title: 'Transmissie service'
    media: /assets/transtune_bas_fransen_6.jpg
    page: 487e0f29-ae33-4f22-9252-fc88f1b77ac0
    is_video: false
  -
    type: item
    cta_title: 'Chiptuning-soft & hardware'
    is_video: false
    media: /assets/porsche-992-tuning.jpg
    page: 27a12bf3-70c7-4a3b-8954-3b41c0d19ecd
  -
    type: item
    cta_title: Walnutblasting
    is_video: false
    media: /assets/transtune_bas_fransen_47.jpg
    page: 4dd6a890-fc28-4eaa-9001-07ec9325aa5f
  -
    type: item
    cta_title: 'Transtune automotive'
    is_video: false
    media: /assets/dsc_8818.jpg
    page: 861f0875-7b43-4a0a-8b47-b94087f07d6e
seo:
  title: 'Transtune - Uw Specialist in Transmissie en Chiptuning!'
title: Transtune
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1><em>Onze diensten</em></h1>'
      -
        type: pages
        pages:
          - 644780c6-677c-445a-bde9-5600aae8527e
          - fee89d2d-dad0-43f1-a99b-384411f0f04f
          - 27a12bf3-70c7-4a3b-8954-3b41c0d19ecd
          - 4dd6a890-fc28-4eaa-9001-07ec9325aa5f
      -
        type: text
        text: '<h1><br><br>Veel geld besparen met automaat spoelen of automaat revisie</h1><p>Transmissie kapot? Automaat schakelt schokkerig? Bij Transtune helpen wij u verder. Transtune is een jong, enthousiast bedrijf en een vakkundige transmissiespecialist in Nederland. Wij kunnen elke <u><a href="{{ link:644780c6-677c-445a-bde9-5600aae8527e }}">automaat spoelen</a></u> of een complete <u><a href="{{ link:fee89d2d-dad0-43f1-a99b-384411f0f04f }}">automaatrevisie</a></u> uitvoeren voor alle automerken (uitgezonderd Japanse en Koreaanse merken). <br><br>Met een automaatrevisie of spoeling kunt u veel geld besparen. Dat is de overtuiging van Harm van Bussel, eigenaar van Transtune. “We krijgen regelmatig klanten in de werkplaats die elders een offerte van soms meer dan 10.000 euro hebben ontvangen voor het compleet vervangen van de automatische versnellingsbak. Vaak is een deelreparatie van 1500 euro al voldoende. Desnoods kunnen we een complete revisie uitvoeren, dat kost tussen de 2800 en 4500 euro. En ook met automaat spoelen (''flushen'') kunnen we veel problemen oplossen. Hoe mooi is dat? We hebben als team meer dan 30 jaar ervaring met het reviseren van automatische transmissies en adviseren onze klanten altijd over de beste oplossing voor hun automaatbak. ”</p>'
      -
        type: button_link
        link: 644780c6-677c-445a-bde9-5600aae8527e
        button_text: 'Automaat spoelen'
      -
        type: button_link
        link: fee89d2d-dad0-43f1-a99b-384411f0f04f
        button_text: 'Automaat revisie'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: |
          <h1><em>Meer vermogen of juist eco rijden met chiptuning</em></h1><p>Harm van Bussel is met zijn 26 jaar een van de jongste
          automotive ondernemers in Nederland en groeide op tussen de auto's. Harm is
          gepassioneerd over de combinatie van elektronica en mechanica. Zeker als je
          daar iets extra's mee kan doen. “Veel berijders vinden het leuk om met een
          kleine ingreep meer vermogen uit de auto te halen. Tuning kan bij alle Europese
          automerken (uitgezonderd de Franse merken). Vooral Audi, BMW, Mercedes en
          Volkswagen zijn wat dat betreft populair. Een prestatieverbetering van 20 tot
          40 procent is zeker haalbaar. Daarnaast zien we een doelgroep die juist zuiniger
          wil rijden om de portemonnee én het milieu te besparen, het zogenaamde eco
          tuning. Ook dat is mogelijk met <u>chiptuning</u>. Bovendien kunnen we de
          fabrieksgarantie overnemen zodat je daar geen zorgen over hebt!”<br>Chiptuning de beste optimalisatie voor uw auto&nbsp;<br><br></p><blockquote><p>"Auto laten chippen bij Transtune en een top resultaat behaald van 170 pk naar 214 pk en van 380 nm naar 420 nm. M'n auto trekt harder en rijdt beter !"</p></blockquote>
      -
        type: button_link
        button_text: 'Chiptuning op maat'
        link: 86fb6f66-7bf1-4401-8d78-f6129229b80d
      -
        type: simple_image
        simple_image: /assets/img/c28f07b8-edc4-4a96-b0a0-6cc09066318b.jpeg
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <p><strong>Ga ook naar Transtune voor</strong></p><h1><em>Walnut blasting</em></h1><p>Met <u><a href="{{ link:c4af29ac-e0bc-4bff-af66-b741bdaea7ae }}">walnut blasting</a></u> verwijderen we koolstofafzetting
          op de inlaat van uw automotor, een probleem dat veel voorkomt bij direct
          ingespoten benzine- en dieselmotoren. Na behandeling presteert uw auto weer als
          vanouds!<em>&nbsp;</em></p><h1><em>Stuurhuis spoelen</em></h1><p>De stuurbekrachtiging van de meeste auto's werkt met een
          hydraulisch systeem en hydraulische olie. Deze ATF olie kan verouderen waardoor
          het sturen zwaarder gaat en er trillingen voelbaar zijn.&nbsp; Het <u><a href="{{ link:45c21fd0-e7d7-470a-9c5b-80ef1f8f6532 }}">stuurhuis spoelen</a></u> en het systeem
          voorzien van nieuwe olie kan dit probleem verhelpen.&nbsp;</p><h1><em>Motor spoelen</em></h1><p>Door allerlei omstandigheden kan een motor intern vervuilen.
          Meestal is dit het geval wanneer een auto alleen korte stukjes rijdt, en niet
          de juiste werktemperatuur bereikt. Door middel van <a href="{{ link:a3980a79-2faf-415d-902a-fd0ed56cfe99 }}"><u>motor spoelen</u>
          </a>verwijderen we alle inwendige sludge. Na behandeling loopt de motor weer
          letterlijk gesmeerd.</p><h1><em>Roetfilter reinigen of vervangen</em></h1><p>Een roetfilter beperkt de uitstoot van roetdeeltjes en
          fijnstof bij dieselmotoren. Onder normale omstandigheden brandt het filter
          automatisch schoon (regenereren). Gebeurt dit niet, of onvoldoende, dan raakt
          het filter verstopt. Het gevolg: verminderde prestaties van uw auto en een
          brandend roetfilterlampje. Bij Transtune kunnen wij het <a href="{{ link:0ce69d15-68f9-4ce3-b861-866d7423499a }}"><u>roetfilter reinigen.</u>
          </a>Is dit niet meer mogelijk? Dan zullen wij het filter vervangen.</p>
hero-img: /assets/img/img_4682.jpg
banner_size: is-fullheight
subtitle: 'De specialist in Transmissie & Tuning'
read_more: de627bca-7595-429e-9b41-ad58703916d7
button_text: 'Neem contact met ons op'
has_cta: true
cta_title: 'Bij Transtune staat uw auto centraal'
cta_subtitle: 'Kies uw auto, stel een offerte samen en plan een afspraak'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Plan direct uw afspraak'
is_hidden: false
template: home
fieldset: home
id: 74d2db8e-baee-4fc6-9435-4050d73ab33e
