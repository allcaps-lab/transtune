---
title: 'Ferrari 488 GTB/GTS'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <p>Voor de Ferrari 488GTS/GTB hebben wij een exclusief pakket
          samengesteld waarmee wij de rijbeleving en de drive ability kunnen waarborgen. Het
          inlaat traject wordt voorzien van een BMC F1 airfilter wat zorgt voor een
          betere flow. Het origineel uitlaatsyteem wordt vervangen door Capristo sportkatalysatoren(200
          cells) in combinatie met het Akrapovic Slip-on uitlaatsysteem. Deze combinatie
          staat garant voor het ultieme geluids en vermogens &nbsp;beleving . na het toepassen van deze upgrade
          komt het totaal vermogen op een verwachte 790 PK en 870 NM koppel. </p><p>&nbsp;<br></p><p>Deze totale upgarde kunnen wij aanbieden voor een vaste prijs
          van 13,500 euro inclusief btw</p>
  -
    type: white_background
  -
    type: blue_background
hero-img: /assets/img/transtune_bas_fransen_38.jpg
has_cta: false
is_hidden: false
fieldset: default
id: 109b146e-2845-487d-ac51-ec09767e8f39
---
<p>Na de Ferrari F40 welke voorzien was van een turbomotor kwam Ferrari terug in 2015 met een nieuwe 3.9L V8 middenmotor voorzien van 2 turbo’s. Dit resulteerde in 670PK  en 760NM koppel.
</p>