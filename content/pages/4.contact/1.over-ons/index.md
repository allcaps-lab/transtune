title: 'Over ons'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h1><strong>Kom naar Transtune in Brabant!</strong></h1><p>Transtune is een jong en innovatief bedrijf in het Brabantse
          Deurne, vlakbij Eindhoven. De onderneming is opgericht door Harm van Bussel,
          een van de jongste automotive ondernemers in Nederland. Maar aan technische
          expertise is er geen gebrek! Harm groeide op tussen de auto's, sleutelde al
          vroeg aan transmissies en maakte dit in combinatie met zijn kennis van
          auto-elektronica een bloeiend bedrijf.&nbsp;<br><br><strong>Uw auto beter laten rijden, dat is de missie van Harm en
          zijn team.&nbsp;</strong><br></p><p><br></p>
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: |
          <h1><strong>Automaat spoelen (flushen) en versnellingsbak revisie</strong></h1><p>Bij Transtune wordt u geholpen door een professioneel team
          van gekwalificeerde vakmensen. Samen hebben ze meer dan 30 jaar ervaring in de
          autotechniek, met name in motormanagementsystemen en het spoelen en reviseren
          van automatische versnellingsbakken. Dit doen wij voor voor elk type auto. Onze
          moderne werkplaats is uitgerust met hightech diagnoseapparatuur om storingen in
          motormanagement en versnellingsbakken te lokaliseren en te verhelpen. De focus
          is daarbij op Europese auto's met een voorliefde voor de Duitse merken. Maar
          ook wanneer u een Volvo, Ford, Peugeot, Citroen, Renault, Alfa Romeo of Fiat
          rijdt kunnen wij de automaat van uw auto flushen of volledig reviseren.&nbsp; &nbsp;</p><p><br></p>
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h1><strong>Auto tuning voor iedereen in Nederland</strong></h1><p>Een andere tak van sport binnen Transtune is het tunen van
          auto's. Chiptuning kan bij alle Europese automerken, met uitzondering van de
          Franse merken. Voor ons als auto tuning bedrijf staat niet het realiseren van
          maximaal vermogen centraal, maar juist het verbeteren van het rijgedrag van de
          auto. Meer souplesse en een betere acceleratie verhogen de verkeersveiligheid
          in het verkeer is de overtuiging van Harm. “Dat doen wij met op maat gemaakte
          software, we programmeren alles zelf voor een optimale prestatie van de motor.
          Best uniek voor auto tuning in Nederland. We krijgen dan ook klanten vanuit het
          hele land en zelfs vanuit België, dus niet alleen de regio Eindhoven!”</p>
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: |
          <h1><strong>Innovatief en vooruitstrevend</strong></h1><p>Service en kwaliteit lopen als een rode draad door het werk
          van Transtune vertelt Harm van Bussel “We zijn innovatief en vooruitstrevend.
          Lopen voorop als het gaat om technische ontwikkelingen in de autobranche.
          Daarnaast geloof ik in ouderwets contact met je klanten. Heeft je auto een
          probleem? Merk je dat hij niet lekker rijdt en komt je dealer er niet uit? Dan
          kan je bij ons altijd een afspraak maken voor een analyse. Het probleem kan in
          de elektronica zitten, maar je kan ook te maken hebben met koolstofafzetting op
          de inlaat, verouderde versnellingsbakolie of een kapotte automaatbak. Afhankelijk
          van het probleem zoeken we de beste oplossing. Soms is dat een <a href="{{ link:162560d3-0264-43ea-a629-bf7508181b26 }}">update van de
          motorsoftware</a>, soms <u><a href="{{ link:c4af29ac-e0bc-4bff-af66-b741bdaea7ae }}">walnutblasting</a></u>, in het andere geval het <u><a href="{{ link:0ce69d15-68f9-4ce3-b861-866d7423499a }}">roetfilter
          vervangen</a></u>, het <a href="{{ link:644780c6-677c-445a-bde9-5600aae8527e }}"><u>spoelen van de automatische transmissie</u> </a>of juist een
          complete <u><a href="{{ link:fee89d2d-dad0-43f1-a99b-384411f0f04f }}">revisie</a></u>.”</p>
hero-img: /assets/img/transtune_bas_fransen_2.jpg
banner_size: is-large
has_cta: false
is_hidden: false
template: about
fieldset: default
mount: team
id: 72c016c6-cc0a-4928-b53b-3275f3f6da0a
