title: 'Ons team'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: full_height_image
                full_height_image: /assets/img/afbeelding3-1575713578.png
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Luuk - 2e autotechnicus</strong></p><p><em>"Transtune is een specialist die zich 2008 onderscheidt als technisch specialist voor transmissie- en tuning techniek. Een kenmerkende eigenschap van Transtune is dat wij beginnen waar anderen vaak noodgedwongen ophouden. Dankzij onze kennis en hightech apparatuur hebben motoren,transmissie,aandrijflijnen en motormagement-systemen geen geheimen meer voor ons. Transtune is specialist in : • Motor Tuning-software • Motor Tuning-hardware • Transmissie Tuning-software • Transmissie Tuning-hardware • Transmissie Powerflush • Transmissie Revisie"&nbsp;&nbsp;</em></p>'
          -
            type: column
            content:
              -
                type: full_height_image
                full_height_image: /assets/img/afbeelding2-1575713572.png
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Roel - Technisch specialist personenauto''s</strong></p><p><em>"Transtune is een specialist die zich 2008 onderscheidt als technisch specialist voor transmissie- en tuning techniek. Een kenmerkende eigenschap van Transtune is dat wij beginnen waar anderen vaak noodgedwongen ophouden. Dankzij onze kennis en hightech apparatuur hebben motoren,transmissie,aandrijflijnen en motormagement-systemen geen geheimen meer voor ons. Transtune is specialist in : • Motor Tuning-software • Motor Tuning-hardware • Transmissie Tuning-software • Transmissie Tuning-hardware • Transmissie Powerflush • Transmissie Revisie"&nbsp;&nbsp;</em></p>'
          -
            type: column
            content:
              -
                type: full_height_image
                full_height_image: /assets/img/afbeelding2-1575721016.jpg
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Jos - Allround medewerker</strong></p><p><em>"Transtune is een specialist die zich 2008 onderscheidt als technisch specialist voor transmissie- en tuning techniek. Een kenmerkende eigenschap van Transtune is dat wij beginnen waar anderen vaak noodgedwongen ophouden. Dankzij onze kennis en hightech apparatuur hebben motoren,transmissie,aandrijflijnen en motormagement-systemen geen geheimen meer voor ons. Transtune is specialist in : • Motor Tuning-software • Motor Tuning-hardware • Transmissie Tuning-software • Transmissie Tuning-hardware • Transmissie Powerflush • Transmissie Revisie"&nbsp;&nbsp;</em></p>'
          -
            type: column
            content:
              -
                type: full_height_image
                full_height_image: /assets/img/afbeelding4-1575713583.png
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Szymon - Assistent monteur</strong></p><p><em>"Transtune is een specialist die zich 2008 onderscheidt als technisch specialist voor transmissie- en tuning techniek. Een kenmerkende eigenschap van Transtune is dat wij beginnen waar anderen vaak noodgedwongen ophouden. Dankzij onze kennis en hightech apparatuur hebben motoren,transmissie,aandrijflijnen en motormagement-systemen geen geheimen meer voor ons. Transtune is specialist in : • Motor Tuning-software • Motor Tuning-hardware • Transmissie Tuning-software • Transmissie Tuning-hardware • Transmissie Powerflush • Transmissie Revisie"&nbsp;&nbsp;</em><br></p>'
          -
            type: column
            content:
              -
                type: full_height_image
                full_height_image: /assets/img/afbeelding1-1575713568.png
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Wesley - Social Media</strong></p><p><em>Sinds 2019 ben ik aan het werk bij Transtune. Door mijn enorme passie voor auto''s ben ik bij dit bedrijf terecht gekomen. Wat ik doe? Ik zorg voor alles wat te maken heeft met het bedrijfsuiterlijk! Denk aan het onderhouden van de website, het verzorgen van materiaal voor social media &amp; diverse andere taken. Mijn doel is om Transtune op de kaart te zetten binnen de Benelux. En daar ga ik mezelf voor de volle 100% voor inzetten!&nbsp;</em></p>'
          -
            type: column
            content:
              -
                type: full_height_image
                full_height_image: /assets/img/afbeelding1-1575721025.jpg
              -
                type: text
                text: '<p><br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Harm - Tuning service</strong></p><p><em>Transtune is een jong en innovatief bedrijf in het Brabantse Deurne, vlakbij Eindhoven. De onderneming is opgericht door Harm van Bussel, een van de jongste automotive ondernemers in Nederland. Maar aan technische expertise is er geen gebrek! Harm groeide op tussen de auto''s, sleutelde al vroeg aan transmissies en maakte dit in combinatie met zijn kennis van auto-elektronica een bloeiend bedrijf.&nbsp;</em><em>"Transtune is een specialist die zich 2008 onderscheidt als technisch specialist voor transmissie- en tuning techniek. Een kenmerkende eigenschap van Transtune is dat wij beginnen waar anderen vaak noodgedwongen ophouden. Dankzij onze kennis en hightech apparatuur hebben motoren,transmissie,aandrijflijnen en motormagement-systemen geen geheimen meer voor ons. Transtune is specialist in : • Motor Tuning-software • Motor Tuning-hardware • Transmissie Tuning-software • Transmissie Tuning-hardware • Transmissie Powerflush • Transmissie Revisie"&nbsp;&nbsp;</em></p>'
hero-img: /assets/img/transtune_bas_fransen_25.jpg
banner_size: is-large
has_cta: false
is_hidden: false
fieldset: default
id: 4b699c43-069b-47c9-8b6f-1b8f472bf31c
