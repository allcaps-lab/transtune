title: Vacatures
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h1>Vacatures&nbsp;</h1><p>Wij zijn altijd op zoek naar goed en gemotiveerd personeel. Mensen die graag in een klein en gezellig team willen samenwerken. Samenwerken voor het ultieme doel: een tevreden klant, die graag bij ons komt.
          <br><br>Op dit moment hebben wij <strong>twee openstaande vacatures.</strong> Klik op de vacature voor meer informatie.<br><br><strong><a href="{{ link:e57009d2-f451-4f32-b633-4ff33ddeee37 }}">Eerste Autotechnicus Transmissie​</a><br><br></strong></p><p><strong><a href="{{ link:042ec975-a89c-409a-868c-6d59b3f0e50f }}">Tweede Autotechnicus Transmissie in opleiding (BBL)</a></strong><br><br>Graag nodigen we je uit voor een eerste kennismaking. Ben je geïnteresseerd? Neem dan contact met Annerie van der Sterre op 06-51664784 en email je cv naar&nbsp;<a href="mailto:info@atmaram.nl">info@atmaram.nl</a>.<br><br></p><p><strong>Heb je vragen naar aanleiding van deze vacature?</strong></p><p>Neem contact op met Harm van Bussel: 0493-312658<br><br></p><p><strong>Let op: Sollicitatiegesprekken vinden plaats op vrijdag 5 juli a.s. vanaf 13:00 uur in Deurne. Reserveer deze datum vast in je agenda!<br><br><br></strong></p><p><em>Acquisitie naar aanleiding van deze vacature wordt niet op prijs gesteld.</em></p>
hero-img: /assets/img/5d5ec144-26dd-494e-8070-5108f3fbe531.jpeg
banner_size: is-large
has_cta: false
is_hidden: false
fieldset: default
id: 8fb1870f-6fa2-41e2-9ebf-1cab94f0dab0
