title: 'Roetfilter service'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Wat u over het roetfilter (DPF) en het controleren en reinigen moet weten</h1><p><em>Elke moderne auto is heden ten dage (vanaf 2011) uitgerust met een roetfilter. Dat roetfilter kan de prestaties van uw auto aardig belemmeren. Wat moet u weten en doen voor een optimale prestatie van uw auto?</em></p>'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h1>Een roetfilter?<br></h1><p>Hoe werkt eigenlijk zo’n roetfilter oftewel zo’n Diesel Particilate Filter (PDF)? Kleine roetdeeltjes kleven vast aan de wanden van de poriën van het filtermateriaal en worden zo afgevangen. Elke 200 – 1.000 km worden de deeltjes bij hoge motorbelasting (motortemperatuur ca. 600 graden) verbrandt (geregenereerd). Fabrikanten streven ernaar om het motorvermogen niet te laten beïnvloeden, maar in de praktijk kan een verontreinigd roetfilter tot 2 – 4% hoger brandstofgebruik leiden en dus ook tot een hogere CO<sub>2</sub>-uitstoot.</p><h1>Is een roetfilter verplicht?</h1><p>Personenauto’s.&nbsp;Personenauto’s vanaf bouwjaar 2011 zijn vanuit de fabriek standaard van een roetfilter voorzien. Bij deze auto’s is het hebben van een roetfilter verplicht. Is een auto van vóór 2011 en stond deze bij de RDW mét roetfilter geregistreerd, ook dan is een goed werkend roetfilter verplicht.</p><p>Bestelauto’s.&nbsp;Hetzelfde geldt trouwens ook voor bestelauto’s, maar die worden pas van bouwjaar 2012 af-fabriek van een roetfilter voorzien.</p>'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Een slecht functionerend roetfilter</h2><p>Een sterk verouderd roetfilter kan vervuild raken en die vervuiling kan uiteindelijk leiden tot een slechte filtering en slechtere motorprestaties.&nbsp;Preventief reinigen&nbsp;voorkomt problemen.</p><h2>Roetfilter vervangen of reinigen?</h2><p>Als u te lang met problemen doorrijdt kan het roetfilter schade oplopen. Een roetfilter vervangen is dan vaak een kostbare aangelegenheid.&nbsp;Reinigen&nbsp;is dan een heel goed alternatief. Transtune checkt altijd éérst de kwaliteit van het filter en de mogelijke interne schade en adviseert pás dan: reinigen of vervangen.</p><p>Roetfilter controle.&nbsp;Het roetfilter wordt aan de buitenkant gecontroleerd op visuele schade én aan de binnenkant. Externe schade is makkelijk te herkennen, maar voor het opsporen van interne schade gebruikt Transtune speciale apparatuur.</p><p>Gebruik van imitatiefilters.&nbsp;Hoogwaardige reiniging werkt ook beter dan het roetfilter vervangen voor een goedkoop imitatiefilter. Dit is meestal een stuk zwakker en kwalitatief veel minder dan een origineel filter.</p><h2>Roetfilter vervangen</h2><p>Uiteraard kan Transtune ook&nbsp;roetfilters vervangen. Dat doen we voor nagenoeg alle merken en type auto’s.</p><p>Vraag altijd eerst advies over welke oplossing het best is in uw situatie. Vervanging van het roetfilter is lang niet altijd nodig, én … reinigen is bij interne schade ook niet de juiste oplossing. Transtune toonaangevend in het hoogwaardig oplossen van roetfilterproblematiek. Neem <a href="https://www.transtune.nl/contact">contact</a> op voor een gedegen advies of maak meteen een&nbsp;afspraak.</p>'
hero-img: /assets/img/img_4642.jpg
banner_size: is-large
has_cta: true
cta_title: 'Wilt u ook meer uit uw auto halen?'
cta_subtitle: 'Neem contact met ons op en vraag naar de mogelijkheden!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: chiptuning
fieldset: default
id: 0ce69d15-68f9-4ce3-b861-866d7423499a
