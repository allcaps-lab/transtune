---
title: Stuurhuisspoelen
page_content:
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h3>Voor het plaatsen van een nieuw stuurhuis of een nieuwe stuurpomp is het belangrijk om het hydraulische systeem goed te spoelen.<br></h3><p>In de praktijk blijkt dat het spoelen van het stuurhuissysteem door tijdgebrek wel eens vergeten wordt. Terwijl dit juist zo belangrijk is. In de leidingen van het systeem bevinden zich immers vaak nog kleine (metalen) restdeeltjes, afkomstig uit het oude stuurhuis of de hydraulische pomp. Niet spoelen betekent dat deze deeltjes opnieuw in het systeem terecht komen. Dit kan tot schade of lekkages leiden. Na montage moet het systeem nogmaals gespoeld worden om restanten, zoals de testolie, goed te kunnen verwijderen. Andere onderdelen zoals de stuurbekrachtigingspomp en leidingen kunnen nog resten vloeistof van de oude olie bevatten. Het vervangen van de vloeistof is bovendien een goed moment om het eventueel aanwezige stuurbekrachtigingsfilter te vervangen of er een te monteren.&nbsp;</p><p>Let op servoleidingen Een andere belangrijke reden om het hydraulische systeem goed te spoelen vormen de servoleidingen. Een slecht functionerend stuurhuis of -pomp kan veroorzaakt worden door restanten van de leidingen in het systeem. De drukleidingen vergaan naar verloop van tijd en stukjes rubber en/of bezinksel verstoppen de verschillende openingen en kleppen van het systeem. Het is daarom verstandig om bij een vervanging van een stuurhuis of stuurpomp ook altijd alle servoleidingen te controleren. Aangezien de leidingen aan de binnenzijde vergaan, is dat moeilijk aan de buitenzijde te zien. Als er sprake is van harde of sponsachtige drukleidingen is het raadzaam om alle leidingen te vervangen. Vaak is het zo dat als er één leiding versleten is, waarschijnlijk alle leidingen aan vervanging toe zijn.</p>'
hero-img: /assets/img/dsc_5063.jpg
banner_size: is-large
has_cta: true
cta_title: 'Wilt u ook meer uit uw auto halen?'
cta_subtitle: 'Neem contact met ons op en vraag naar de mogelijkheden!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: stuurhuis-spoelen
seo:
  title: 'Stuurhuis spoelen en flushen door Transtune!'
  description: 'Laat uw stuurhuis vakkundig spoelen. Transtune is specialist in Stuurhuis Spoelen. Selecteer eenvoudig uw auto en plan uw afspraak!'
fieldset: default
id: 45c21fd0-e7d7-470a-9c5b-80ef1f8f6532
---
<h1>Stuurhuis spoelen en flushen</h1>
<p>Tegenwoordig maken de meeste 
auto's gebruik van stuurbekrachtiging. Weinig hiervan zijn hydraulisch 
bekrachtigde systemen. Dit soort systemen zijn gevuld met ATF olie of 
speciale hydraulische vloeistof. Door het gebruik van de bestuurder 
wordt het stuurhuis en de overige componenten al gauw zwaar belast. Wij 
raden u daarom aan om de stuurhuis te laten spoelen en flushen.
</p>