title: Motorspoelen
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Motor spoelen (engine flushing)</h1><p>Met motor spoelen oftewel engine flushing wordt de motor van binnen te gereinigd. Hoge temperaturen en vaak in combinatie met stop-go-rijden, zorgen voor een snelle afbraak van cruciale olie additieven (toevoegingen). Deze additieven beschermen bepaalde onderdelen van de motor en voorkomen allerlei problemen. Zonder deze additieven kan de oxidatie-olie veranderen in zware, black sludge in de carter, oliezeef, doorgangen, tuimelaars en in nog meer andere cruciale delen van de motor.</p><p>(Black of White) Sludge is vervuiling van voornamelijk carterolie omdat er door volumeverandering in het carter lucht naar binnen en buiten kan. De meest herkenbare symptomen van sludge zijn:</p><ul><li>erg dikke zwarte olie;</li><li>mayonaise-achtige substantie;</li><li>harde afzettingen in de motor;</li><li>druppels water in de olie.</li></ul>'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Werkwijze</h2><p>Door het motor spoelen wordt de smeercapaciteit verbeterd. Wij adviseren altijd motor spoelen te combineren met ‘Top End Treatment’. Dit heeft een conditionerende werking waardoor de motor inwendig langer wordt schoon gehouden voor een optimale en langdurige bescherming.</p><ul><li>Motor spoelen heeft de volgende voordelen:</li><li>inwendig reiniging van de motor en een verbeterde smering;</li><li>verwijdering van ingedikte motorolie, black- en white sludge;</li><li>neutralisatie van de zuurgraad van de achtergebleven motorolie;</li><li>kwalitatief goede olie tot de volgende olieverversing;</li><li>reiniging van cilinderwanden, zuigerveren en zuigerveergroeven;</li><li>stabilisatie van de compressie-einddrukken en de cilindervulling;</li><li>reiniging van hydraulische klepstoters, nokkenas- en lichthoogteverstellingen;</li><li>vermindering van de inwendige vervuiling via het EGR- en carterventilatiesysteem;</li><li>reducering van de roetbelasting van roetfilters bij dieselmotoren;</li><li>optimalisatie van het brandstofverbruik en het motorvermogen.</li></ul>'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Prijzen</h2><p>Neem&nbsp;<a href="{{ link:de627bca-7595-429e-9b41-ad58703916d7 }}">contact</a>&nbsp;met ons op voor de prijs van een motor spoeling van uw auto.</p>'
hero-img: /assets/img/img_4429.jpg
banner_size: is-large
has_cta: true
cta_title: 'Wilt u ook meer uit uw auto halen?'
cta_subtitle: 'Neem contact met ons op en vraag naar de mogelijkheden!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: chiptuning
fieldset: default
id: a3980a79-2faf-415d-902a-fd0ed56cfe99
