title: 'Online programmeringen'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Online programmeringen – autosoftware updaten</h1><p>Transtune heeft de nieuwste high tech-appratuur op het gebeid van software. Net als officiële dealers staan wij van alle grote merken online in verbinding met de fabriek:</p><ul><li>BMW;</li><li>Mercedes;</li><li>Volvo;</li><li>Groupe PSA (Peugeot, Citroën, Opel, …)</li><li>VAG-groep (Volkwagen, Audi, Seat, …).</li></ul>'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Werkwijze</h2><p>Als er storingen zijn in een automatische transmissie, updaten we altijd eerst de software van de motor en van de transmissie. Pas dan wordt samen met u reparatie overwogen. Voornamelijk bij de&nbsp;dual clutch transmissies&nbsp;doet updaten van de software vaak wonderen.</p><p>De volgende werkzaamheden kunnen wij in eigenhuis kunnen uitvoeren:</p><ul><li>software-updates van álle modules - van DSG transmissie tot koplamp module…;</li><li>inleren van autosleutels (tot de nieuwste beveiligde componenten);</li><li>van de nieuwste BMW’s het online service verleden bijwerken;</li><li>inleren van achteraf ingebouwde accessoires;</li><li>enz.</li></ul>'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Prijzen</h2><p>Neem&nbsp;<a href="{{ link:de627bca-7595-429e-9b41-ad58703916d7 }}">contact</a>&nbsp;met ons op voor meer informatie als u denkt dat sommige auto-onderdelen geüpdatet moeten worden of maak meteen een&nbsp;afspraak.</p>'
hero-img: /assets/img/transtune_bas_fransen_13.jpg
banner_size: is-large
has_cta: true
cta_title: 'Wilt u ook meer uit uw auto halen?'
cta_subtitle: 'Neem contact met ons op en vraag naar de mogelijkheden!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: chiptuning
fieldset: default
id: 162560d3-0264-43ea-a629-bf7508181b26
