title: 'Mercedes groep'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Mercedes</h1><p>Heeft u een Mercedes? Check hieronder uw tranmissie en vraag een offerte aan.</p>'
      -
        type: pages
        pages:
          - 6d469c87-cf75-4ab8-a5f3-631bc7ebf00d
          - 1a0913c5-f40d-4064-afa5-5c8a45613a00
          - dc8a10dd-4ae8-4bfb-90cf-25eca7ecc11f
          - 8b36a237-c5d4-46f5-9f4f-b2d6bdb96c31
          - 1d4524e9-bae9-49f9-b158-e3935497340c
has_cta: false
is_hidden: true
fieldset: default
id: 4468dcc3-952c-4be2-8b8e-bd40c4f89d23
