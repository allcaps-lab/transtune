title: 'Koppelomvormer vervangen'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h2><strong>Koppelomvormer
          repareren, reviseren of vervangen?</strong></h2><p><br>De koppelomvormer is een belangrijk component van uw auto.
          Het zorgt bij auto's met een automatische transmissie namelijk voor de
          verbinding tussen de motor en de versnellingsbak. Net zoals de koppeling dit
          doet in een handgeschakelde auto.<br><br>In het kort bestaat een koppelomvormer uit een behuizing met
          daarin het turbinewiel, de stator en het pompwiel. De behuizing is gevuld met
          olie, en deze olie draagt via het turbinewiel het motorvermogen over op de
          aangedreven wielen.<br><br>Helaas kan ook de koppelomvormer kapot gaan. Typische
          problemen zijn schokkerig schakelen of trillingen in de aandrijflijn. Ook kan
          het gebeuren dat de auto helemaal niet meer wil rijden ook al staat de hendel
          van de transmissie in D of R.<br><br>Deze symptomen kunnen worden veroorzaakt door een defecte
          koppelomvormer. Wilt u hier zekerheid over? Voor een professionele diagnose
          gaat u naar de transmissiespecialisten van Transtune! Met onze jarenlange
          kennis zijn we in staat snel de oorzaak van een probleem te vinden en een
          oplossing te vinden.&nbsp;</p>
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2><strong>Wat is een koppelomvormer?</strong></h2><p>Een&nbsp;koppelomvormer&nbsp;(in het Engels Torque Converter) is een koppeling die werkt op basis van vloeistof. Deze wordt vaak toegepast in plaats van een mechanische koppeling, Deze wordt toegepast bij auto’s met een automatische versnellingsbak.</p><h3><strong><em>Werking</em></strong></h3><p>Een&nbsp;koppelomvormer&nbsp;bestaat in basis uit drie delen. Eén schoepenrad zit vast aan de motor (pomp), één schoepenrad zit vast aan de versnellingsbak (rotor) en in het midden zit een speciaal schoepenrad (stator) om de vloeistof-stroom om te keren. Bij toepassing in auto’s hebben koppelomvormers vaak ook een&nbsp;<em>lockup</em>&nbsp;om de pomp en rotor mechanisch te verbinden en zo verliezen in de vloeistof te voorkomen.</p>'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h2><strong>Koppelomvormer defect?</strong></h2><p>Wanneer de koppelomvormer defect is zijn er verschillende
          manieren om dit aan te pakken: repareren, reviseren of vervangen. Afhankelijk
          van de ernst van de klachten adviseren wij u altijd de beste optie voor de
          laagste kosten. </p><h3><em>Repareren</em></h3><p>Bij repareren zullen wij de koppelomvormer demonteren en de
          defecte of versleten onderdelen vervangen. Daarna wordt het systeem voorzien
          van nieuwe originele olie voordat we de omvormer weer in de auto monteren.&nbsp;</p><h3><em>Reviseren</em></h3><p>Een revisie kan je zien als een uitgebreide reparatie. Bij
          een revisie demonteren we de complete koppelomvormer en alle onderdelen worden
          grondig nagekeken. Op basis hiervan vervangen we alle versleten onderdelen en
          wordt het complete systeem grondig gereinigd voordat we het weer monteren. We
          raden overigens aan om bij transmissieproblemen niet te lang door te rijden.
          Een revisie is namelijk vaak goedkoper dan vervangen.&nbsp;</p><h3><em>Vervangen</em></h3><p>Is repareren of reviseren van de koppelomvormer niet meer
          mogelijk of onzinnig? Dan kunnen wij de kapotte omvormer vervangen door een
          nieuw exemplaar.<br><br>Wilt u direct weten wat de kosten zijn voor het vervangen
          van uw koppelomvormer? Vul dan bovenaan deze pagina de gegevens in van uw auto
          voor een snelle prijsopgave!</p>
hero-img: /assets/img/dsc04314132-1554745203.jpg
banner_size: is-medium
subtitle: 'Selecteer uw auto en bekijk direct de prijs van het vervangen van een koppelomvormer'
has_cta: true
cta_title: 'Vul de infobox in met uw gegevens en bekijk uw opties'
cta_subtitle: 'OF MAIL NAAR INFO@TRANSTUNE.NL'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: koppelomvormer
seo:
  title: 'Koppelomvormer Vervangen [2 jaar garantie!] Kwaliteit van Transtune'
  description: 'Transtune vervangt uw koppelomvormer! Wij staan garant voor hoge kwaliteit en eerlijkheid. Bij kijk de mogelijkheden voor uw auto op de website!'
fieldset: default
id: 432bc31d-d77d-401e-95df-5491044494d4
