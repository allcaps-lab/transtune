title: 'Deel reparatie'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h2>Deelreparatie</h2><p>Een deelreparatie bij Transtune wil zeggen dat we de
          versnellingsbak niet compleet reviseren, maar alleen de defecte onderdelen
          vervangen. Dit kunnen onderdelen zijn in de versnellingsbak zelf, maar ook een
          <a href="{{ link:432bc31d-d77d-401e-95df-5491044494d4 }}">koppelomvormer</a> of een <a href="{{ link:ffd9b037-6193-4de3-a019-7b62259ecd04 }}">mechatronic</a>.<br><br>Veel garages proberen u bij een defecte koppelomvormer of
          mechatronic ook een compleet nieuwe of gereviseerde versnellingsbak te
          verkopen. Bij Transtune doen we dat niet: we voeren alleen de reparatie uit die
          voor u de snelste en voordeligste oplossing biedt!</p>
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: |
          <h2><strong>Wanneer repareren?</strong><br></h2><p>Voordat we een automatische transmissie repareren voeren we
          eerst een diagnose uit om het probleem te vinden. Hiervoor hebben we de auto
          een dag nodig, zodat we met koude motor en transmissie kunnen beginnen. We
          checken eventuele storingsmeldingen van de auto en controleren de
          transmissieolie op glycol (koelvloeistof).<br><br>Op basis van onze bevindingen maken we een plan van aanpak.
          Soms is het voldoende de <a href="{{ link:644780c6-677c-445a-bde9-5600aae8527e }}">transmissie te spoelen</a>, maar een deelreparatie of
          <a href="{{ link:fee89d2d-dad0-43f1-a99b-384411f0f04f }}">complete revisie</a> zijn ook mogelijk. Uiteraard ontvangt u hiervan vooraf een
          offerte.<br><br>Bij een reparatie van de transmissie zijn er verschillende
          mogelijkheden:</p>
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h3><strong>Koppelomvormer repareren</strong></h3><p>Een koppelomvormer verbindt de automatische versnellingsbak met de motor van uw auto. Klachten over een defecte koppelomvormer zijn bijvoorbeeld schokkerig schakelen, er zijn trillingen of de auto gaat dreunen. Wij kunnen de koppelomvormer van uw auto compleet vervangen maar het is ook mogelijk deze te repareren of te reviseren.</p><p><strong><br></strong></p><h3><strong>Mechatronic repareren</strong></h3><p>De mechatronic is de elektronische regelunit van DSG versnellingsbakken (ook wel bekend als S-tronic, DCT, PDK en Powershift). Ook vinden we mechatronics in veel automatische transmissies met een koppelomvormer zoals die van ZF, Aisin AW, GM en Jatco. Storingen in de mechatronic zorgen voor allerlei schakelproblemen. Bij Transtune kunnen wij de mechatronic van uw auto vervangen voor een geheel nieuw exemplaar. Ook is het mogelijk het systeem te reviseren, te modificeren of te voorzien van nieuwe software.</p><p><br></p><h3><strong>Versnellingsbak repareren</strong></h3><p>Wanneer er onderdelen in de versnellingsbak zelf, zoals tandwielen, defect zijn dan is het doodzaak deze te vervangen. Hiervoor demonteren we de versnellingsbak, maken deze open en vervangen de defecte onderdelen door originele, nieuwe onderdelen. Daarna krijgt de bak nieuwe, originele olie en monteren we het geheel weer in de auto.</p>'
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Neem contact met ons op'
hero-img: /assets/img/transtune_bas_fransen_17.jpg
banner_size: is-large
subtitle: 'Laat uw defecte onderdelen vakkundig vervangen door Transtune'
has_cta: true
cta_title: 'Vul de infobox in met uw gegevens en bekijk uw opties'
cta_subtitle: 'OF MAIL NAAR INFO@TRANSTUNE.NL'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
seo:
  title: 'Problemen met de automaatbak? Laat uw transmissie repareren bij Transtune!'
  description: 'Laat een deelreparatie uitvoeren bij Transtune. U komt niet voor verrassingen te staan u weet waar u aan toe bent.'
fieldset: default
id: 5686030b-cc64-469c-892b-d67abd96c502
