title: 'Volvo groep'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>De Volvo Groep</h1>'
      -
        type: pages
        pages:
          - fb94852d-7d12-4d19-8720-107df0f9ea5e
          - 79172359-15dc-42cb-8297-b65bce6b5b7a
has_cta: false
is_hidden: true
fieldset: default
id: aa6fdf30-1b21-423b-a69f-49499f0b0436
