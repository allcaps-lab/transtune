title: 'Meest gestelde vragen'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>De meest gestelde vragen over flushen en spoelen</h1><p>Mensen die flushen (spoelen) overwegen,&nbsp;lopen vaak met dezelfde vragen rond. De meest gestelde vragen hebben we voor u op een rijtje gezet.</p><p><br></p><p><strong>Longlife olie hoeft toch nooit vervangen te worden ?</strong></p><p><strong></strong>In vergelijking met bijvoorbeeld uw motorolie gaat de olie in uw transmissie veel langer mee. Ook in de olie van uw transmissie ontstaat echter verontreiniging en neemt de kwaliteit van toevoegingen af. Zodoende dient ook de longlife olie in uw transmissie periodiek gecontroleerd en/of vervangen te worden.</p><p><br></p><p><strong>Wat houdt flushen bij Transtune in?</strong></p><ul><li>Flushen bij en door Transtune houdt een zeer grondige manier van spoelen in, want:</li><li>we maken eerst een proefrit met u, zodat we de schakelkwaliteit en eventuele klachten met u kunnen bespreken en kunnen beoordelen;</li><li>met behulp van speciale testapparatuur controleren wij de elektronica van uw transmissie op storingsmeldingen;</li><li>afhankelijk van het type transmissie voeren wij powerflushwerkzaamheden uit;</li><li>na deze werkzaamheden maken we weer samen een proefrit en beoordelen we de werkzaamheden.</li></ul><p><strong><br></strong></p><p><strong>Wat kost spoelen ?</strong><br></p><p>Afhankelijk van het transmissietype varieert de prijs tussen de € 215 en € 537.&nbsp;<u>Gebruik onze info box om uw prijs te achterhalen.</u></p><p><u><br></u></p><p><strong>Wat is de meerwaarde van flushen tegenover verversen?</strong></p><p>Door met een lopende motor de transmissie op druk te houden en er schone olie door te spoelen wordt álle vervuiling verwijderd. Dus ook uit de koppelomvormer, warmtewisselaar en leidingen.</p>'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<p><strong>Lost het spoelen bepaalde klachten op ?</strong></p><p>Met het verversen van de olie en het spoelen van de transmissie kun je geen versleten materiaal meer ‘terugstoppen’ in de transmissie. Stuk is dan ook echt stuk, maar als zich geen defecten in uw transmissie voordoen, zoals kapotte of versleten onderdelen, zal het schakelen van uw transmissie aanzienlijk verbeteren doordat de olie weer soepel en vloeibaarder is.</p><p><br></p><p><strong>Kunnen jullie ook direct de olie in het differentieel vervangen ?</strong></p><p>Ja, voor een kleine meerprijs van € 45,45 excl. btw kunnen wij ook meteen in een moeite door de olie in het differentieel vervangen.</p><p><br></p><p><strong>Doen jullie dezelfde werkzaamheden als zf in Duitsland ?</strong></p><p>Ja, wij doen dezelfde werkzaamheden en leveren dezelfde service en kwaliteit als de filialen in Duitsland.<br></p><p><br></p><p><strong>Worden kogeltjes, veertjes e.d. in de transmissie vervangen?</strong></p><p>Afhankelijk van het type transmissie zullen bepaalde onderdelen vervangen worden. Dit wordt ook bepaald door de aard van de eventuele klacht.</p><p><br></p><p><strong>Hoe lang duurt flushen bij mijn auto?</strong></p><p>Afhankelijk van de transmissie duurt de oliewissel tussen de 1,5 en 5 uur. U kunt erop wachten; wij hebben een mooie wachtruimte met wifi en heerlijke koffie.</p><p><br></p><p><strong>Welke olie gebruiken jullie?</strong></p><p>Originele olie. Helaas is het met automaatbakken niet mogelijk om met één leverancier te werken. 80% van onze producten zijn origineel. De reden hiervoor is dat veel auto’s nog in de fabrieksgarantie zitten. Als hier bijvoorbeeld een aftermarket product inkomt van nederlandse olie handelaren, is dit door de dealer altijd te achterhalen en loopt de fabrieksgarantie gevaar.</p><p>Mercedes en BMW: In de Mercedes 7G tronic zit blauwe ATF-olie. Wij spoelen en verversen deze ook weer met 15 liter originele blauw atf olie. Het zelfde geldt voor ZF automaten in BMW en Audi; die maken echter gebruik van groene ATF-olie.</p><p>First fil producten: Wij hebben alleen maar producten op voorraad die als first fil zijn gebruikt. Denk aan o.a. originele producten, Castrol en Fuchs.</p><p>Grote voorraad. Wij hebben 16 oliebunkers met totaal 5.000 liter verschillende ATF-olie op voorraad. Hierdoor kunnen we voor een lagere prijs als de concurrent werken en dan ook nog met originele olie.</p>'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<p><strong>Wordt het filter altijd vervangen ?</strong></p><p>Wij vervangen altijd het filter en de behoorden pakkingen en stekkers. De prijslijst op de website is dus ook inclusief filter. Niet alle automaatbakken zijn echter voorzien van een filter, zoals bijvoorbeeld bij Volvo de aisin warner automaat. Dit type automaat heeft wel een filter maar zit in de automaat verwerkt, om deze te vervangen moet de automaat gedemonteerd worden. (geheel onder de auto uit en dan de bak splitsen)</p><p><br></p><p><strong>Wat zijn de meest voorkomende klachten waarmee jullie kunnen helpen?</strong></p><p><em>De&nbsp;<u>meest voorkomende klachten</u>&nbsp;zijn:</em></p><p>1. <a href="{{ link:8b30e341-ace3-471b-a19a-7e2c1f5ad058 }}">Bonken of slecht schakelen van de automaat</a></p><p>2. <a href="{{ link:b51221fe-7dc8-41d4-bb65-49792c67e76e }}">Schommelend toerental</a></p><p>3. <a href="{{ link:7dc05eaa-56e7-4ab0-8d02-d7e6794eb4f6 }}">Slippende koppeling</a></p><p>4. <a href="{{ link:52be200d-9232-431d-875c-90e0ba9d1831 }}">Verhoogd brandstofverbruik</a></p>'
hero-img: /assets/img/transtune_bas_fransen_9.jpg
banner_size: is-large
subtitle: 'SELECTEER UW AUTO, U KUNT DAN DIRECT DE PRIJS ZIEN VOOR HET SPOELEN!'
has_cta: true
cta_title: 'Wanneer kan ik bij jullie terecht ?'
cta_subtitle: 'Als u telefonisch of online een afspraak maakt, kunt u binnen twee weken bij ons terecht.'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact!
is_hidden: false
fieldset: default
id: aeedc3ee-c2ac-4593-8d6f-bcb431ac8e68
