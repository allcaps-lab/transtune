---
title: Werkwijze
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<p><strong><a href="{{ link:644780c6-677c-445a-bde9-5600aae8527e }}">Automaat spoelen</a> (powerflush) door Transtune</strong></p><p>Transtune is gespecialiseerd in automaatbak storingen en automaat bakspoelingen (Powerflush). Pas, en alléén dan, als spoelen niet tot het gewenste resultaat leidt, adviseren wij een transmissierevisie.</p>'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<p><em><strong><em>Onze werkwijze</em></strong></em><em></em></p><ul><li>We maken een testrit samen met u en beoordelen de automaat<br><br></li><li>We lezen zowel de motor als de automaat uit, als deze beide zonder storingen zijn kunnen we verder<br><br></li><li>We sluiten de Powerflush-machine aan, we hebben alle type koppelingen op voorraad, bedenk wel; iedere transmissie maakt gebruik van een andere aansluiting<br><br></li><li>Nu we de machine hebben aangesloten starten we de motor, hierdoor gaat automatisch de oliepomp van de automaat ook draaien<br><br></li><li>De olie stroomt door de Powerflush-machine, we kunnen de kwaliteit van de olie beoordelen en het reinigings middel toevoegen aan de oude olie<br><br></li><li>De motor draait nog steeds, en gaan nu elke versnelling bedienen zodat alle olie kanalen gereinigd worden<br><br></li><li>Dit proces duurt 20 min bij een olietemperatuur van 90 graden<br><br></li><li>Nu de automaat volledig is gereinigd, zetten we de motor stil en vervangen het filter.<br><br></li><li>Bij BMW modellen is dit een complete carter pan en bij een DSG bijvoorbeeld een los inzet filter<br><br></li><li>We vullen de Powerflush-machine met 15 liter nieuwe ATF-olie die gehomologeerd is aan uw auto<br><br></li><li>We starten de motor, en gaan wederom elke versnelling bedienen, de transmissie zuigt nu alleen maar nieuwe olie aan via het nieuwe filter, de oude ATF-olie olie wordt in de Powerflush-machine opgeslagen<br><br></li><li>Nu is het eindresultaat in beeld met nog steeds een draaiende motor en u ziet nu alleen maar nieuwe olie door de Powerflush-machine lopen<br><br></li><li>We ontkoppelen de machine en bouwen alles weer dicht.<br><br></li><li>Met zekerheid is nu álle oude olie vervangen door originele nieuwe olie<br><br></li><li>Bij bepaalde auto''s kunnen we in de computer aangeven dat we de olie op de transmissie hebben vervangen<br><br></li><li>We zijn nu ongeveer 2 uur verder: tijd voor een proefrit<br><br></li><li>U kunt tijdens het proces meekijken in de werkplaats en/of genieten van een kopje koffie in de wachtruimte.</li></ul>'
hero-img: /assets/img/transtune_bas_fransen_55-1557328879.jpg
banner_size: is-large
subtitle: 'SELECTEER UW AUTO, U KUNT DAN DIRECT DE PRIJS ZIEN VOOR HET SPOELEN!'
has_cta: true
cta_title: 'Heeft u nog vragen?'
cta_subtitle: 'Lees dan ook eens de meest gestelde vragen over automaatbakspoelen.'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: automaatspoelen
fieldset: default
id: d54da2af-2310-49f9-a4a9-c1f0e4360b5d
---
<h2>Voor 70% van de relaties is spoelen de oplossing</h2><i></i>
<p>Heeft u problemen met uw versnellingsbak en heeft u van uw standaard garage een dure offerte voor een voor een versnellingsbakrevisie gekregen? Bij 70% van de meest voorkomende versnellingsbakproblemen wordt het probleem opgelost door de versnellingsbak te spoelen.
</p>