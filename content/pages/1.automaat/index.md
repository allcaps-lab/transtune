---
title: Automaat
page_content:
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: |
          <h1>Automaat spoelen</h1><ul><li>Altijd
               met de originele transmissieolie van de autofabrikant, of met het beste
               alternatief van de bekende topmerken als Castrol en Mobile.</li><li>Altijd
               <strong>inclusief</strong> vervangen filter.</li><li>Altijd
               klaar <strong>binnen twee uur.</strong></li><li>Altijd
               een afspraak <strong>binnen 5 werkdagen. </strong></li></ul><p>Spoelen is geen tovermiddel. Je lost er
          niet alle problemen mee op. Dankzij onze jarenlange ervaring (bij Transtune
          spoelen we zo'n 2500 transmissies per jaar) kunnen wij dit vooraf al aangeven
          en eventueel een alternatief aanbieden.<br></p>
      -
        type: button_link
        link: 644780c6-677c-445a-bde9-5600aae8527e
        button_text: 'Automaat spoelen'
      -
        type: simple_image
        simple_image: /assets/img/automaat-spoelen-hero.png
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Automaat Revisie</h2><p>Transtune heeft meer dan 30 jaar ervaring met het reviseren van automatische versnellingsbakken. We reviseren ongeveer 350 automaatbakken per jaar. Revisie gebeurt altijd in eigen beheer, in onze speciaal uitgeruste werkplaats. Bij een revisie vervangen we het complete binnenwerk van de automaat. Reviseren gaat dus veel verder dan alleen repareren. En we hebben vertrouwen in ons product: bij Transtune krijgt u twee jaar garantie op een revisie. Veel langer dan de één jaar garantie die u bij veel andere aanbieders krijgt!&nbsp;&nbsp;</p>'
      -
        type: button_link
        link: fee89d2d-dad0-43f1-a99b-384411f0f04f
        button_text: 'Automaat Revisie'
      -
        type: simple_image
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: "<h2>Deelreparatie</h2><p>Bij een deelreparatie vervangen we alleen de defecte onderdelen, bijvoorbeeld de <a href=\"https://transtune.nl/automaat/deel-reparatie/koppelomvormer-vervangen\">koppelomvormer</a> of de <a href=\"https://transtune.nl/automaat/deel-reparatie/mechatronic-vervangen\">mechatronic</a>. Dit is alleen van toepassing bij auto's met een relatief lage kilometerstand (minder dan 150.000 km)<br><br>Hieronder vindt u een opsomming van de automatische transmissies die wij vaak herstellen:</p><ul><li>ZF 8HP</li><li>ZF 6HP</li><li>Mercedes-Benz<ul><li>5G tronic</li><li>7G tronic</li></ul></li><li>VAG Groep (Volkswagen, Audi, Seat, Skoda, Porsche, Bentley, Lamborghini) \t<ul><li>DQ200</li><li>DQ250</li><li>DL500/ 0B5</li><li>DL800</li></ul></li></ul>"
      -
        type: button_link
        link: 5686030b-cc64-469c-892b-d67abd96c502
        button_text: Deelreparatie
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h2>Is mijn automaat kapot?</h2><p>De automatische versnellingsbak van moderne auto's is een
          prachtig stuk hoogstaande techniek. Was je vroeger al trots op een automaat met vier
          versnellingen, tegenwoordig is een 6, 8, of zelfs 10-traps automaat de norm.&nbsp;<br><br>Snel zijn de moderne automaten ook. Met name wanneer de
          auto is voorzien van een dubbele koppeling ben je tegenwoordig met een
          automatische versnellingsbak eerder van je plek dan met een traditionele
          handbak. Bekende voorbeelden hiervan zijn de S-tronic van Audi, DSG van
          Volkswagen en DCT van BMW. <br><br>Maar hoe
          mooi de techniek ook is, er kan altijd wat kapot gaan. Dit geldt niet alleen
          voor de hardware, dus de transmissieonderdelen, ook de software kan haperen.
          Met name de Mechatronic zorgt soms voor <u>problemen</u>. Dit onderdeel kan je
          zien als de 'hersenen' van de DSG-automaat. Het schakelt de juiste versnelling
          in en bedient de koppeling, allemaal op basis van razendsnelle computerberekeningen.&nbsp; &nbsp;&nbsp;<br><br></p><p>Bekende voorbeelden van problemen met automatische
          transmissies:<br></p><ul><li><a href="https://transtune.nl/automaat/problemen/bonkt">Automaat
               schakelt schokkerig</a></li><li>Automatische
               versnellingsbak schakelt niet meer</li><li>Niet
               hoger schakelen dan de derde versnelling</li><li>Automaat
               schakelt slecht</li><li><a href="https://transtune.nl/automaat/problemen/slipt">Automaat
               slipt bij schakelen</a></li><li><a href="https://transtune.nl/automaat/problemen/schommelend-toerental">Schommelend
               toerental bij koude motor</a></li><li><a href="https://transtune.nl/automaat/problemen/slipt">Automaat
               slipt van 2 naar 3</a></li></ul><p><br><br></p>
hero-img: /assets/img/transtune_bas_fransen_15.jpg
banner_size: is-large
subtitle: 'Dé specialist in transmissie service'
has_cta: true
cta_title: 'Maak snel een afspraak'
cta_subtitle: 'Selecteer uw brand, model, generation en type om een afspraak te maken'
is_hidden: false
seo:
  title: 'Transtune.nl | Problemen met de automaat? Wij lossen het voor u op!'
  description: 'Wij zijn gespecialiseerd in Automaat spoelen en Automaat Revisies. ✓Geen onverwachte dure offertes ✓Eerlijk ✓Topkwaliteit. Ga voor de service van Transtune!'
  priority: '1.0'
fieldset: default
id: 487e0f29-ae33-4f22-9252-fc88f1b77ac0
---
<h1>Dé specialist in transmissie service</h1>
<p>Heeft u een probleem met de automaat? Wat het probleem ook is, bij Transtune hebben we de
oplossing. U kunt altijd bij ons terecht voor het opsporen en verhelpen van
storingsmeldingen en andere problemen met de automatische transmissie van uw
auto. Wij zijn gespecialiseerd in:
	<br>
</p>
<ul>
	<li>Automaat spoelen</li>
	<li>Automaat revisie</li>
	<li>Deelreparatie </li>
</ul>