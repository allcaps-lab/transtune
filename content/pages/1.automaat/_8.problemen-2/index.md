---
title: Problemen
fieldset: default
id: ced5d2fa-e40f-4269-9689-424d7f2e85e8
---
<h1>De meest voorkomende automaatproblemen</h1>
<p><i>Veel garages adviseren een revisie van de versnellingsbak terwijl een professionele spoeling vaak ook kan volstaan. Wat zijn de meest voorkomende transmissieproblemen?</i>
</p>
<h2>De automaatbak</h2>
<p>De versnellingsbak is een zeer belangrijk onderdeel van uw auto. De automaatbak voert enorm veel handelingen uit. Denk aan optrekken, afremmen en de vele duizenden schakelingen. Wordt er met enige regelmaat een aanhangwagen of een caravan worden getrokken, dan wordt de vak nog vele malen meer belast.
</p>
<h2>De gevolgen van gebruik en ouderdom</h2>
<p>Door het veelvuldige gebruik en door de ouderdom kan de transmissie slijten en de ATF-olie (Automatic Transmission Fuel) vervuilen. Daardoor kan de automaatbak minder goed gaan werken. In de ergste gevallen weigert de transmissie helemaal te werken.
</p>
<p><strong>Olie verversen. </strong>Adviseert uw automerkfabrikant geen olieverversingsperiode? Dan is ons advies om de olie <u>tussen de 60.000 en de 120.000 </u>te laten vervangen (afhankelijk type auto). Hiermee voorkomt u problemen.
</p>
<p><strong>Spoelen.</strong> De <u>Powerflush methode</u> van Transtune lost de meeste transmissieproblemen op en zorgt ervoor dat er weer zo goed als nieuw geschakeld kan worden. Wat zijn die veelvoorkomende problemen?
</p>
<h2>Veelvoorkomende problemen met de automaatbak</h2>
<p>We hebben het hier niet over mechanische problemen. Want daarvoor zal de bak meestal open moeten en ook echt gerepareerd moeten worden. Powerflush oftewel het spoelen van de bak kan wel vaak een goede oplossing zijn in geval van:
</p>
<p>1.Bonken of slecht schakelen van de automaat
</p>
<p>2.Schommelend toerental
</p>
<p>3.Slippende koppeling
</p>
<p>4.Verhoogd brandstofverbruik
</p>
<p>5.Trillingen bij lage toerental
</p>
<h2>Bonken of slecht schakelen van de automaat</h2>
<p>Heeft u problemen zoals het bonken tijdens het schakelen of slecht schakelen van de automaat? Dan ligt de oplossing waarschijnlijk in het <u>spoelen van de automaatbak</u>. Transtune spoelt uw gehele automaat door. Inclusief de koppelomvormer, de leidingen, de koeler en eventueel de warmtewisselaar. Alle vervuiling en oude olie tappen wij af en vervangen wij door nieuwe van hoge kwaliteit olie.
</p>
<h2>Schommelend toerental</h2>
<p>                                                    Een schommelend toerental komt steeds meer voor. Zelfs bij een koude motor en bij rustig rijgedrag. Ook hiervoor geldt dat preventief spoelen en verversen deze klachten voorkomt en oplost. De oude olie wordt meteen en 100% vervangen door nieuwe. Een leuk weetje is dat dit alles praktisch zonder sleutelen plaats kan vinden. Transtune heeft elk type olie in grote hoeveelheid op voorraad (totaal meer dan 5.000 liter). Daarom hoeft u over de kosten ook géén zorgen te maken. Door de Powerflush voorkomt u niet alleen een schommelend toerental, maar voorkomt u eveneens dure reparaties in de toekomst.
</p>
<h2>Slippende koppeling</h2>
<p>Overmatig slippen tijdens het schakelen of slecht schakelen komt meestal door vervuiling van versnellingsbakolie. Door de combinatie van slijtage en vervuiling raken de oliekanalen verstopt. Dit probleem kan zelfs voorkomen bij een lage kilometerstand ondanks de fabrikant een ‘longlife-garantie geeft op de automatische versnellingsbak. Verstopte en vervuilde oliekanalen kunnen voor problemen zorgen zoals; bonken, slippen of weigeren met als gevolg hoge reparatie kosten. Daarom is het kosten besparend om tijdig uw automaat te laten <u>spoelen (flushen)</u>.
</p>
<h2>Verhoogd brandstof verbruik</h2>
<p><strong>Ondanks Sealed for life.</strong> Merkt u opeens een verhoging in uw brandstofverbruik terwijl u zuinig probeert te rijden? Vervuilde ATF-olie kan hiervoor dé aanleiding zijn. Ondanks dat autofabrikanten de vermelding 'Sealed for life' (onderhoudsvrij) aan bij uw automaat geven, blijkt dat in de praktijk helaas niet zo te zijn. Vervuilde olie kan wel degelijk tot transmissieproblemen leiden.
</p>
<p><strong>Controleer oliepeil en ververs de olie.</strong> Controleer regelmatig het oliepeil van uw auto en ververs deze regelmatig. Zeker bij zwaar gebruik zoals rijden met aanhanger of caravan. Doet u dit niet op tijd zal uw auto slechter gaan schakelen met de ergste gevolgen van dien. Garages bieden dan al snel een vervanging van de versnellingsbak aan wat enorme kosten met zich mee brengt. Kiest u tijdig voor een automaatspoeling, dan bent u stukken goedkoper uit en scheelt enorm veel tijd.
</p>
<p><i>70 % van alle schakelproblemen wordt veroorzaakt door vervuiling. Controleer regelmatig het oliepeil en laat regelmatig de olie verversen. Powerflush (automaatspoelen) werkt preventief én is voor veel problemen de beste oplossing.</i>
</p>
<p><i></i><i></i>
</p>
<hr>
<p><a name="_msocom_1"></a>
</p>
<p><br>
</p>