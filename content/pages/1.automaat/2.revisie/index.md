title: Revisie
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Automaat revisie</h1><p>Problemen met de automatische transmissie komen regelmatig voor. De automaat schakelt schokkerig of ronduit slecht, hij schakelt koud niet goed of de automaat slipt door.</p><p>Bij Transtune in Deurne lossen wij deze problemen vakkundig voor u op! We zijn een jong bedrijf en een vakkundige transmissiespecialist in Nederland. Wij kunnen voor alle automatische versnellingsbakken een complete revisie uitvoeren, zoals DSG en DCT transmissies, ZF versnellingsbakken, Tiptronics en Multitronics en S-tronics. Dit doen we voor alle automerken (met uitzondering van Japanse en Koreaanse merken) en altijd met originele onderdelen.</p><p>Met een automaatrevisie bij Transtune kunt u bovendien veel geld besparen! De officiële merkdealer vraagt al snel 10.000 euro voor het compleet vervangen van de automatische versnellingsbak, terwijl een deelreparatie of revisie de problemen ook vaak kan oplossen.</p><p>We hebben als team meer dan 30 jaar ervaring met het reviseren van automatische transmissies, en u krijgt van ons altijd het beste advies over de slimste oplossing. Dit kan een revisie zijn, maar ook een deelreparatie of spoelen van de automaat.​</p>'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: |
          <h2>De voordelen van automaat revisie bij Transtune</h2><p>Met Transtune kiest u voor betrouwbare kwaliteit. Of het nu
          gaat om een DSG versnellingsbak die kapot is, een ZF 8-traps automaat met
          problemen of een Multitronic die schokt: onze revisiespecialisten werken met de
          nieuwste diagnoseapparatuur en werkplaatsgereedschappen om elk probleem op te
          lossen.<br>
          <br>
          Dit zijn de voordelen van Transtune:&nbsp;</p><ul><li>Gratis
               diagnose in onze werkplaats.</li><li>Eerst
               overleggen wij met u of een revisie wel noodzakelijk is. Veel klanten
               krijgen van hun garage het advies om de automaat te reviseren, terwijl
               spoelen of een deelreparatie óók volstaat. Daarmee bent u dan een stuk
               goedkoper uit.</li><li>We
               werken met de nieuwste technieken en de modernste high tech-apparatuur op
               het gebied van automaat revisie en automaat spoelen.</li><li>U
               profiteert van onze gratis haal en breng service. Dit betekent dat we de
               complete auto ophalen, of de losse versnellingsbak wanneer deze al
               gedemonteerd is.</li><li>Wij
               gebruiken originele olie en onderdelen.</li><li>Uw
               revisie is binnen 5 dagen klaar.</li></ul>
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h2><strong>Werkwijze automaat reviseren&nbsp; </strong></h2><p>&nbsp;Onze werkwijze is als volgt:</p><p>1. Proefrit en diagnose in de werkplaats.</p><p>2. Software uitlezen en controleren op foutcodes.</p><p>3.&nbsp; Automaat
          controleren op aanwezigheid van koelvloeistof.<br>&nbsp; &nbsp;&nbsp;</p><p><strong>Wanneer reviseren noodzakelijk is:</strong></p><p>4. De automaatbak demonteren.</p><p>5. Alle onderdelen controleren en defecte onderdelen
          vervangen (de omvormer, mechatronic,</p><p>koppelingsplaten, pakkingen en alle slijtagedelen worden
          sowieso altijd vervangen).</p><p>6. Stralen en wassen van alle onderdelen in onze
          parelstraalcabine en wasmachine.</p><p>7. Transmissie opnieuw monteren en controleren op de
          testbank.</p><p>8. Uitgebreide proefrit om te bevestigen dat de gehele
          transmissie weer perfect functioneert.</p>
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: |
          <h2>Garantie</h2><p>Bij Transtune geloven we in kwaliteit. We vervangen alle
          transmissieonderdelen door originele onderdelen en gebruiken alleen originele
          olie. U krijgt daarom twee jaar garantie op een revisie die is uitgevoerd door
          Transtune! Zonder revisie krijgt u op het vervangen van defecte onderdelen drie
          maanden garantie.</p><p>&nbsp;Meer weten? Maak vandaag nog een afspraak of neem contact op
          voor meer informatie. U vindt ons in Deurne in Noord-Brabant, vlakbij Venlo en
          Eindhoven. We voeren automaat versnellingsbak revisies uit voor klanten in heel
          Nederland en België!</p><p>Wij voeren automaat revisie uit voor de volgende merken:<br></p><p><a href="https://transtune.nl/automaat-revisie/cars/abarth">Abarth</a>, <a href="https://transtune.nl/automaat-revisie/cars/alfa-romeo">Alfa-Romeo</a>, <a href="https://transtune.nl/automaat-revisie/cars/alpina">Alpina</a>, <a href="https://transtune.nl/automaat-revisie/cars/aston-martin">Aston Martin</a>, <a href="https://transtune.nl/automaat-revisie/cars/audi">Audi</a>, <a href="https://transtune.nl/automaat-revisie/cars/bmw">BMW</a>, <a href="https://transtune.nl/automaat-revisie/cars/bentley">Bentley</a>, <a href="https://transtune.nl/automaat-revisie/cars/buick">Buick</a>, <a href="https://transtune.nl/automaat-revisie/cars/cadillac">Cadillac</a>, <a href="https://transtune.nl/automaat-revisie/cars/chevrolet">Chevrolet</a>, <a href="https://transtune.nl/automaat-revisie/cars/chrysler">Chrysler</a>, <a href="https://transtune.nl/automaat-revisie/cars/citroen">Citroën</a>, <a href="https://transtune.nl/automaat-revisie/cars/ds">DS</a>, <a href="https://transtune.nl/automaat-revisie/cars/dodge">Dodge</a>, <a href="https://transtune.nl/automaat-revisie/cars/ferrari">Ferrari</a>, <a href="https://transtune.nl/automaat-revisie/cars/fiat">Fiat</a>, <a href="https://transtune.nl/automaat-revisie/cars/ford">Ford</a>, <a href="https://transtune.nl/automaat-revisie/cars/gmc">GMC</a>, <a href="https://transtune.nl/automaat-revisie/cars/honda">Honda</a>, <a href="https://transtune.nl/automaat-revisie/cars/hummer">Hummer</a>, <a href="https://transtune.nl/automaat-revisie/cars/infiniti">Infiniti</a>, <a href="https://transtune.nl/automaat-revisie/cars/jaguar">Jaguar</a>, <a href="https://transtune.nl/automaat-revisie/cars/jeep">Jeep</a>, <a href="https://transtune.nl/automaat-revisie/cars/kia">Kia</a>, <a href="https://transtune.nl/automaat-revisie/cars/lamborghini">Lamborghini</a>, <a href="https://transtune.nl/automaat-revisie/cars/land-rover">Land Rover</a>, <a href="https://transtune.nl/automaat-revisie/cars/lexus">Lexus</a>, <a href="https://transtune.nl/automaat-revisie/cars/lincoln">Lincoln</a>, <a href="https://transtune.nl/automaat-revisie/cars/lotus">Lotus</a>, <a href="https://transtune.nl/automaat-revisie/cars/mg">MG</a>, <a href="https://transtune.nl/automaat-revisie/cars/maserati">Maserati</a>, <a href="https://transtune.nl/automaat-revisie/cars/mazda">Mazda</a>, <a href="https://transtune.nl/automaat-revisie/cars/mclaren">MCLaren</a>, <a href="https://transtune.nl/automaat-revisie/cars/mercedes-benz">Mercedes-Benz</a>, <a href="https://transtune.nl/automaat-revisie/cars/mercury">Mercury</a>, <a href="https://transtune.nl/automaat-revisie/cars/mini">Mini</a>, <a href="https://transtune.nl/automaat-revisie/cars/mitsubishi">Mitsubishi</a>, <a href="https://transtune.nl/automaat-revisie/cars/nissan">Nissan</a>, <a href="https://transtune.nl/automaat-revisie/cars/opel">Opel</a>, <a href="https://transtune.nl/automaat-revisie/cars/peugeot">Peugeot</a>, <a href="https://transtune.nl/automaat-revisie/cars/pontiac">Pontiac</a>, <a href="https://transtune.nl/automaat-revisie/cars/porsche">Porsche</a>, <a href="https://transtune.nl/automaat-revisie/cars/renault">Renault</a>, <a href="https://transtune.nl/automaat-revisie/cars/rover">Rover</a>, <a href="https://transtune.nl/automaat-revisie/cars/saab">Saab</a>, <a href="https://transtune.nl/automaat-revisie/cars/seat">Seat</a>, <a href="https://transtune.nl/automaat-revisie/cars/skoda">Skoda</a>, <a href="https://transtune.nl/automaat-revisie/cars/smart">Smart</a>, <a href="https://transtune.nl/automaat-revisie/cars/ssangyong">SsangYong</a>, <a href="https://transtune.nl/automaat-revisie/cars/subaru">Subaru</a>, <a href="https://transtune.nl/automaat-revisie/cars/suzuki">Suzuki</a>, <a href="https://transtune.nl/automaat-revisie/cars/toyota">Toyota</a>, <a href="https://transtune.nl/automaat-revisie/cars/vauxhall">Vauxhall</a>, <a href="https://transtune.nl/automaat-revisie/cars/volkswagen">Volkswagen</a>, <a href="https://transtune.nl/automaat-revisie/cars/volvo">Volvo</a></p>
hero-img: /assets/img/dsc_0054.jpg
banner_size: is-large
subtitle: 'Selecteer uw auto en bekijk direct de prijs voor het reviseren van uw automaatbak'
has_cta: true
cta_title: 'Vul de infobox op deze pagina in met uw gegevens'
cta_subtitle: 'of neem contact met ons op'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: automaat-revisie
seo:
  title: 'Automaat Revisie - Gratis Diagnose - Transtune.nl'
  description: 'Laat uw automaat vakkundig reviseren door onze specialisten. Vul in onze handige infobox uw auto in en maak een afspraak!'
  priority: '1.0'
fieldset: default
id: fee89d2d-dad0-43f1-a99b-384411f0f04f
