title: 'VAG groep'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>De VAG GROEP</h1><p>Zit uw auto in de vag groep? Check hieronder uw transmissie en vraag een offerte aan.</p><h1>AUDI</h1>'
      -
        type: pages
        pages:
          - 769cb048-f0af-460e-9786-8e7ba2fa75b9
          - 1793d6e3-21fe-4aac-8f74-39ade6aa9a45
          - 1991171f-fa42-482f-b87d-db9edb7b4b0b
          - 5859542e-8349-4b36-a8aa-9d8730dc047e
          - 3be8a4bd-9eae-4529-82f8-00052192f5b2
      -
        type: pages
        pages:
          - acd87d22-15bd-490d-a536-d40c846ccf28
          - 62555723-1810-4091-bdf9-a6ad96ae9ca9
          - 1245d8ef-7958-4835-8483-ff79e595158f
          - 6234cde6-ff53-44fb-8d82-0f9583d8b15b
          - 4f23d353-3f4e-49f3-acee-5df93a3e50ad
      -
        type: text
        text: '<p><br></p>'
      -
        type: pages
        pages:
          - 7acff763-f8b1-48e1-a987-95099b8e2469
      -
        type: text
        text: '<h1><br></h1><h1>Volkswagen</h1>'
      -
        type: pages
        pages:
          - 769cb048-f0af-460e-9786-8e7ba2fa75b9
          - 1793d6e3-21fe-4aac-8f74-39ade6aa9a45
          - 4e0570ca-3f1e-4abb-9e2c-bd07832ee511
          - fbd518ec-35d9-4f58-9d83-ed18ee40a1d5
          - 3be8a4bd-9eae-4529-82f8-00052192f5b2
      -
        type: text
        text: '<h1><br></h1><h1>SEAT<br></h1>'
      -
        type: pages
        pages:
          - 769cb048-f0af-460e-9786-8e7ba2fa75b9
          - 1793d6e3-21fe-4aac-8f74-39ade6aa9a45
      -
        type: text
        text: '<h1><br></h1><h1>Skoda</h1>'
      -
        type: pages
        pages:
          - 769cb048-f0af-460e-9786-8e7ba2fa75b9
          - 1793d6e3-21fe-4aac-8f74-39ade6aa9a45
      -
        type: text
        text: '<h1><br></h1><h1>Lamborghini</h1>'
      -
        type: pages
        pages:
          - 6234cde6-ff53-44fb-8d82-0f9583d8b15b
      -
        type: text
        text: '<h1><br></h1><h1>Porsche</h1>'
      -
        type: pages
        pages:
          - acd87d22-15bd-490d-a536-d40c846ccf28
          - 1245d8ef-7958-4835-8483-ff79e595158f
has_cta: false
menu: none
is_hidden: true
fieldset: default
id: a91f17c7-99d3-49bb-aa37-9151e587cebf
