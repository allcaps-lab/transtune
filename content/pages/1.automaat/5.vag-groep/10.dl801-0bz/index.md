---
title: 'DL801 (0BZ)'
fieldset: default
id: 6234cde6-ff53-44fb-8d82-0f9583d8b15b
---
<p>0BZ (DL801, S-Tronic)
</p>
<p>Wet dual clutch 7-speed longitudinal AWD (electronic control)
</p>
<p>Fitted to Audi R8 Type 42 & 4S
</p>