title: 'Automaat Bonkt'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h1><strong>Automaat bonkt</strong>&nbsp;</h1><p>Een automatisch transmissie moet altijd soepel schakelen.
          Dat betekent vlot en soepel opschakelen en keurig terugschakelen zonder dat je
          het merkt. Veel transmissie gaan echter bonken bij het schakelen als ze niet
          goed worden onderhouden. Dit bonken en schokken kan plaatsvinden met de
          automaatbak in 'R', bij het wegrijden, opschakelen, terugschakelen of bij het
          accelereren.&nbsp;&nbsp;</p><h2><strong>ATF-olie</strong></h2><p>De oorzaak van een automaat die bonkt is vaak slijtage en
          vervuiling van de ATF-olie in de transmissie. Deze olie zorgt ervoor dat alle
          onderdelen soepel samenwerken en is cruciaal voor een probleemloos
          schakelgedrag. Bij hogere kilometerstanden, grofweg vanaf 80.000 kilometer, kan
          de olie echter verouderen en vervuild zijn waardoor hij zijn smerende werking verliest.&nbsp;</p><h2><strong>Automaatbak spoelen</strong></h2><p>Hierdoor kunnen verschillende problemen ontstaan, waaronder
          dus het bekende bonken of schokken van de automaatbak. Gelukkig is er een
          goede, betaalbare oplossing voor dit probleem: de automaatbak spoelen. Bij
          Transtune beschikken we over alle expertise en de juiste hightech apparatuur om
          de automatische transmissie van uw auto te spoelen.&nbsp;</p><h2><strong>Powerflush</strong></h2><p>Wij spoelen uw transmissie met ongeveer 15 liter
          transmissieolie via de bekende Powerflush-methode. In de automaatbak zelf zit
          ongeveer 8 liter olie, de overige olie wordt gebruikt om het complete systeem
          te reinigingen waaronder de koppelomvormer (indien aanwezig), de oliekoeler en
          de leidingen. We vervangen dus álle olie en niet slechts een deel zoals bij
          veel garagebedrijven gebeurt. Dit zorgt ervoor dat er geen oude olie
          achterblijft, en dat uw complete transmissie wordt voorzien van nieuwe,
          originele olie.</p>
hero-img: /assets/img/img_1348.jpg
banner_size: is-medium
subtitle: 'heeft u last van een bonkende automaat'
has_cta: true
cta_title: 'Vul de infobox in met uw gegevens en bekijk uw opties'
cta_subtitle: 'OF MAIL NAAR INFO@TRANSTUNE.NL'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
seo:
  title: 'Automaat bonkt? Transtune lost uw automaat probleem op!'
  description: 'Transtune is uw specialist voor de automaat problemen voor alle type auto''s. Eerlijk en betrouwbaar!'
fieldset: default
id: 8b30e341-ace3-471b-a19a-7e2c1f5ad058
