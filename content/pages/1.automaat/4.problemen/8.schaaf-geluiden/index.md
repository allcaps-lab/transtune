title: 'Schaafgeluiden Automaat'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h1><strong>Schaafgeluiden</strong>&nbsp;Automaat</h1><p>Een metaalachtig geluid bij het optrekken of een schaaf
          geluid in de auto kan duiden op een defect in de automatische transmissie. Een
          automaatbak hoort namelijk zonder kabaal zijn werk te doen. Elke transmissie is
          namelijk ontworpen en gebouwd om geruisloos en soepel te functioneren voor een
          comfortabel rijgedrag.&nbsp;</p><h2><strong>Te weinig onderhoud</strong></h2><p>Toch kan ook een automaatbak problemen gaan vertonen, zeker
          wanneer er sprake is van te weinig onderhoud. Veel auto eigenaren denken dan
          'hoezo, de automaatbak van mijn auto is gesealed dus er is helemaal geen
          onderhoud nodig'. Helaas valt dat in de praktijk tegen. Alle bewegende
          onderdelen hebben onderhoud nodig en een transmissie dus ook. Het moment waarop
          dit moet gebeuren is echter niet voorgeschreven. Sommige transmissies gaan
          probleemloos 15 jaar en 100.000 kilometer of meer mee, terwijl een andere
          transmissie al na 5 jaar en 10.000 kilometer open moet voor onderhoud.&nbsp;&nbsp;</p><h3><strong>Is spoelen de oplossing?</strong></h3><p>Veel problemen met de automatische transmissie kunnen we
          oplossen door deze te <a href="{{ link:644780c6-677c-445a-bde9-5600aae8527e }}">spoelen</a>. Een uitzondering is als er schaafgeluiden optreden
          in de transmissie of een metaalachtig geluid te horen is bij&nbsp; optrekken. Dit betekent meestal dat de lagers
          van de transmissie versleten zijn en dan is reparatie of vervangen de enige
          optie.&nbsp;</p><h3><strong>Kom naar Transtune voor een diagnose</strong></h3><p>Voordat het zover is neemt u het beste <a href="{{ link:de627bca-7595-429e-9b41-ad58703916d7 }}">contac</a>t op met
          Transtune voor een uitgebreide diagnose. We hebben zoveel ervaring met
          transmissies dat we met onze hightech diagnoseapparatuur én ouderwets
          vakmanschap tijdens een proefrit precies kunnen achterhalen wat het probleem is
          met de transmissie van uw auto. Op basis van deze diagnose zullen we u een
          oplossing voorstellen. Dit kan alsnog spoelen zijn, maar ook repareren,
          <a href="{{ link:7210b9f8-4ed6-7008-8545-c85658fdbe01 }}">reviseren</a> of volledig vervangen van de automaatbak.</p>
hero-img: /assets/img/img_1348.jpg
banner_size: is-medium
subtitle: 'Schaafgeluiden automaat'
has_cta: true
cta_title: 'Vul de infobox in met uw gegevens en bekijk uw opties'
cta_subtitle: 'OF MAIL NAAR INFO@TRANSTUNE.NL'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
seo:
  title: 'Geeft u automaat schaafgeluiden? Transtune lost deze voor u op!'
  description: 'Transtune is uw specialist voor de automaat problemen voor alle type auto''s. Eerlijk en betrouwbaar!'
fieldset: default
id: da672082-59d0-44b9-a00f-3ab76ccce4c7
