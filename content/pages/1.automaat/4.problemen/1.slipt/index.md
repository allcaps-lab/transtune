title: 'Slippen van de Automaat'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h1><strong>Koppeling slipt</strong>&nbsp;</h1><p>Een slippende koppeling. Het is een probleem waar we
          allemaal wel eens mee te maken krijgen. Maar wat is er dan eigenlijk aan de
          hand? <br><br>Het meest bekende symptoom van een slippende koppeling bij
          een handgeschakelde auto merk je bij het accelereren. De motor maakt wel meer
          toeren, maar de auto versnelt trager dan je gewend bent. Dit kan te maken
          hebben met versleten koppelingsplaten, maar er kunnen ook andere oorzaken zijn.</p><h2><strong>Slijtage en vervuiling</strong></h2><p>Bij een automatische transmissie die is voorzien van koppelingsplaten,
          waaronder DSG automatische transmissies, kan in principe hetzelfde probleem
          ontstaan. Toch worden de meeste problemen met een slippende automaatbak niet
          eens veroorzaakt door versleten koppelingsplaten, maar door vervuiling van de
          versnellingsbakolie. Door slijtage en vervuiling raken de oliekanalen verstopt
          en wordt het systeem niet meer goed gesmeerd.&nbsp;</p><h2><strong>Laat uw automaat spoelen</strong></h2><p>De beste oplossing is om uw automaat te laten spoelen door
          Transtune. Dit doen wij met onze Powerflush machine. Dit is een moderne methode
          om de volledige automaat te spoelen waarbij we alle ATF-olie, dus inclusief
          alle leidingen, de koppelomvormer, de mechatronic en de oliekoeler, verversen.
          Dit is beter dan de ouderwetse manier waarbij slechts een klein deel van de olie
          wordt vervangen.&nbsp;</p><h2><strong>Wacht niet tot het te laat is</strong></h2><p>Wilt u problemen met de automatische transmissie voorkomen?
          Laat deze dan preventief spoelen bij transtune. Wij flushen bijna 2.000
          automaten per jaar voor autobedrijven, leasemaatschappijen en particulieren. Én
          voor een uiterst aantrekkelijke prijs!</p>
      -
        type: button_link
        link: 644780c6-677c-445a-bde9-5600aae8527e
        button_text: 'Laat uw automaat spoelen'
hero-img: /assets/img/img_1348.jpg
banner_size: is-medium
subtitle: 'Heeft u een last van een slippende koppeling?'
has_cta: true
cta_title: 'Vul de infobox in met uw gegevens en bekijk uw opties'
cta_subtitle: 'OF MAIL NAAR INFO@TRANSTUNE.NL'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
seo:
  title: 'Slippende koppeling? Transtune lost uw automaat probleem op!'
  description: 'Transtune is uw specialist voor de automaat problemen voor alle type auto''s. Eerlijk en betrouwbaar!'
fieldset: default
id: 7dc05eaa-56e7-4ab0-8d02-d7e6794eb4f6
