title: Problemen
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Automaat problemen</h2><p><em>Veel garages adviseren een revisie van de versnellingsbak terwijl een professionele spoeling vaak ook kan volstaan. Wat zijn de meest voorkomende transmissieproblemen?</em></p>'
      -
        type: pages
        pages:
          - 7dc05eaa-56e7-4ab0-8d02-d7e6794eb4f6
          - bd5a1225-5e8f-4241-878a-e8dc3539bf1e
          - 0ed646bd-7060-44f8-bc04-4aaee297f874
          - 8b30e341-ace3-471b-a19a-7e2c1f5ad058
      -
        type: pages
        pages:
          - 52be200d-9232-431d-875c-90e0ba9d1831
          - b51221fe-7dc8-41d4-bb65-49792c67e76e
          - 3c22daf4-bd57-4001-ac16-eaf71a2db70d
          - da672082-59d0-44b9-a00f-3ab76ccce4c7
      -
        type: text
        text: '<p><strong><br>De automaatbak</strong></p><p>De versnellingsbak is een zeer belangrijk onderdeel van uw auto. De automaatbak voert enorm veel handelingen uit. Denk aan optrekken, afremmen en de vele duizenden schakelingen. Wordt er met enige regelmaat een aanhangwagen of een caravan worden getrokken, dan wordt de vak nog vele malen meer belast.<br><br></p><p><strong>De gevolgen van gebruik en ouderdom</strong></p><p>Door het veelvuldige gebruik en door de ouderdom kan de transmissie slijten en de ATF-olie (Automatic Transmission Fuel) vervuilen. Daardoor kan de automaatbak minder goed gaan werken. In de ergste gevallen weigert de transmissie helemaal te werken.</p><p><strong>Olie verversen<br></strong>Adviseert uw automerkfabrikant geen olieverversingsperiode? Vaak adviseert de fabrikant van de automaatbak zoals ZF/Aisin warner een verversing termijn, echter de fabrikant van de auto geeft aan dit is longlife olie is. Dan is ons advies om de olie&nbsp;<u>tussen de 60.000 en de 120.000&nbsp;</u>te laten vervangen (afhankelijk type auto). Hiermee voorkomt u problemen.<br><br></p><p><strong>Spoelen.</strong>&nbsp;<a href="{{ link:644780c6-677c-445a-bde9-5600aae8527e }}">De&nbsp;<u>Powerflush methode</u></a>&nbsp;van Transtune lost de meeste transmissieproblemen op en zorgt ervoor dat er weer zo goed als nieuw geschakeld kan worden. Wat zijn die veelvoorkomende problemen?</p>'
hero-img: /assets/img/dsc04316133-1554745733.jpg
banner_size: is-medium
subtitle: 'Heeft u problemen met de automaat?'
has_cta: true
cta_title: 'Automaat problemen'
cta_subtitle: 'Automaat problemen'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Neem contact met ons op'
is_hidden: false
seo:
  title: 'Automaat Problemen? - Transtune lost deze voor U op!'
  description: 'Transtune is specialist op gebied van automaat problemen. Bonkt, slipt, hapert u automaat? Geen Probleem! Wij lossen dit voor u op! Maak snel een afspraak!'
fieldset: default
id: 91456910-4b4e-4b6b-8b56-943eb6c08c47
