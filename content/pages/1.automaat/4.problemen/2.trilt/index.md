title: 'Trillen van de Automaat'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h1><strong>Automaat trilt</strong></h1><p>Een trillende of vibrerende automatische transmissie is
          vervelend. Het is niet alleen slecht voor de techniek, het verpest ook het
          rijplezier! Je voelt het stuur trillen en het is vaak net alsof de automaat
          tussen twee versnellingen blijft hangen. Ook kan het gebeuren dat met de
          automaat in D en de voet op de rem de auto gaat trillen.&nbsp;</p><h2><strong>Koppelomvormer</strong></h2><p>Als uw automaat trilt bij lage toerentallen zit het probleem
          meestal in de koppelomvormer. Typische problemen met een <a href="{{ link:432bc31d-d77d-401e-95df-5491044494d4 }}">defecte koppelomvormer</a>
          zijn schokkerig schakelen of trillingen in de aandrijflijn. En soms wil de auto
          helemaal niet meer wil rijden ook al staat de hendel van de transmissie in D of
          R.&nbsp;</p><h3><strong>Vooral bij sterke dieselmotoren</strong></h3><p>Een trillende automaat door een defecte koppelomvormer komt
          vaak voor bij de sterkere dieselmotoren. Veel garages bieden dan aan om de
          gehele transmissie, inclusief de koppelomvormer, te spoelen maar vaak is dit
          slechts een lapmiddel en keert het probleem na enkele duizenden kilometers weer
          terug.&nbsp;</p><h3><strong>Laat een diagnose stellen</strong></h3><p>Wilt u hier zekerheid over? Voor een professionele diagnose
          gaat u naar de transmissiespecialisten van Transtune! Met onze jarenlange
          kennis zijn we in staat snel de oorzaak van een probleem te vinden en een
          oplossing te vinden. Wij kunnen de koppelomvormer voor u repareren, reviseren
          of volledig vervangen.</p>
      -
        type: button_link
        link: 432bc31d-d77d-401e-95df-5491044494d4
        button_text: 'Koppelomvormer vervangen'
hero-img: /assets/img/img_1348.jpg
banner_size: is-medium
subtitle: 'Automaat problemen? Automaat'
has_cta: true
cta_title: 'Vul de infobox in met uw gegevens en bekijk uw opties'
cta_subtitle: 'OF MAIL NAAR INFO@TRANSTUNE.NL'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
seo:
  title: 'Last van een trillende koppeling? Transtune lost uw automaat probleem op!'
  description: 'Transtune is uw specialist voor de automaat problemen voor alle type auto''s. Eerlijk en betrouwbaar!'
fieldset: default
id: bd5a1225-5e8f-4241-878a-e8dc3539bf1e
