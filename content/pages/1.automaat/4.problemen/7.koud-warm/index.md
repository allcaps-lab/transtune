title: 'Koud/warm Automaat'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h1><strong>Automaat Koud/Warm</strong></h1><h2><strong>Automaat schakelt slecht bij warme motor</strong></h2><p>Wanneer de automaat van uw auto bij koude motor goed
          schakelt, maar bij een warme motor slecht schakelt dan is er waarschijnlijk een
          probleem met de <a href="{{ link:ffd9b037-6193-4de3-a019-7b62259ecd04 }}">mechatronic.</a> Bekend is dat de automaat dan <a href="{{ link:7dc05eaa-56e7-4ab0-8d02-d7e6794eb4f6 }}">slipt</a> of <a href="{{ link:8b30e341-ace3-471b-a19a-7e2c1f5ad058 }}">bonkt</a>. En als
          u schakelt van 'R' naar 'D' dan gaat dit vaak gepaard met een klap. De oorzaak
          van het probleem is dat het aluminium waar de mechatronic van gemaakt te veel
          uitzet bij een warme motor. De unit raakt hierdoor ontregeld, en dat merk je in
          het schakelen.&nbsp;</p><h3><strong>Mechatronic regelt het schakelen</strong></h3><p>De mechatronic is namelijk het elektronische brein dat het
          schakelgedrag regelt van automatische transmissies. Dit geldt voor
          versnellingsbakken met een dubbele koppeling (bijvoorbeeld DSG, S-tronic, DCT,
          PDK en Powershift) en ook voor automatische transmissies met een koppelomvormer
          zoals die van ZF, Aisin AW, GM en Jatco.&nbsp;</p><h3><strong>Maak een afspraak bij Transtune voor een diagnose</strong></h3><p>Dit typische probleem van een automaat die slecht schakelt
          bij een warme motor, en goed bij een koude motor,&nbsp; kan alleen worden opgelost door de mechatronic
          te vervangen, te reviseren of te repareren. Dit is afhankelijk van de ernst van
          het probleem. We vervangen de mechatronic alleen wanneer revisie of repareren
          niet mogelijk is of financieel gezien niet zinvol. In alle andere gevallen
          zullen we reparatie of revisie adviseren om de kosten zo laag mogelijk te
          houden.</p>
hero-img: /assets/img/img_1348.jpg
banner_size: is-medium
subtitle: 'Problemen met koud/warm schakelen'
has_cta: true
cta_title: 'Vul de infobox in met uw gegevens en bekijk uw opties'
cta_subtitle: 'OF MAIL NAAR INFO@TRANSTUNE.NL'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
seo:
  title: 'Problemen met het schakelen met warm/koude motor? Transtune helpt u!'
  description: 'Transtune is uw specialist voor de automaat problemen voor alle type auto''s. Eerlijk en betrouwbaar!'
fieldset: default
id: 3c22daf4-bd57-4001-ac16-eaf71a2db70d
