title: 'BMW Groep'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>De BMW GROEP</h1><p>Zit uw auto in de bmw groep? Check hieronder uw tranmissie en vraag een offerte aan.</p>'
      -
        type: pages
        pages:
          - 24ea06ca-ba87-4eac-8eb3-aaa521747873
          - 5847afe1-443c-478f-b527-89a17db1de66
          - ee3242b9-e359-46c5-b68d-83e7747ad280
          - 63ff5dd3-27c4-44e5-b112-735e19203748
          - cb42dd9e-5af0-4cfa-adfa-37576b92686a
has_cta: false
menu: none
is_hidden: true
fieldset: default
id: 8e1a30e6-6af6-459c-b7b6-2d4435049788
