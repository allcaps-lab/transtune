title: 'Panamera Turbo 971 Downpipe set (SS)'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche Panamera Turbo (971)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Downpipe set (SS) -&nbsp;</strong><strong>700PK / 1000NM</strong></p><p>Voor de ultieme gewichtsreductie en afstemmingspotentieel biedt F1-performance roestvrijstalen, katloze downpipe set om van de Porsche een nog krachtigere machine te maken. Deze opstelling is bedoeld voor toegewijde autoliefhebbers, en zal bijdragen aan de algehele ervaring van wat al een verbluffende performance-auto is voor elke veeleisende bestuurder. Deze prachtig vervaardigde Downpipe set leveren geweldig geluid zonder ongewenst cabinegeluid.&nbsp;</p><p>Te behalen vermogen: <strong>700PK / 1000NM</strong></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.495,- incl. 21% BTW</strong></p>'
              -
                type: carousel
                slides:
                  - /assets/porsche-panamera-turbo-971-downpipe-1-1574194525.JPG
                  - /assets/porsche-panamera-turbo-971-downpipe-1574194532.JPG
                carousel_height: 350
                carousel_width: 622
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Order here!'
hero-img: /assets/img/porsche-panamera-turbo-971-downpipe-1.JPG
banner_size: is-fullheight
subtitle: F1-Performance
has_cta: true
cta_title: 'Staat uw auto er niet tussen?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
seo:
  title: 'F1-performance Downpipes for Porsche Panamera Turbo 971'
  image: /assets/porsche-panamera-turbo-971-downpipe-1-1574194321.JPG
fieldset: default
id: da585c99-6ec8-407f-b964-be732e554a59
