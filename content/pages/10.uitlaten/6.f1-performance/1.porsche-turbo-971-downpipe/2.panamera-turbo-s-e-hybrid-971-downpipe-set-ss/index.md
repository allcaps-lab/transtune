title: 'Panamera Turbo S E-Hybrid 971 Downpipe set (SS)'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche Panamera Turbo S E-hybrid (971)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Downpipe set (SS) - 800</strong><strong>PK / 1100NM</strong></p><p>Voor de ultieme gewichtsreductie, afstemmingspotentieel en racegeluid biedt F1-performance roestvrijstalen, katloze downpipe set om van de Porsche een nog krachtigere machine te maken. Deze opstelling is bedoeld voor toegewijde baanauto''s en liefhebbers, en het zal ook bijdragen aan de algehele ervaring van wat al een verbluffende performance-auto is voor elke veeleisende bestuurder. Deze prachtig vervaardigde Downpipe set leveren geweldig geluid zonder ongewenst cabineluid.</p><p>Te behalen vermogen: <strong>800PK / 1100NM</strong></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.495,- incl. 21% BTW</strong></p>'
              -
                type: carousel
                slides:
                  - /assets/porsche-panamera-turbo-971-downpipe-1-1574195393.JPG
                  - /assets/porsche-panamera-turbo-971-downpipe-1574195396.JPG
                carousel_height: 350
                carousel_width: 622
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Order here!'
hero-img: /assets/img/porsche-panamera-turbo-971-downpipe.JPG
banner_size: is-fullheight
subtitle: F1-performance
has_cta: true
cta_title: 'Staat uw auto er niet tussen?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: 7c91d860-b332-4b8b-b3ff-75cce349e702
