title: Abarth
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Abarth 595/595 Pista/Competizione&nbsp;<br></h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line with Carbon tailpipes</strong></p><p>Kies voor een RVS slip-on uitlaatsysteem en maak de Abarth nog sneller, nog responsiever, en geef het een nog opvallendere look en dat onmiskenbare sportieve Akrapovič-geluid. Maak de look af met een set prachtige 115 mm eindpijpen in titanium of koolstofvezel.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1623,82 incl. &nbsp;21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/6999afb0-78e7-40a1-879a-84d6a86d3e9a.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Abarth 500/500C</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line with Carbon tailpipes</strong></p><p>Kies voor een RVS slip-on uitlaatsysteem en maak de Abarth (1.4 16V T Jet) nog sneller, nog responsiever, en geef het een nog opvallendere look en dat onmiskenbare sportieve Akrapovič-geluid.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1623,82 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/a90bc1a5-f1ef-41f5-8ce1-8b5ad7d44837.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Abarth 595/595C/Turismo</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line with Carbon tailpipes</strong></p><p>Kies voor een RVS slip-on uitlaatsysteem en maak de Abarth nog sneller, nog responsiever, en geef het een nog opvallendere look en dat onmiskenbare sportieve Akrapovič-geluid. Maak de look af met een set prachtige 115 mm eindpijpen in titanium of koolstofvezel.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1626,24 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/6999afb0-78e7-40a1-879a-84d6a86d3e9a-2.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/img_6456.jpg
banner_size: is-fullheight
subtitle: 'Akrapovic uitlaatsystemen voor'
has_cta: true
cta_title: 'Staat uw type auto er niet tussen?'
cta_subtitle: 'Vul het contactformulier in en wij nemen zo spoedig mogelijk contact op'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: 67a99dc6-5f2e-4aa6-9fbe-1c5b330015ff
