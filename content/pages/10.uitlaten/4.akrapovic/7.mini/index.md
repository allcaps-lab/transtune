title: Mini
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Mini Cooper S (R56)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (SS)</strong></p><p>Het uitlaatsysteem van hoogwaardig roestvrij staal draagt ​​nog meer bij aan de prestaties en plezier. Dit systeem bestaat uit een geoptimaliseerde verbindingspijp met een resonator en achterste uitlaatdemper. Het verbetert de prestaties, voegt kracht en koppel toe en verbetert de responsiviteit van de viercilindermotor, vooral in het middenbereik van de toeren. Het prestatie-uitlaatsysteem heeft een sportieve en agressieve toon, zonder ongewenst gedreun, dat past bij het karakter van de auto waarvoor het is ontworpen. Maak de look af met een set van twee prachtige 95 mm eindpijpen in titanium of koolstofvezel. Optionele downpipe met sportkatalysator voor nog meer vermogen.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.780,35 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mini-cooper-s-r56.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/mini_jcw_23.jpg
banner_size: is-fullheight
subtitle: 'Akrapovic uitlaatsystemen voor'
has_cta: true
cta_title: 'STAAT UW AUTO ER NIET TUSSEN?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: 2c5e81df-3cc5-4c97-86a6-8852b4139cab
