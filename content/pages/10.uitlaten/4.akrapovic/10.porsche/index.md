title: Porsche
page_content:
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche Carrera (992) 2019&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: |
                  <p><strong>Slip-on Line Race (titanium)</strong></p><p>De Slip-On Race Line van Akrapovič is de eerste stap in het afstemmingsproces van het uitlaatsysteem en kan eenvoudig worden geïnstalleerd op het originele systeem van de Porsche Carrera. Gebouwd van hoogwaardig titanium en afgewerkt met twee opvallende dubbele ronde uitlaatpijpen, maakt dit instapsysteem de Carrera krachtiger en lichter en geeft de auto dat unieke, sportieve Akrapovič-geluid.&nbsp;
                  </p><p>Het Slip-On Race-systeem kan optioneel worden gecombineerd met Link Pipe Set (Titanium).
                  
                  Montagemelding: het systeem is ontworpen om het Porsche sportuitlaatsysteem te vervangen.</p>
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€4.573,80 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/akrapovic-porsche-carrera-992-1572682842.jpg
      -
        type: button_link
        link: e6328b2c-7102-4907-befe-88c8fc15f72c
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche 911 Carrera /S/4/4S/GTS (991.2)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Gebouwd uit hoogwaardige, hittebestendige, ultralichte titaniumlegeringen - die gezandstraald en gecoat zijn voor duurzaamheid - is de Slip-On Line de eerste stap in uitlaatmodificatie. Met een gewichtsbesparing van meer dan 35% op voorraad, terwijl de prestaties worden verbeterd, maakt de uitlaat gebruik van de nieuwste innovatieve technologieën in de constructie, inclusief onderdelen die zijn gegoten in de eigen gieterij van Akrapovič. Het is perfect afgestemd om een ​​uniek geluid te geven aan de boxermotor met turbocompressor en is afgewerkt met twee prachtige uitlaatpijpen om het esthetische plezier te vergroten. Ontworpen om het Porsche sportsysteem te vervangen, kan de Slip-On Line ook worden gebruikt met de optionele roestvrijstalen Downpipeset om het geluid en de prestaties verder te verbeteren.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€5.658,94 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-911-carrera--s-4-4s-gts.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche 911 Carrera Cabriolet /S/4/4S/GTS (991.2)&nbsp;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (titanium)</strong></p><p>Gebouwd uit hoogwaardige, hittebestendige, ultralichte titaniumlegeringen - die gezandstraald en gecoat zijn voor duurzaamheid - is de Slip-On Line de eerste stap in uitlaatmodificatie. Met een gewichtsbesparing van meer dan 35% op voorraad, terwijl de prestaties worden verbeterd, maakt de uitlaat gebruik van de nieuwste innovatieve technologieën in de constructie, inclusief onderdelen die zijn gegoten in de eigen gieterij van Akrapovič. Het is perfect afgestemd om een ​​uniek geluid te geven aan de boxermotor met turbocompressor en is afgewerkt met twee prachtige uitlaatpijpen om het esthetische plezier te vergroten. Ontworpen om het Porsche sportsysteem te vervangen, kan de Slip-On Line ook worden gebruikt met de optionele roestvrijstalen verbindingspijpset om het geluid en de prestaties verder te verbeteren.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€5.658,94 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-911-carrera-cabriolet.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche 911 GT3 (991) / (991.2)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Race Line (Titanium)</strong></p><p>De Slip-On Race Line van Akrapovič is de eerste stap in het afstemmingsproces van het uitlaatsysteem en kan eenvoudig worden geïnstalleerd op het originele systeem van de Porsche GT3. Gebouwd van hoogwaardig titanium en afgewerkt met twee opvallende dubbele ronde uitlaatpijpen, maakt dit instapsysteem de GT3 krachtiger en lichter en geeft de auto dat unieke, sportieve Akrapovič-geluid. (optioneel uit te breiden met Link pipe set, of Evolution race header set)</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.395,80 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-911-gt3-(991).png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche 911 GT3 (991) / (991.2)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Race Header Set (Titanium)</strong></p><p>Evolution Race Header Set converteert het slip-on-systeem naar een volledig systeem, gebouwd voor het circuit en geeft de Porsche GT3 de extra prestaties die daarbij horen. Handgemaakte titanium headers bevatten 100 cpsi sportkatalysatoren en titanium flenzen - gegoten in de eigen gieterij van Akrapovič. Het systeem biedt een perfecte vermogenscurve vanaf 3.000 tpm tot de top van de vermogensband, ontworpen om de prestaties continu te verhogen naarmate het toerental toeneemt.&nbsp;</p><p>(let op, Slip-on is niet inbegrepen)</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€5.494,00 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-911-gt3-evolution-header.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche 911 GT3 RS (991) / (991.2)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>De Slip-On Line van Akrapovič vertegenwoordigt de eerste stap in het afstemmingsproces van het uitlaatsysteem en de uitlaat kan eenvoudig worden geïnstalleerd op het originele systeem van de Porsche GT3 RS. Gebouwd uit hoogwaardig titanium en afgewerkt met twee opvallende dubbele ronde uitlaatpijpen, maakt dit instapsysteem de GT3 RS krachtiger en lichter en geeft de auto dat unieke Akrapovič-geluid. inclusief tailpipe set</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€5.585,01 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-911-gt3-rs-(991).png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche 911 GT3 RS (991) / (991.2)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Race Header Set (Titanium)</strong></p><p>Evolution Race Header Set converteert het slip-on-systeem in een volledig systeem, gebouwd voor het circuit en geeft de Porsche GT3 RS de extra prestaties die daarbij horen. Prachtig gemaakt van hoogwaardig titanium en met een coating, is de Evolution Race Line net zo opvallend om naar te kijken als het gebruik is. Van de handgemaakte titanium headers met 100 cpsi sportkatalysatoren en titanium flenzen - gegoten in de eigen gieterij van Akrapovič - tot de prachtig afgewerkte ronde titanium uitlaatpijpen, de Evolution Race Line is pure kwaliteit.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€5.494,01 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-911-gt3-rs-(991)-evolution.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche 911 Turbo / Turbo S (991.2)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>De Akrapovič Slip-On Line voor de Porsche 911 Turbo / Turbo S (991.2) is de eerste fase in het uitlaatafstemmingsproces en geeft deze reeds uitstekende auto een verdere stap in de prestaties. Voltooi het slip-on systeem met titanium eindpijpen in combinatie met koolstofvezel diffuser in matte of hoogglans afwerking.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€7.211,60 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-911-turbo---turbo-s.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche 911 Turbo / Turbo S (991)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>De Akrapovič Slip-On Line voor de Porsche 911 Turbo / Turbo S is de eerste fase in het uitlaatafstemmingsproces en geeft deze reeds uitstekende auto een verdere stap in de prestaties. Gemaakt van een hoogwaardige titaniumlegering en afgewerkt met prachtig handgemaakte koolstofvezel en titanium eindpijpen, vermindert de Slip-On Line het gewicht om te helpen bij het hanteren. De geluidstechnici van Akrapovič hebben ook een diep en uniek geluid gecreëerd om de Porsche 911 Turbo / Turbo S nog meer te laten opvallen. De Slip-On Line kan ook worden gebruikt in combinatie met de optionele Akrapovič-Downpipes - met of zonder cat - die aan het systeem kunnen worden toegevoegd om de prestaties naar een ander niveau te tillen.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.221,98 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-911-turbo-(991).png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche Boxter Spyder (981)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Dit volledig titanium uitlaatsysteem met drie uitlaatconfiguraties bespaart gewicht op de achteras en verbetert bovendien de handling met meer vermogen en koppel verkregen door verbeterde gasstroomdynamiek, verminderde tegendruk en een meer lineaire vermogenscurve. Vermogenswinsten zijn vooral merkbaar in het middenbereik. Al deze pluspunten worden afgerond met een zeer aangename klanknoot. Dankzij het dual-mode uitlaatklepsysteem verbetert de "cat-back" Slip-On het onmiskenbaar diepe resonerende geluid van Akrapovič.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.442,52 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-boxter-spyder-(981).png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche Cayenne (536)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Deze uitlaat is gemaakt van hoogwaardig lichtgewicht titanium en heeft een centrale demper die uitzet om twee sets uitlaatpijpen te produceren, met een keuze uit een titanium- of koolstofvezelafwerking die is ontworpen om de lijnen van de auto aan te vullen. Twee kleppen aan weerszijden van de geluiddemper regelen de luchtstroom en bevinden zich in klephuizen die zijn gegoten in de eigen gieterij van Akrapovič.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.037,90 incl 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-cayenne-(536).png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche Cayenne Turbo (958)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Het Slip-On-systeem met een geluiddemper gemaakt van hoogwaardig en lichtgewicht titanium levert meer vermogen, verbetert de reactietijd en voegt meer koppel toe. Het resultaat van het door de computer ontworpen uitlaatsysteem met optimale uitlaatgasstroom is het diepe geluid. Het geluid wordt luider gemaakt door het puntgelaste blok in de verbindingspijp te verwijderen die voorkomt dat de uniek ontworpen uitlaat wordt omzeild.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€5.720,52 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-cayenne-958.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche Cayman GT4 (981)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on line (Titanium)</strong></p><p>Dit volledig titanium uitlaatsysteem met drie uitlaatconfiguraties bespaart gewicht op de achteras en verbetert bovendien de handling met meer vermogen en koppel verkregen door verbeterde gasstroomdynamiek, verminderde tegendruk en een meer lineaire vermogenscurve. Vermogenswinsten zijn vooral merkbaar in het middenbereik. Al deze pluspunten worden afgerond met een zeer aangename klanknoot. Dankzij het dual-mode uitlaatklepsysteem verbetert de "cat-back" Slip-On het onmiskenbaar diepe resonerende geluid van Akrapovič.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.442,52 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-cayman-gt4-(981).png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche Macan TURBO / GTS (95B)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Het uitlaatsysteem Evolution Line (Titanium) is gemaakt van hoogwaardig lichtgewicht titanium om het gewicht te verminderen, met klepbehuizingen gegoten in de eigen gieterij en past rechtstreeks op de voorraadafvoerpijpen van de Macan. Ontwikkeld om zowel vermogens- als koppelwinsten over het hele bereik te leveren en de tegendruk aanzienlijk te verminderen, het is afgewerkt met handgemaakte koolstofvezel eindpijpen om het visuele plezier van de uitlaat te verrijken.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€7205,55 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-macan-gts.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche Panamera /4/ Sport Turismo (971)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Dit verbluffende systeem is volledig gemaakt van hoogwaardig titanium. Het bevat onderdelen die in de eigen gieterij van het bedrijf zijn gegoten, met een extra paar uitlaatkleppen achter de achterdempers, ontworpen om optimale controle over het uitlaatgeluid te bieden voor het beste auditieve genot. Het systeem is perfect afgestemd om de allerbeste geluidservaring in alle modi te leveren - er is enorm veel moeite gedaan om het een rijker hoogfrequent V6-geluid te geven met een sportieve racetoon, terwijl het soepel blijft en elke drone wordt geëlimineerd voor maximaal dagelijks comfort.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€5.735,40 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porche-panamera--4--sport-turismo.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche Panamera TURBO / GTS / Sport Turismo (971)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Dit verbluffende systeem is volledig gemaakt van hoogwaardig titanium. Het bevat onderdelen gegoten in de eigen gieterij van het bedrijf, met een actieve X-aansluiting en een extra paar uitlaatkleppen achter de achterste geluiddempers, ontworpen om optimale controle over het uitlaatgeluid te bieden voor het beste auditieve genot. Het systeem is perfect afgestemd om de allerbeste geluidservaring in alle modi te leveren - er is enorm veel moeite gedaan om het een sportief, diep hoger frequentie V8-geluid te geven met een onmiskenbaar Akrapovič-karakter, terwijl het soepel blijft en elke drone voor elke dag maximaal elimineert comfort.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€7.865,00 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-panamera-gts---sport-turismo.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Porsche Panamera Turbo S E-hybrid / Sport Turismo (971)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)&nbsp;</strong></p><p>Het systeem is perfect afgestemd om de allerbeste geluidservaring in alle modi te leveren - er is enorm veel moeite gedaan om het een sportief, diep hoger frequentie V8-geluid te geven met een onmiskenbaar Akrapovič-karakter, terwijl het soepel blijft en elke drone voor elke dag maximaal elimineert comfort. Door de optionele Akrapovič-geluidskit te gebruiken, kan de bestuurder kiezen tussen een luid, racy geluid of een stillere toon voor lange afstanden, ongeacht de modus die voor het voertuig is gekozen. Het uiterlijk van het Evolution-systeem kan ook verder worden verbeterd door vier ronde uitlaatpijpen van titanium of koolstofvezel toe te voegen.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€7.865,00 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/porsche-panamera-turbo-s-e-hybrid.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/img_7806.jpg
banner_size: is-fullheight
subtitle: 'Akrapovic uitlaatsystemen voor'
has_cta: true
cta_title: 'Staat uw auto er niet tussen?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: 933dd0ea-05a1-4dc6-bc5f-826458a06768
