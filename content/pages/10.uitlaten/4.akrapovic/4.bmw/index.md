title: BMW
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>BMW 340i (F30/F31)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Het Slip-On-uitlaatsysteem voor de BMW 340i is het resultaat van de perfecte integratie van expertise, de beste technologie en het gebruik van de beste materialen. Het is volledig gemaakt van titanium en afgewerkt met twee sets nieuw ontworpen handgemaakte koolstofvezel eindpijpen. Deze sportieve elegantie laat deze Beierse machine zich onderscheiden van de rest en biedt de smaak van een krachtiger voertuig, geaccentueerd met de extra vermogenswinst en gewichtsverlies van het Slip-On-uitlaatsysteem. (uitbreiden met Link pipe set is aanbevolen)</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.891,90 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-340i-f30-f31.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>BMW 440i (F32/F33/F36)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Het Slip-On-uitlaatsysteem voor de BMW 440i is het resultaat van de perfecte integratie van expertise, de beste technologie en het gebruik van de beste materialen. Het is volledig gemaakt van titanium en afgewerkt met twee sets nieuw ontworpen handgemaakte koolstofvezel eindpijpen. Deze sportieve elegantie laat deze Beierse machine zich onderscheiden van de rest en biedt de smaak van een krachtiger voertuig, geaccentueerd met de extra vermogenswinst en gewichtsverlies van het Slip-On-uitlaatsysteem. (uitbreiden met Link pipe set is aanbevolen)</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.891,90 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-440i-f32-f33-f36.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>BMW M140i (F20/F21)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Het Slip-On-uitlaatsysteem voor de BMW M140i is het resultaat van de perfecte integratie van expertise, de beste technologie en het gebruik van de beste materialen. Het is volledig gemaakt van titanium en afgewerkt met twee sets nieuw ontworpen handgemaakte koolstofvezel eindpijpen. Deze sportieve elegantie laat deze Beierse machine zich onderscheiden van de rest en biedt de smaak van een krachtiger voertuig, geaccentueerd met de extra vermogenswinst en gewichtsverlies van het Slip-On-uitlaatsysteem. De dual-mode ECU-gestuurde klepuitlaat van Akrapovič biedt een soepel, rijk geluid bij normaal cruisen en een diepe, sportieve, bijna agressieve toon bij dynamischer rijden met de M140i. Evolution Link pipe set is raadzaam</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.722,50 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m140i-f20-f21.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>BMW M2 (F87)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Gemaakt van ultra-lichtgewicht hoogwaardig titanium, dat het totale gewicht van de auto zal verlagen en de wegligging zal verbeteren. Dit perfect afgewerkte ECE-goedgekeurde systeem is afgestemd om het maximale uit de TwinPower Turbo zescilindermotor van de M2 ​​te halen en is ontworpen om meer vermogen over het hele toerenbereik te leveren. Dit nieuwe systeem heeft twee afzonderlijke buizen, van de downpipe tot de uitlaat - in tegenstelling tot de originele configuratie met één buis van de auto. Het is perfect gerouteerd en gevormd om een ​​optimale gasstroom van de prachtige motor van de M2 ​​te bieden en een merkbare prestatieverbetering te leveren. Het geluid is ook verbeterd door de ingenieurs van Akrapovič; het unieke externe geluid heeft een hogere toonhoogte, wat een echt race-gevoel geeft in het bovenste toerentalbereik. De Evolution Line is perfect voor dagelijks gebruik, met laag toerental rijden soepel en verfijnd, maar toont nog steeds het echte potentieel van het systeem. Wanneer hogere toeren worden ingedrukt en de kleppen permanent open zijn, heeft het geluid een verslavende racetoon.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.158,66 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m2-f87-1569926466.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>BMW M2 competition (F87N)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>De nieuwste toevoeging aan de BMW M-serie is de ontzagwekkende M2-competitie, een prachtige high-performance auto die nog beter kan worden gemaakt door een Akrapovič Slip-On-uitlaatsysteem toe te voegen. Gemaakt van titanium met onderdelen gegoten in de eigen gieterij van Akrapovič &nbsp;zal dit systeem het gewicht van het uitlaatsysteem met meer dan 45% verminderen en extra vermogen en lagere tegendruk leveren. Het geluid is ook verbeterd door de ingenieurs van Akrapovič, die een echte sportieve toon leveren, vooral in het middelste en bovenste toerentalbereik, maar zonder gedreun. Koolstofvezel uitlaatpijpen geven een extra uitstraling aan de achterzijde van de M2 ​​Competition om zijn sportieve potentieel te tonen. De prestaties kunnen verder worden verbeterd met de toevoeging van optionele evolution link-pijpset.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€4.706,90 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m2-competition-f87.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>BMW M240i (F22/F23)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Het Slip-On-uitlaatsysteem voor de BMW M240i is het resultaat van de perfecte integratie van expertise, de beste technologie en het gebruik van de beste materialen. Het is volledig gemaakt van titanium en afgewerkt met twee sets nieuw ontworpen handgemaakte koolstofvezel eindpijpen. Deze sportieve elegantie laat deze Beierse machine zich onderscheiden van de rest en biedt de smaak van een krachtiger voertuig, geaccentueerd met de extra vermogenswinst en gewichtsverlies van het Slip-On-uitlaatsysteem. De dual-mode ECU-gestuurde klepuitlaat van Akrapovič biedt een soepel, rijk geluid bij normaal cruisen en een diepe, sportieve, bijna agressieve toon bij het dynamischer rijden van de M240i.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.722,50 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m240i.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>BMW M3 (F80)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>De nieuwste toevoeging aan de BMW M-serie is de ontzagwekkende M3, een uitstekende high-performance auto die nog beter kan worden gemaakt door een Akrapovič Slip-On-uitlaatsysteem toe te voegen. Gemaakt van titanium - waarvan de onderdelen in de eigen gieterij van Akrapovič zijn gegoten. Dit systeem is een volledig nieuw ontwerp en het vermindert het totale voertuiggewicht en levert extra vermogen en een sportief geluid, dat is aangepast om lagere frequenties te markeren en het luisterplezier te vergroten. Verhogingen van zowel vermogen als koppel dragen bij aan de zintuiglijke ervaring, gecombineerd met verbeterde behendigheid. Het uiterlijk van het Slip-On-systeem kan ook verder worden verbeterd door titanium- of carbon-uitlaatpijpen en de verbluffende diffuser met carbon achter toe te voegen.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€4.163,92 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m3-f80.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>BMW M4 (F83/F84)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>De nieuwste toevoeging aan de BMW M-serie is de ontzagwekkende M4, een uitstekende high-performance auto die nog beter kan worden gemaakt door een Akrapovič Slip-On-uitlaatsysteem toe te voegen. Gemaakt van titanium - waarvan de onderdelen in de eigen gieterij van Akrapovič zijn gegoten. Dit systeem is een volledig nieuw ontwerp en het vermindert het totale voertuiggewicht en levert extra vermogen en een sportief geluid, dat is aangepast om lagere frequenties te markeren en het luisterplezier te vergroten. Verhogingen van zowel vermogen als koppel dragen bij aan de zintuiglijke ervaring, gecombineerd met verbeterde behendigheid. Het uiterlijk van het Slip-On-systeem kan ook verder worden verbeterd door titanium- of carbon-uitlaatpijpen en de verbluffende diffuser met carbon achter toe te voegen.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€4.163,92 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m4-f82-f83.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>BMW M5 (F10)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Na het toevoegen van een volledig titanium Evolution-systeem wilt u nooit meer stoppen met rijden. Het uitlaatsysteem is gemaakt van ons eigen titanium en bestaat uit dubbele uitlaatdempers met uitlaatkleppen, voor stroming geoptimaliseerde buizen en een uniek gegoten X-vormig onderdeel met resonatoren. De Evolution verlaagt de tegendruk en geeft de M5 meer vermogen zonder de katalysatoren te verwijderen. Het verhoogt het reactievermogen en biedt een groot verschil in gewicht. Het geluid is ook verbeterd, met een diep sportief geluid dat groeit tot een agressief gebrul. Het werkt goed met het ANC-systeem van de M5 - net als het klepsysteem, dat wordt geregeld via de instellingen van de M5. Maak de look af met een set van vier prachtige eindpijpen in titanium of koolstofvezel.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€8.214,96 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m5-f10.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>BMW M5 / M5 Competition (F90)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Gebouwd van hoogwaardig lichtgewicht titanium - met sommige onderdelen gegoten in de eigen gieterij van Akrapovič - zal dit systeem het gewicht van het uitlaatsysteem verminderen. Ontwikkeld om zowel vermogens- als koppelversterkingen over het hele bereik te leveren met de meeste winst bij lagere toerentallen, verbetert het reactievermogen en de rijeigenschappen. Het systeem levert duidelijk ander geluid dan het standaardsysteem, het meest merkbaar bij schakelen, achteruitgaan en versnellen. De koolstofvezel eindpijpen zijn perfect gemaakt en ontworpen om mooi te passen in de optionele koolstofvezel diffuser. Optionele carbon-fiber spiegelkappen zijn ook beschikbaar om het uiterlijk van de auto verder te verbeteren.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€5.795,80 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m5-competition.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>BMW M6 (F12/F13)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Het Evolution-uitlaatsysteem gemaakt van ons eigen titanium bestaat uit nieuw ontwikkelde dubbele uitlaatdempers met uitlaatkleppen, voor stroming geoptimaliseerde buizen en een uniek gegoten titanium X-vormig onderdeel met resonatoren. De tegendruk wordt verlaagd en de M6 profiteert van meer vermogen over het hele bereik van rpms. Het verhoogt het reactievermogen en een verbeterde vermogen-gewichtsverhouding is bereikt zonder de katalysatoren te verwijderen. De uitlaat van de Evolution-prestaties biedt een diep sportief geluid, dat groeit tot een agressief gebrul. Het werkt goed met het ANC-systeem van de M6 - net als het klepsysteem, dat wordt geregeld via de instellingen van de M6. Maak het uiterlijk compleet met een set van vier prachtige uitlaatpijpen in titanium of koolstofvezel.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.890,- incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m6-f12-f13.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2><strong>BMW M6 Gran Coupé (F06)</strong></h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Het Evolution-uitlaatsysteem gemaakt van ons eigen titanium bestaat uit nieuw ontwikkelde dubbele uitlaatdempers met uitlaatkleppen, voor stroming geoptimaliseerde buizen en een uniek gegoten titanium X-vormig onderdeel met resonatoren. De tegendruk is verlaagd en de M6 Gran Coupe profiteert van meer vermogen over het hele bereik van toerental. Het verhoogt het reactievermogen en een verbeterde vermogen-gewichtsverhouding is bereikt zonder de katalysatoren te verwijderen. De uitlaat van de Evolution-prestaties biedt een diep sportief geluid, dat groeit tot een agressief gebrul. Het werkt goed met het ANC-systeem van de M6 Gran Coupe - net als het klepsysteem, dat wordt geregeld via de instellingen van de M6. Maak het uiterlijk compleet met een set van vier prachtige uitlaatpijpen in titanium of koolstofvezel.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€8.489,94 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m6-gran-coupe.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>BMW X5 M (F87)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>De Evolution Line (Titanium) is ontworpen om te voldoen aan de ECE-typegoedkeuring als een plug-and-play-systeem en past rechtstreeks op de downpipes van de BMW X5 M. Het is afgestemd om een ​​perfecte geluidsbegeleiding te bieden aan het sportieve gevoel van de auto, en is perfect afgewerkt met handgemaakte carbon eindpijpen voor een mooie visuele aantrekkingskracht. De opvallende ronde uitlaatpijpen hebben titanium binnenste delen om ze nog meer te laten opvallen. De uitlaatpijpen zijn onberispelijk vervaardigd en ontworpen om naadloos te passen met de optionele diffusor van koolstofvezel. Het is ook verkrijgbaar met de optionele Akrapovič-geluidskit.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.862,15 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-x5-m-(f85).png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>BMW X6 M (F86)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (titanium)</strong></p><p>De Evolution Line (Titanium) is ontworpen om te voldoen aan de ECE-typegoedkeuring als een plug-and-play-systeem en past rechtstreeks op de downpipes van de BMW X6 M. Het is afgestemd om een ​​perfecte geluidsbegeleiding te bieden aan het sportieve gevoel van de auto, en is perfect afgewerkt met handgemaakte carbon eindpijpen voor een mooie visuele aantrekkingskracht. De opvallende ronde uitlaatpijpen hebben titanium binnenste delen om ze nog meer te laten opvallen. De uitlaatpijpen zijn onberispelijk vervaardigd en ontworpen om naadloos te passen met de optionele diffusor van koolstofvezel. Het is ook verkrijgbaar met de optionele Akrapovič-geluidskit.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.862,15 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-x6-m-(f86).png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>BMW Z4 M40i G29</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Vervaardigd volgens exacte specificaties van hoogwaardig titanium - met onderdelen gegoten in de eigen gieterij van Akrapovič - maakt dit systeem gebruik van lichtgewicht materialen om het totale voertuiggewicht te verminderen en het vermogen en koppel over het hele toerenbereik te optimaliseren.&nbsp;</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€4.174,50 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-z4-m40i-g29-1569933189.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/dsc_5063-1569933463.jpg
banner_size: is-fullheight
subtitle: 'Akrapovic uitlaatsystemen voor'
has_cta: true
cta_title: 'STAAT UW AUTO ER NIET TUSSEN?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: 4a794a97-513d-41bc-bf36-368b39693f14
