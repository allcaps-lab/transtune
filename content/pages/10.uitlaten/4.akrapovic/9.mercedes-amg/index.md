title: Mercedes-AMG
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Mercedes-AMG C63(s) AMG Coupé (c205)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Het systeem vervangt zowel de normale als de performance-uitlaten die op de Mercedes-AMG C 63 en C 63 S zijn gemonteerd. Dit verbluffende systeem bevat onderdelen die in de eigen gieterij van het bedrijf zijn gegoten, met een actieve X-aansluiting en een extra paar uitlaatkleppen achter de achterdempers, ontworpen voor optimale controle over het uitlaatgeluid voor optimaal luisterplezier. Met twee gedefinieerde geluidskarakteristieken en drie verschillende instellingen voor rijgeluid is het systeem perfect afgestemd om het allerbeste geluid in alle modi te leveren. Met de optionele Akrapovič Sound Kit-controller kan de gebruiker de uitlaattoon kiezen, ongeacht de gekozen modus van het voertuig en is een aanbevolen upgrade bij het vervangen van het basis uitlaatsysteem.&nbsp;</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€7.392,86 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mercedes-c63-c205-coupe.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Mercede-AMG C63(s) AMG Sedan / Estate (W205)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong><br></p><p>Het systeem vervangt zowel de normale als de performance-uitlaten die op de Mercedes-AMG C 63 en C 63 S zijn gemonteerd. Dit verbluffende systeem bevat onderdelen die in de eigen gieterij van het bedrijf zijn gegoten, met een actieve X-aansluiting en een extra paar uitlaatkleppen achter de achterdempers, ontworpen voor optimale controle over het uitlaatgeluid voor optimaal luisterplezier. Met twee gedefinieerde geluidskarakteristieken en drie verschillende instellingen voor rijgeluid is het systeem perfect afgestemd om het allerbeste geluid in alle modi te leveren. Met de optionele Akrapovič Sound Kit-controller kan de gebruiker de uitlaattoon kiezen, ongeacht de gekozen modus van het voertuig en is een aanbevolen upgrade bij het vervangen van het basis uitlaatsysteem.&nbsp;</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€7.392,86 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mercede-c63-estate-sedan-w205.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Mercedes-AMG GT Coupé /GT S / GT C</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Afgewerkt met handgemaakte carbon eindpijpen, heeft deze uitlaat een prachtige soundtrack en die unieke handtekening van Akrapovič. Het geluid wordt geregeld door een complexe X-verbinding, gegoten in de eigen titaniumgieterij, met een nauwkeurig bewerkte klep om de tonen te veranderen, die kunnen worden bediend met een consoleknop in de cabine of door de Sound Remote Controller van de optionele Akrapovič geluidskit. Wanneer de X-verbindingsklep open is, levert het uitlaatsysteem een ​​meer continu, soepel en harmonisch geluid voor een comfortabelere rit, en in gesloten modus is de akoestiek gemaximaliseerd om de auto''s een pure sportieve en intense muscle-car V8 te geven brullen. Vermogen en koppelniveaus worden verhoogd over het gehele toerenbereik.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€7.667,90 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mercedes-benz-amg-coupe-gt---gt-s.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Mercedes-AMG E63 / E63S (W213)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Dit verbluffende systeem bevat ook andere onderdelen die in de eigen gieterij zijn gegoten, met een actieve X-aansluiting en een extra paar uitlaatkleppen achter de achterdempers, ontworpen om optimale controle over het uitlaatgeluid te bieden voor het beste luisterplezier. Het systeem behoudt het kenmerkende AMG-geluid, terwijl het nog zwakker en rijker wordt, en het biedt geweldige akoestische feedback zowel binnen als buiten de cabine. Een toename van prestaties en koppel door een breed toerentalbereik en een aanzienlijke gewichtsbesparing van meer dan 45% maakt dit een zeer serieuze prestatie-upgrade. Met geoptimaliseerde gasstroom en verminderde tegendruk, maakt het onbeperkte afstemmingsopties mogelijk.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€8.518,40 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mercedes-amg-e-63---e-63-s-(w213).png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Mercedes-AMG G500 / G550 (W463A)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line(Titanium)</strong></p><p>De Evolution Line (Titanium) voor de Mercedes-AMG G 500 is ontwikkeld met het oog op duurzaamheid en prestaties met behulp van hoogwaardig duurzaam lichtgewicht titanium, waardoor het systeem 45% lichter is dan het standaardsysteem. Het is afgewerkt met speciaal ontworpen uitlaatpijpen - gevormd in de gieterij van Akrapovič - die aan weerszijden van het voertuig uitstappen, als aanvulling op het robuuste uiterlijk van de auto, perfect gerouteerd om in een beperkte ruimte te passen en gecoat voor verhoogde duurzaamheid. Dit systeem - met een paar uitlaatkleppen achter de achterdempers voor optimale controle over de uitlaattonen - levert een uniek geluid dat duidelijk dieper en sportiever is, vooral tijdens het versnellen en schakelen. Vermogen en koppelniveaus worden verhoogd over het gehele toerentalbereik.</p>'
          -
            type: column
            content:
              -
                type: full_height_image
                full_height_image: /assets/img/mercedes-amg-g500---g-550.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Mercedes-AMG G63 (W463A)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>De Evolution Line (Titanium) voor de Mercedes-AMG G 63 is ontwikkeld met het oog op duurzaamheid en prestaties met behulp van hoogwaardig duurzaam lichtgewicht titanium, waardoor het systeem 45% lichter is dan het standaardsysteem. Het is afgewerkt met speciaal ontworpen uitlaatpijpen - gevormd in de gieterij van Akrapovič - die aan weerszijden van het voertuig uitstappen, als aanvulling op het robuuste uiterlijk van de auto, perfect gerouteerd om in een beperkte ruimte te passen en gecoat voor verhoogde duurzaamheid. Dit systeem - met een paar uitlaatkleppen achter de achterdempers voor optimale controle over de uitlaattonen - levert een uniek geluid dat duidelijk dieper en sportiever is, vooral tijdens het versnellen en schakelen. Vermogen en koppelniveaus worden verhoogd over het gehele toerentalbereik.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€8.881,40 incl. 21% BTW&nbsp;</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mercedes-amg-g63-(w463a).png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Mercedes-AMG S63 Coupé (C217)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Het ultralichte Evolution Line-uitlaatsysteem voor de S 63 AMG Coupé is volledig gemaakt van titanium van de hoogste kwaliteit en bevat onderdelen die in de eigen gieterij van het bedrijf zijn gegoten. Dit revolutionaire nieuwe systeem heeft voor het eerst een actieve X-verbinding, die perfect is gevormd om uitlaatgassen van beide kanten van de motor te mengen. Het systeem is ontwikkeld met het oog op duurzaamheid en prestaties met behulp van speciale titaniumlegeringen - die 45% lichter zijn dan staal en 47% lichter dan het standaardsysteem. De Evolution Line heeft een uniek geluidsprofiel dat de cilindervolgorde van de motor volledig tot zijn recht laat komen. Door het gebruik van de eigen elektronica van de auto en de Akrapovič geluidskit, zijn verschillende geluidskarakteristieken beschikbaar door de besturing van de actieve X-verbinding - die een geleide en nauwkeurig bewerkte klep heeft om de tonen te veranderen. Met de klep open is het geluid voller, meer continu en resonerend, maar in de gesloten positie produceert het systeem een ​​pure, sterke brul die doet denken aan een muscle car.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€11.866,62 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mercedes-amg-s-63-coupe-(c217).png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/dsc_8853.jpg
banner_size: is-fullheight
subtitle: 'Akrapovic uitlaatsystemen voor'
has_cta: true
cta_title: 'Staat uw auto er niet tussen?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: 9f74b64e-c571-4f8d-a452-d5c2104b65cf
