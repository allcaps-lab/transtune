title: Ferrari
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Ferrari 458 Italia / 458 Spider</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>De Ferrari 458 Italia is een van de meest indrukwekkende wegauto''s ooit ontworpen. Met zijn lichtgewicht constructie, technische innovaties en 570 pk 4,5-liter motor, is het een onvergetelijke rijervaring. Voeg ons titanium Slip-On-uitlaatsysteem toe voor nog meer vermogen, minder gewicht en een verbeterd sportief geluid. Onze Slip-On heeft dual-mode uitlaatkleppen en een drievoudig exit-systeem dat een helder maar ingetogen geluid levert bij lage toeren, maar bij hogere toeren ontketent het volledig een ongelooflijk diep sportief geluid van de F1-geïnspireerde, hoogdraaiende V8. Maak de look af met een set van drie prachtige carbon eindpijpen. Verdere afstemmingsmogelijkheden en nog agressiever geluid zijn beschikbaar met onze titanium koppelingspijpset met high-flow katalysatoren.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€9.707,54 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/ferrari-458-italia---458-spider.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Ferrari 488 GTB / 488 Spider</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: |
                  <p><strong>Slip-on Line (Titanium)</strong></p><p>Gebouwd met behulp van hoogwaardige, ultralichte speciale titaniumlegeringen - met onderdelen gegoten in de eigen gieterij van Akrapovič - met een gewichtsreductie van 35,2% ten opzichte van de standaard uitlaat. Buizen met een grotere diameter helpen de luchtstroom en een andere systeemconfiguratie verlaagt de tegendruk om de prestaties en het vermogen te verhogen. Het is ECE-goedgekeurd en wordt geleverd met eenvoudig te installeren instructies. Het is afgewerkt met een koolstofvezel buitenhuls op de uitlaatpijpen. Vooral bij het combineren van het Slip-On-systeem met de optionele roestvrijstalen verbindingspijpsets met of zonder katalysatoren, benadrukt het het karakter van het Ferrari V8-geluid (flatcrankmotor) met de unieke Akrapovič-noot.
                  
                  Een optionele Akrapovič geluidskit is beschikbaar om de verschillende geluidsinstellingen te regelen.&nbsp;</p><p>Bij het installeren van Akrapovič optionele koppelingspijpset met of zonder katalysatoren is het opnieuw toewijzen van de ECU verplicht.</p>
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.837,65 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/ferrari-488-gtb---488-spider.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/transtune_bas_fransen_54.jpg
banner_size: is-fullheight
subtitle: 'Akrapovic uitlaatsystemen voor'
has_cta: true
cta_title: 'STAAT UW AUTO ER NIET TUSSEN?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: bda5f588-b67c-4568-98a8-d9fb3cc80b6c
