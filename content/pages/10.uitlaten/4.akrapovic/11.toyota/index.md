title: Toyota
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Toyota Supra (A90)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Dit ECE-typegoedgekeurde systeem is volledig vervaardigd uit hoogwaardig titanium en heeft een uitlaatklep met een behuizing in de eigen gieterij. De enkele demperopstelling heeft een unieke set titanium uitlaatpijpen, met behulp van een ontwerp dat niet eerder te zien was op een aftermarket-uitlaat. Volledig aangepast aan de lijnen van de auto, is de buitenzijde van de uitlaatpijp gecoat voor meer duurzaamheid, terwijl het binnenoppervlak een blauw-paarse natuurlijke titaniumkleur heeft verkregen door gloeien, plus een stijlvol en sportief opdrukontwerp om het systeem een zeer speciale en exclusieve afwerking. Door het gebruik van lichtgewicht materialen zal het systeem het totale voertuiggewicht verminderen en het vermogen en koppel over het gehele toerenbereik optimaliseren.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€3.254,90 incl 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/toyota-supra-(a90).png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/image.php.jpeg
banner_size: is-fullheight
subtitle: 'Akrapovic uitlaatsystemen voor'
has_cta: true
cta_title: 'STAAT UW AUTO ER NIET TUSSEN?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: 17a8e110-54a9-48c7-999b-929e0a355e8e
