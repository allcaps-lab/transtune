---
title: Audi
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>&nbsp;Audi R8 5.2 fsi coupé/spyder &nbsp;2016-&gt;&nbsp;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (titanium)</strong></p><p>Het Slip-On Line uitlaatsysteem is gemaakt van een ultralichte hoogwaardige titaniumlegering, waardoor de vermogen-gewichtsverhouding en de rijeigenschappen van de R8 worden verbeterd. De Slip-On Line (Titanium) is met precisie ontworpen om de gasrespons te verbeteren en zowel het vermogen als het koppel te vergroten. Het hoogwaardige uitlaatsysteem is ontworpen om de tegendruk te verminderen tot bijna de helft van de voorraadniveaus door een combinatie van gespecialiseerde uitlaatvormen, secundaire resonatoren en titanium dual-mode uitlaatkleppen. Het is afgewerkt met twee prachtig met de hand vervaardigde koolstofvezel en titanium eindpijpen die perfect in de standaard diffusor van de R8 passen.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€8.515,98 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/audi-r8-5.2fsi-coupe-1569920883.png
      -
        type: text
        text: '<p><br></p>'
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Audi RS4 Avant B8</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (titanium)</strong></p><p>De sportwagens van Audi zijn de perfecte partner voor nauwkeurig ontwikkelde Evolution-systemen van titaniumlegering, die deze Ingolstadt-machines op alle fronten nog aantrekkelijker maken. Ze leveren meer vermogen, verlagen het gewicht en leveren dat onmiskenbare Akrapovič-geluid. Hij is diep en resoneert bij lagere toerentallen en sportief bij hogere toerentallen. Het systeem levert ook royaal extra prestaties. Deze titanium uitlaatsystemen met drie dempers en speciale onderdelen die in de eigen gieterij zijn gegoten, hebben ingebouwde kleppen om het geluid aan te passen. Kleppen zijn verbonden met de standaardelektronica van de RS. Het pakket wordt afgerond met koolstofvezel en titanium eindpijpen, die echt opvallen en de sportieve geest van de Akrapovič-uitlaat weergeven.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€5.711,32 incl. 21% BTW</strong><br></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/audi-rs4-avant-b8.png
      -
        type: text
        text: '<p><br></p>'
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Audi RS6 Avant C7&nbsp;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Audi''s krachtige RS-modellen zijn een ideale match voor de vakkundig vormgegeven en prachtig ontworpen Akrapovič Evolution-uitlaatsystemen. De Evolution geeft meer vermogen, hoger koppel, verlaagt het gewicht en levert een onmiskenbaar Akrapovič-geluid - dat een diep geluid heeft bij lagere toeren en een ''knallend'' geluid. Dit titanium uitlaatsysteem heeft gegoten collectoren en drie dempers, waarvan de centrale een x-stuk binnenkant heeft, die het vermogen verhogen en het geluid verbeteren. De kleppen van het systeem zijn ontworpen om te openen en sluiten in lijn met de standaardelektronica van de RS, en het hele systeem heeft koolstofvezel eindpijpen om te pronken met de uitstekende afwerking van de Akrapovič-uitlaat.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€8.395,17 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/audi-rs6-avant-c7.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Audi RS7 Sportback C7</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Audi''s krachtige RS-modellen zijn een ideale match voor de vakkundig vormgegeven en prachtig ontworpen Akrapovič Evolution-uitlaatsystemen. De Evolution geeft meer vermogen, hoger koppel, verlaagt het gewicht en levert een onmiskenbaar Akrapovič-geluid - dat een diep geluid heeft bij lagere toeren en een ''knallend'' geluid. Dit titanium uitlaatsysteem heeft gegoten collectoren en drie dempers, waarvan de centrale een x-stuk binnenkant heeft, die het vermogen verhogen en het geluid verbeteren. De kleppen van het systeem zijn ontworpen om te openen en sluiten in lijn met de standaardelektronica van de RS, en het hele systeem heeft koolstofvezel eindpijpen om te pronken met de uitstekende afwerking van de Akrapovič-uitlaat.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€8.395,17 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/audi-rs7-sportback-c7.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Audi S6 Avant/Limousine C7</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: |
                  <p><strong>Evolution Line (Titanium)</strong></p><p>Het Audi S-reeks beschikt over krachtige V8-motoren die perfect passen bij de Akrapovič Evolution-uitlaatsystemen.
                  De Audi S-reeks Evolution-uitlaten, gestileerd en in eigen huis ontworpen door de ingenieurs van Akrapovič, omvat kleppen die worden gestuurd door elektrische actuators die zijn ontworpen om hun positie te veranderen, afhankelijk van het rijprogramma van de auto en de stijl van de bestuurder. Ze kunnen ook worden gewijzigd in de individuele programma-instellingen van de auto.
                  De toevoeging van een Evolution-uitlaat zal het vermogen en het koppel verhogen, terwijl het gewicht afneemt en het unieke Akrapovič-geluid wordt toegevoegd. Evolutie-uitlaten geven de auto een merkbaar ander karakter, met een diep geluid bij lagere toerentallen en een sportievere noot als de toerentallen stijgen.
                  Met vier, prachtig vormgegeven ronde koolstofvezel eindpijpen, ziet het Evolution-systeem er net zo goed uit als het werkt op Audi S-modellen.</p>
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€7.614,64 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/audi-s6-avant-limousine-c7.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2><strong>Audi S7 Sportback C7</strong></h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Evolution Line (Titanium)</strong></p><p>Het Audi S-reeks beschikt over krachtige V8-motoren die perfect passen bij de Akrapovič Evolution-uitlaatsystemen. De Audi S-reeks Evolution-uitlaten, gestileerd en in eigen huis ontworpen door de ingenieurs van Akrapovič, omvat kleppen die worden gestuurd door elektrische actuators die zijn ontworpen om hun positie te veranderen, afhankelijk van het rijprogramma van de auto en de stijl van de bestuurder. Ze kunnen ook worden gewijzigd in de individuele programma-instellingen van de auto. De toevoeging van een Evolution-uitlaat zal het vermogen en het koppel verhogen, terwijl het gewicht afneemt en het unieke Akrapovič-geluid wordt toegevoegd. Evolutie-uitlaten geven de auto een merkbaar ander karakter, met een diep geluid bij lagere toerentallen en een sportievere noot als de toerentallen stijgen. Met vier, prachtig vormgegeven ronde koolstofvezel eindpijpen, ziet het Evolution-systeem er net zo goed uit als het werkt op Audi S-modellen.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€7.641,64 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/audi-s7-sportback-c7.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Audi S3 8V</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Downpipe / Linkpipe</strong></p><p>De high-flow downpipe met front link pijp is gemaakt van hoogwaardig roestvrij staal en eenmaal gemonteerd op Golf R / Audi S3 uitgerust met een standaard of Akrapovič prestatie-uitlaatsysteem, verbetert het de prestatiecijfers verder: zowel het koppel als het vermogen. Het bevat een katalysator (200 cpsi) die ECE-compatibel is. Deze optionele downpipe produceert een verbeterd geluid dat een pure race-emotie en fantastische feedback bij hoge toeren levert, zonder ongewenste drone, waardoor het geschikt is voor dagelijks gebruik.</p><p><br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.117,50 incl 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/audi-s3-8v-downpipe.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/transtune_bas_fransen_44.jpg
banner_size: is-fullheight
subtitle: 'Akrapovic uitlaatsystemen voor'
has_cta: true
cta_title: 'Staat uw auto er niet tussen?'
cta_subtitle: 'Vul het contactformulier in en wij nemen contact met u op!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: b0cf28c5-e44b-4da4-a7b5-6acb3f78c074
---
<p><strong></strong>
</p>