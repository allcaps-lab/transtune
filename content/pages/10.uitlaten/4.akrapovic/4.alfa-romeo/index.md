title: 'Alfa Romeo'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Alfa Romeo&nbsp;<strong>Giulia Quadrifoglio</strong></h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: |
                  <p><strong>Evolution Line (Titanium)</strong></p><p>Het systeem bevat onderdelen die in de eigen gieterij van het bedrijf zijn gegoten, zoals het klephuis en het X-deel in de geluiddemper, die perfect is gevormd om uitlaatgassen met een nauwkeurig gedefinieerde mengverhouding te laten mengen.
                  
                  Ontwikkeld om zowel vermogens- als koppelwinsten over het hele bereik te leveren en de tegendruk aanzienlijk te verminderen, het is afgewerkt met handgemaakte koolstofvezel en titanium eindpijpen om het visuele plezier van de uitlaat te verbeteren.
                  
                  Diepgaande engineering heeft een diep en sportief geluid geproduceerd zonder ongewenste drone, perfect geschikt voor dagelijks gebruik. Het verschilt duidelijk van het standaardsysteem, vooral tijdens gedeeltelijke belasting, tijdens het schakelen en bij hogere toerentallen, waar het een puur racegeluid levert.</p>
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.146,80 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/alfa-romeo-giulia-quadrifoglio.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/dsc03161-1570439872.jpg
banner_size: is-fullheight
subtitle: 'Akrapovic uitlaatsystemen voor'
has_cta: true
cta_title: 'STAAT UW AUTO ER NIET TUSSEN?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: 27e18d2b-9c46-4c26-bbce-0d487fa0be76
