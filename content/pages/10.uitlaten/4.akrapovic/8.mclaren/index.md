title: Mclaren
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>McLaren MP4-12C / 12C Spyder</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>De prestatie-uitlaat voor de McLaren MP4-12C is een hightech-systeem gemaakt van een speciale duurzame en hittebestendige titaniumlegering met grotere buizen en unieke oplossingen om het koppel en vermogen bij alle toeren te verbeteren, vooral in het meest gebruikte middenbereik. Onze compromisloze aanpak maakt gebruik van de beste materialen en het beste ontwerp. Lichte systemen besparen gewicht en verlagen het zwaartepunt, wat ook leidt tot een betere handling. Een speciale coating garandeert een state-of-the-art look. De 12C met het Akrapovič Slip-On-uitlaatsysteem klinkt dieper in de lage toeren en geeft een heerlijk race-geluid naarmate de toeren hoger worden. Slip-On-systemen bevatten speciale geluidsoplossingen om de geluidsfrequenties af te stemmen: elk element van het systeem is ontworpen voor maximale geluidsuitvoer en biedt alles wat nodig is voor aangenaam cabinecomfort en om de V8 te laten zingen voor echte autoliefhebbers bij alle toeren in totaal versnellingen. Bovendien wekken de koolstofvezel eindpijpen de opvallende visuele indruk dat dit geen gewone McLaren is.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€5.881,21 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mclaren-mp4-12c-akrapovic.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>McLaren 540C</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Een uitstekende auto verdient een uitstekende uitlaat, en de Slip-On Line (Titanium) voor de McLaren is zeker dat. Het is gemaakt van speciale, lichtgewicht, duurzame en hittebestendige titaniumlegeringen, allemaal perfect gevormd en gevormd om in de auto te passen. Grotere buizen leveren een geoptimaliseerde gasstroom en het gebruik van ultralichte materialen zal het gewicht van het voertuig aanzienlijk verminderen en de handling verbeteren. Een onderscheidende coating biedt een verbeterd uiterlijk en de delen die het meest worden blootgesteld aan warmte hebben speciale isolatie voor bescherming. Met twee vlekkeloos gestileerde en afgewerkte handgemaakte koolstofvezel eindpijpen, die de titanium pijpen prachtig aanvullen, is deze uitlaat een kunstwerk. Het wordt compleet geleverd met een soundtrack die perfect overeenkomt met de prestaties van de auto, ideaal afgestemd om een ​​unieke geluidskleur en het gevierde kenmerkende geluid van Akrapovič te produceren.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.615,32 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mclaren-540c.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>McLaren 570S / 570Spider / 570GT</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Een uitstekende auto verdient een uitstekende uitlaat, en de Slip-On Line (Titanium) voor de McLaren is zeker dat. Het is gemaakt van speciale, lichtgewicht, duurzame en hittebestendige titaniumlegeringen, allemaal perfect gevormd en gevormd om in de auto te passen. Grotere buizen leveren een geoptimaliseerde gasstroom en het gebruik van ultralichte materialen zal het gewicht van het voertuig aanzienlijk verminderen en de handling verbeteren. Een onderscheidende coating biedt een verbeterd uiterlijk en de delen die het meest worden blootgesteld aan warmte hebben speciale isolatie voor bescherming. Met twee vlekkeloos gestileerde en afgewerkte handgemaakte koolstofvezel eindpijpen, die de titanium pijpen prachtig aanvullen, is deze uitlaat een kunstwerk. Het wordt compleet geleverd met een soundtrack die perfect overeenkomt met de prestaties van de auto, ideaal afgestemd om een ​​unieke geluidskleur en het gevierde kenmerkende geluid van Akrapovič te produceren.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.615,32 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mclaren-570s-570-spider.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>McLaren 650S / 650S Spyder&nbsp;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>De 650S met het Akrapovič Slip-On-uitlaatsysteem klinkt dieper in de lage toeren en geeft een heerlijk race-geluid naarmate de toeren hoger worden. Slip-On-systemen bevatten speciale geluidsoplossingen om de geluidsfrequenties af te stemmen: elk element van het systeem is ontworpen voor maximale geluidsuitvoer en biedt alles wat nodig is voor aangenaam cabinecomfort en om de V8 te laten zingen voor echte autoliefhebbers bij alle toeren in totaal versnellingen. Bovendien wekken de koolstofvezel eindpijpen de opvallende visuele indruk dat dit geen gewone McLaren is.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€6.273,25 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mclaren-650s-650s-spyder.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/welding-(3).jpg
banner_size: is-fullheight
subtitle: 'Akrapovic uitlaatsystemen voor'
has_cta: true
cta_title: 'STAAT UW AUTO ER NIET TUSSEN?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: 44b8dd01-d99a-4c6c-a21c-914e44ff3fac
