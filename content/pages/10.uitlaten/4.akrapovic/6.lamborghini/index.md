title: Lamborghini
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Lamborghini Aventador LP 700-4 Coupé/Roadster</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium-Inconel)</strong></p><p>Dit perfect met precisie vervaardigde titaniumsysteem verhoogt het vermogen van de Aventador nog verder, terwijl het een enorme gewichtsbesparing van 46% biedt ten opzichte van de standaard uitlaat. Gemaakt van hoogwaardig titanium, gietstukken uit de eigen gieterij van Akrapovič en Inconel rechte buizen, is de Slip-On ontworpen om perfect te passen met de Link-pipe set van akrapovic &nbsp;of het standaard systeem. De geluidstechnici van Akrapovič hebben intensief gewerkt om het systeem een ​​sprankelend V12-racegeluid te geven dat een plezierige en comfortabele auditieve ervaring in de auto is en een opvallende ''wow-factor'' produceert voor voorbijgangers. De Slip-On voor de Aventador is het eerste uitlaatsysteem met de nieuw ontwikkelde geavanceerde Akrapovič-geluidskit om de uitlaatkleppen te bedienen, waardoor de bestuurder een dual-mode-optie heeft om een ​​diep sportief geluid te kiezen of meer normaal Akrapovič-geluid. Dit wordt bereikt met slechts een druk op de knop op de Sound Remote Controller of via de downloadbare app, zodat de bestuurder het unieke geluid van het Akrapovič-uitlaatsysteem volledig kan waarderen. De uitstekend afgewerkte uitlaatpijpen van koolstofvezel en titanium en de nauwgezette aandacht voor detail op het hele systeem, garanderen dat de Akrapovič Slip-On perfect synchroniseert met de mooi geproportioneerde achterkant van de Aventador.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€13.198,68 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/lamborhini-aventador-lp-700-4.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2><strong>Lamborghini Gallardo LP550-2</strong></h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>Het nauwkeurig ontworpen uitlaatsysteem met slip-on-prestaties van titaniumlegering vermindert het gewicht van deze supercar en verbetert de handling. We hebben extra vermogen gewonnen door een verbeterde dynamiek van de gasstroom en een daling van de tegendruk. Als aanvulling op ons ontwerp van de uitlaatdemper hebben we onze bekende gegoten titanium dual-mode uitlaatkleppen toegevoegd om een ​​virtuele symfonie van de grote V10 te creëren. Als er geen optionele Akrapovic-geluidskit voor instelbaar geluid wordt gebruikt, zijn de kleppen bij lagere motortoerentallen en tijdens cruisebediening gesloten en produceren een diepere en scherpere uitlaattonen. Bij wijd open gashendel gaan de kleppen open.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€7.918,59 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/lamborghini-gallardo-lp-550-2.png
              -
                type: text
                text: '<p><br></p>'
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Lamborghini Huracán LP580-2 Coupé/Spyder&nbsp;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>De slip-on-lijn is nauwkeurig ontworpen van een hoogwaardige titaniumlegering, die is ontworpen om het gewicht aanzienlijk te verminderen en de bediening te verbeteren, heeft grotere hoofdbuizen dan het standaardsysteem en een speciaal ontworpen X-onderdeel (voor dwarsstroom) in de uitlaat om tegendruk te verminderen en het vermogen€ en koppel over het hele toerenbereik te verhogen. Het unieke gegrom van de natuurlijk afgezogen V10-motor wordt verbeterd naarmate de prestaties toenemen en levert een onderscheidende toon die de zintuigen prikkelt, zonder ongewenst cabinegeluid. Bij lagere toeren en tijdens cruisebediening zijn de kleppen gesloten, waardoor een diepere en scherpere uitlaattonen worden geproduceerd, maar wanneer de toeren stijgen en de kleppen openen, wordt de volledige gepassioneerde soundtrack van de motor vrijgegeven. Dit prachtige systeem is perfect afgewerkt met vier carbon eindpijpen, waardoor deze uitlaat er net zo goed uitziet als hij klinkt en presteert.</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€10.070,10 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/lamborghini-huracan-lp-580-2-coupe-spyder.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Lamborgini Huracán LP610-4 Coupé/Spyder</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Line (Titanium)</strong></p><p>De slip-on-lijn is nauwkeurig ontworpen van een hoogwaardige titaniumlegering, die is ontworpen om het gewicht aanzienlijk te verminderen en de bediening te verbeteren, heeft grotere hoofdbuizen dan het standaardsysteem en een speciaal ontworpen X-onderdeel (voor dwarsstroom) in de uitlaat om tegendruk te verminderen en het vermogen€ en koppel over het hele toerenbereik te verhogen. Het unieke gegrom van de natuurlijk afgezogen V10-motor wordt verbeterd naarmate de prestaties toenemen en levert een onderscheidende toon die de zintuigen prikkelt, zonder ongewenst cabinegeluid. Bij lagere toeren en tijdens cruisebediening zijn de kleppen gesloten, waardoor een diepere en scherpere uitlaattonen worden geproduceerd, maar wanneer de toeren stijgen en de kleppen openen, wordt de volledige gepassioneerde soundtrack van de motor vrijgegeven. Dit prachtige systeem is perfect afgewerkt met vier carbon eindpijpen, waardoor deze uitlaat er net zo goed uitziet als hij klinkt en presteert.</p><p><br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€10.070,10 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/lamborghini-huracan-lp-580-2-coupe-spyder-1569938704.png
hero-img: /assets/img/dsc_8361.jpg
banner_size: is-fullheight
subtitle: 'Akrapovic uitlaatsystemen voor'
has_cta: true
cta_title: 'STAAT UW AUTO ER NIET TUSSEN?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: a87b8e72-818c-47b1-8a63-e7597812ddc7
