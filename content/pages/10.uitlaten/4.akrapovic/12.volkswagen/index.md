title: Volkswagen
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Volkswagen Golf GTI / Performance / TCR (MK7 / 7.5)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Slip-on Race Line (Titanium)</strong></p><p>Akrapovič zorgt voor een revolutie in het hot hatch-segment met volledig titanium Slip-On Race-uitlaatsystemen voor de VW Golf GTI. Duurzame, lichte en prestatiegerichte titaniumlegeringen, een kenmerk van Akrapovič-uitlaatsystemen voor supercars en winnaars van 24-uurs races, bieden exclusiviteit voor eigenaren van de nieuwe GTI. Het unieke lichtgewicht Slip-On Race Line uitlaatsysteem van Akrapovič voor de Golf GTI levert een premium touch door dat diepe, sportieve Akrapovič geluid, prestaties en exclusieve ontwerp, benadrukt door twee uitstekende carbon eindpijpen.&nbsp;</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.087,79 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/volkswagen-golf-gti.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Volkswagen Golf GTI / Performance / TCR (MK7 / 7.5)</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: |
                  <p><strong>Downpipe / Link Pipe (SS)</strong></p><p>Voor ultiem afstemmingspotentieel en extra gewichtsreductie bieden wij een roestvrijstalen neerwaartse buis / koppelingsbuis met een hoogwaardige katalysator. Dit pakket geeft je geluid een volledige race-upgrade. Met verhoogd cabinegeluid, is dit pakket gemaakt voor bestuurders die geen compromissen sluiten. "Extreem" is het woord hier.
                  
                  Deze high-flow downpipe vereist herprogrammering van de ECU.&nbsp;</p><p>&nbsp;De productafbeelding toont de downpipe / link-buis samen met het slip-on-systeem.</p>
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.069,40 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/volkswagen-golf-gti-downpipe.png
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/dsc_0543.jpg
banner_size: is-fullheight
subtitle: 'Akrapovic uitlaatsystemen voor'
has_cta: true
cta_title: 'Staat uw auto er niet tussen?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: f73645a1-8a93-434c-bff0-336d8c4a8a99
