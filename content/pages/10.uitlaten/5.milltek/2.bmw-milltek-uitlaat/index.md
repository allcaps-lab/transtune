title: BMW
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>1 Series M 135i 3 &amp; 5 Door (F21 &amp; F20) 2012 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Road. Polished Tips&nbsp;</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp;&nbsp;</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.362,06 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/image-coming-soon-1574852570.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>1 Series M140i 3 &amp; 5 Door (F20 &amp; F21 LCI - Non-OPF models only) 2015-2018</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Cat Back Resonated with Polished GT-90 Trims (EC Approval Coming Soon)</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp;&nbsp;</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.620,92 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m140i-f20-f21-non-opf-ssxbm1040.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>1 Series M140i 3 &amp; 5 Door (F20 &amp; F21 LCI - OPF models only) 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>GPF Back - Resonated with Polished GT-90 Trims - EC Approved</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp; &nbsp;</em>&nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.500,30 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m140i-f20-f21-opf-ssxbm1111.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>2 Series F22 M235i Coupé 2014 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Polished Tips - EC Approval Coming Soon&nbsp;&nbsp;</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp; &nbsp;</em></p><p>&nbsp; &nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.353,83 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m235i-f22-ssxbm987.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>2 Series M240i Coupe (F22 LCI- Non-OPF equipped models only) 2015-2018</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Cat Back Resonated with Polished GT-90 Trims - EC Approval Coming Soon&nbsp;</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp; &nbsp;</em><br></p><p><br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.483,30 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m240i-f22-non-opf-ssxbm1050.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>2 Series M240i Coupe (F22 LCI- OPF equipped models only) 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>GPF Back - Resonated with Polished GT-90 Trims - EC Approved</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp; &nbsp;</em></p><p>&nbsp; &nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.403,87 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/image-coming-soon-1574854278.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>2 Series F87 M2 Coupé 2016 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Hollowtek Twin Valved system with Polished GT-90 Trims - EC Approved&nbsp;&nbsp;</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp;&nbsp;</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.158,26 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m2-f87-coupe-ssxbm1033.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>2 Series F87 M2 Competition Coupé 2018 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>GPF/OPF Back System with Quad GT-90 Polished Trims - EC Approved</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp;&nbsp;</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.488,99 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m2-competition-f87-coupe-ssxbm1080-1574855067.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>3 Series F80 M3 &amp; M3 Competition Saloon (Non OPF/GPF Models Only) 2014-2018</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>EC-Approved. Polished Tips - OE System requires cutting</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp;</em>&nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.259,50 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m3-f80-competition-non-opf-gpf-ssxbm992-1574855854.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>3 Series F80 M3 &amp; M3 Competition Saloon (OPF/GPF Models Only) 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>GPF/OPF Back - Road - Polished GT-90 Trims - EC Approved</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp; &nbsp;</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.149,97 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/image-coming-soon-1574857782.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>4 Series F82/83 M4 Coupe/Convertible &amp; M4 Competition Coupé (Non-OPF equipped models only) 2014-2018</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>EC-Approved. Polished Tips - OE System requires cutting</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp; &nbsp;</em>&nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.259,50 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m4-competition-f82-non-opf-ssxbm992.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>4 Series F82/83 M4 Coupe/Convertible &amp; M4 Competition Coupé (OPF/GPF equipped models only) 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>GPF/OPF Back - Road - Polished GT-90 Trims - EC Approved&nbsp;&nbsp;</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp; &nbsp;</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.149,97 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/image-coming-soon-1574858434.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>5 Series M5 Saloon M TwinPower Turbo V8 (F10) 2011 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Polished Tips (EC Approved)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp; &nbsp;</em>&nbsp;</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€3.144,86 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/bmw-m5-f10-v8-twinpower-turbo-ssxbm1016.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/dsc_5077-1574200338.jpg
banner_size: is-fullheight
subtitle: 'MILLTEK SPORT UITLAATSYSTEMEN VOOR'
has_cta: true
cta_title: 'Staat uw auto er niet tussen?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: 13ca403e-8d58-40de-89c1-7b95cfeb3a6e
