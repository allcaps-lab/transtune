title: Mercedes-AMG
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>A-Class A45 AMG 2.0 Turbo 2012 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Resonated Valved - EC Approved&nbsp;</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.774,04 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mercedes-benz-a45-amg-2.0-turbo-ssxmz113.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>A-Class A35 AMG 2.0 Turbo (Hatch Only) 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>GPF/OPF Back System - Valved - Connects to OE Tailpipes - EC Approved&nbsp;</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.202,54 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mercedes-benz-a35-amg-2.0-turbo-ssxmz124.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>CLA-Class CLA45 AMG 2.0 Turbo 2013 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Cat Back Resonated Valved - EC Approved</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.756,46 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mercedes-benz-cla45-amg-2.0-turbo-ssxmz110.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>G-Class G63S (W463) 4.0 Bi-Turbo V8 (UK/European GPF/OPF Cars Only) 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Cat Back Valved with Carbon JET-90 Trims - Re-uses OE Valve Motors</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€3.520,11 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mercedes-benz-g63s-amg-ssxmz128.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/dsc04213.jpg
banner_size: is-fullheight
subtitle: 'MILLTEK SPORT UITLAATSYSTEMEN VOOR'
has_cta: true
cta_title: 'Staat uw auto er niet tussen?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: b5237d07-3ff6-4029-af0e-64f9ff9730d0
