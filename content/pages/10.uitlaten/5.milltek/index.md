title: Milltek
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h2>MILLTEK SPORT</h2><p><strong>Milltek Sport</strong> Is Opgericht in 1983 door een echte autoliefhebber, uitgegroeid tot een van de toonaangevende fabrikanten van performance-uitlaatsystemen voor een voortdurend groeiend assortiment voertuigen.
          
          Met hoofdkantoor in het Verenigd Koninkrijk en een ontwikkelings- en testcentrum op de iconische Nürburgring, Duitsland.&nbsp;</p>
      -
        type: simple_image
        simple_image: /assets/img/milltek.for-light.png
      -
        type: text
        text: '<p>Ze ontwerpen, ontwikkelen en testen hun hele assortiment uitlaatsystemen in eigen huis met behulp van de nieuwste apparatuur, zoals de nieuwste All Electrical CNC Mandrel Tube Benders en Faro 3D Laserscanners, gecombineerd met een van de meest ervaren teams van ingenieurs en productiebedrijven Medewerkers in de branche stellen ons in staat om als eerste marktproducten en innovatieve ontwerpen te blijven leveren.</p>'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Merken</h2>'
      -
        type: pages
        pages:
          - 32387445-96c9-4f2a-b0d0-6c69ac95da98
          - 13ca403e-8d58-40de-89c1-7b95cfeb3a6e
          - 54604eb4-b919-476d-a662-67a7c91ceeb3
          - b5237d07-3ff6-4029-af0e-64f9ff9730d0
      -
        type: pages
        pages:
          - 0a258eb7-c34b-487b-a198-5db8d3972716
          - 9b4a33a7-93be-4384-9879-4f08fec7cd42
          - fde356e4-cab9-4c1a-934d-6f6d2b3852d7
          - b6d1cd66-49e6-4987-80da-179139a2dd30
      -
        type: pages
        pages:
          - 8173eb0e-e7a3-45a1-8cdb-4a66891afd79
hero-img: /assets/img/1408.jpg
banner_size: is-fullheight
subtitle: 'De leider van de Performance uitlaatsystemen'
has_cta: true
cta_title: 'Milltek Uitlaat systeem'
cta_subtitle: 'Beschikbaar bij Transtune'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Neem contact met ons op'
menu: none
is_hidden: false
fieldset: default
id: c95dc676-085d-4213-a5d3-d2456d723ffc
