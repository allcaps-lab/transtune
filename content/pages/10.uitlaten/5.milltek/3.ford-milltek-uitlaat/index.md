title: Ford
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>F150 Raptor 3.5 V6 EcoBoost 2018 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Cat Back with GT-115 Polished Trims</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp;&nbsp;</em>&nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.605,38 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/ford-raptor-3.6-ecoboost-ssxfd317.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Focus Mk3 RS 2.3-litre EcoBoost 4wd 5-Door Hatchback 2016 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>EC Approved - Resonated with Polished GT-115 Trims&nbsp;</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp;&nbsp;</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.703,19 incl 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/ford-focus-rs-mk3-2.3-ecoboost-ssxfd190-1574861781.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Mustang 5.0 V8 GT (Fastback) 2015-2018</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Dual Outlet Resonated (Quieter) with Polished GT-100 Tips (EC Approved)</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp;&nbsp;</em>&nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.860,88 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/ford-mustang-gt-5.0-v8-fastback-ssxfd152.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Mustang 2.3 EcoBoost (Fastback) 2015-2018</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Dual Outlet Resonated with GT-100 Polished Trims (EC Approved)</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp;&nbsp;</em>&nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.791,74 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/ford-mustang-2.3-ecoboost-ssxfd173.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Mustang 5.0 V8 GT (Fastback - Facelift Model) 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Cat Back with Polished GT-100 Trims - EC Approval Coming Soon</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.&nbsp;&nbsp;</em>&nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.381,06 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/image-coming-soon-1574862474.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/milltek-ford.jpg
banner_size: is-fullheight
subtitle: 'MILLTEK SPORT UITLAATSYSTEMEN VOOR'
has_cta: true
cta_title: 'Staat uw auto er niet tussen?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: 54604eb4-b919-476d-a662-67a7c91ceeb3
