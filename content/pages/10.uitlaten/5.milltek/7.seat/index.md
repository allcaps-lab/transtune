title: Seat
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Ateca Cupra 300 4Drive (GPF/OPF Models Only) 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>GPF/OPF Back - Resonated with GT-100 Polished Trims - EC Approval Coming Soon</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em></p><p>&nbsp; &nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.191,70 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/seat-alteca-cupra-300-ssxse223.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Ibiza Cupra 1.8TFSI (6P) 2016 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat Back</strong></p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€801,40 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/seat-ibiza-cupra-1.8-tfsi-ssxse136.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Leon FR 1.4 TSI SC and 5-door 2013 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Resonated (quieter) with GT-80 Polished Trims</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.148,02 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/image-coming-soon-1574866951.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Leon Cupra 280 &amp; 290 2.0 TSI (Non OPF/GPF Models) 2014-2017</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>EC-Approved. Polished Oval Tips</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.379,24 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/seat-leon-cupra-280-290-2.0-tsi-ssxse157.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Leon Cupra 300 2.0 TSI (Non-OPF/GPF Models) 2018-2019</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Resonated Polished Ovals - EC Approved</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.674,28 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/seat-leon-cupra-300-2.0-tsi-ssxse213.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Leon Cupra 290 3 &amp; 5 Door Hatch (GPF/OPF Equipped Models Only) 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Resonated - Dual Polished Oval - EC Approved<br></p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.340,11 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/image-coming-soon-1574867782.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Leon ST Cupra 280 &amp; 290 2.0 TSI (280 &amp; 290PS - Non-OPF/GPF Models) 2015 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Polished Oval Tips. EC Approved</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.811,68 incl 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/seat-leon-cupra-280-290-2.0-tsi-non-opf-gpf-ssxse168.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Leon ST Cupra 300 (4x4) Estate / Station Wagon / Combi (Non-OPF/GPF) 2017-2018</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Polished Oval Tips. EC Approved</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.918,67 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/image-coming-soon-1574868357.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/seat-leon-milltek.jpg
banner_size: is-fullheight
subtitle: 'MILLTEK SPORT UITLAATSYSTEMEN VOOR'
has_cta: true
cta_title: 'Staat uw auto er niet tussen?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: fde356e4-cab9-4c1a-934d-6f6d2b3852d7
