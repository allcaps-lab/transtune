title: MINI
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Mk3 (F56) Mini Cooper S 2.0 Turbo (UK and European models) - Pre Facelift Models 2014-2018</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Resonated (quieter). Polished Tips</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.270,66 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mini-cooper-s-2.0-turbo-pre-facelift-ssxm407-1574864554.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Mk3 (F56) Mini Cooper S 2.0 Turbo (UK and European models) - LCI with GPF/OPF Only 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>GPF Back with Polished GT-90 Trims - EC Approval Coming Soon</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.031,62 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/mini-cooper-s-2.0-turbo-facelift-ssxm445.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/mini_jcw_22.jpg
banner_size: is-fullheight
subtitle: 'MILLTEK SPORT UITLAATSYSTEMEN VOOR'
has_cta: true
cta_title: 'Staat uw auto er niet tussen?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: 0a258eb7-c34b-487b-a198-5db8d3972716
