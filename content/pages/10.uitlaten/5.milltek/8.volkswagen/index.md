title: Volkswagen
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Golf MK7 GTi (including GTi Performance Pack, Clubsport &amp; Clubsport S models) 2013-2016</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>E-Mark System. Polished Tips</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.334,24 incl 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/volkswagen-golf-gti-mk7-ssxvw225.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Golf MK7 R Estate / Variant 2.0 TSI 300PS 2015-2017</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Resonated uses OE Trims in rear Diffuser - EC Approved</p><p>(Non-resonated mogelijk)&nbsp;</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.717,52 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/image-coming-soon-1574869180.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Golf MK7.5 GTi (Non Performance Pack Models &amp; Non-GPF Equipped Models Only) 2017-2018&nbsp;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Cat Back Resonated GT-100 Polished Trims - EC Approved</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.354,30 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/volkswagen-golf-gti-mk7.5-ssxvw457.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Golf MK7.5 GTi (Performance Pack Models &amp; Non OPF/GPF Equipped Models Only) 2017-2018</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Cat Back Resonated GT-100 Polished Trims - EC Approval Coming Soon</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.508,71 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/volkswagen-golf-gti-mk7.5-performance-pack-ssxvw497-1574871842.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Golf MK7.5 GTi (TCR &amp; Performance Pack Models - GPF/OPF Equipped Models Only) 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Cat Back Resonated GT-100 Polished Trims - EC Approved</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp;</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.283,14 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/volkswagen-golf-gti-mk7.5-tcr-performance-pack-ssxvw534.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Golf MK7 R 2.0 TSI 300PS 2014-2016</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Resonated (quieter). Polished GT-100 Round Trims&nbsp;</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em></p><p>&nbsp; &nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.125,80 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/volkswagen-golf-r-mk7.5-300ps-ssxvw399.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Golf Mk7.5 R 2.0 TSI 310PS (Non-GPF Equipped Models Only) 2017 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Resonated (quieter). Polished Oval Tips -- EC Approved&nbsp;&nbsp;</p><p>(Non-resonated mogelijk)</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.409,24 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/volkswagen-golf-r-mk7.5-non-gpf-ssxvw421.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Golf Mk7.5 R 2.0 TSI 300PS (GPF Equipped Models Only) 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>GPF Back - Polished GT-100 Tips - EC Approval Coming Soon</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em>&nbsp; &nbsp;</p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.966,39 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/volkswagen-golf-r-mk7.5-gpf-ssxvw421.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Golf Mk7.5 R Estate / Variant 2.0 TSI 310PS (Non-GPF Equipped Models Only) 2018 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Polished Quad Oval Tips. EC Approved</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em></p><p>&nbsp; &nbsp; &nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€2.1404,41 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/image-coming-soon-1575722492.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Golf Mk7.5 R Estate / Variant 2.0 TSI 300PS (GPF Equipped Models Only) 2019 -&gt;</h2>'
      -
        type: columns
        columns:
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>Cat-back</strong></p><p>Polished Quad Oval Tips. EC Approval Coming Soon</p><p><em>Ons assortiment Cat-Back-systemen is perfect geschikt voor iedereen die zijn geluid en esthetiek wil verbeteren, ontworpen als een directe vervanging voor het originele systeem, beschikbaar in meerdere configuraties waarmee de eigenaar de toename van het volume van zijn uitlaat kan bepalen en / of een meer geschikte of agressievere toon toe kan voegen. Al onze producten zijn gemaakt van 304L roestvrij staal "Aircraft-steel". Dit hoogwaardige materiaal zorgt ervoor dat uw Cat-Back-systeem bestand is tegen alle barre omstandigheden.</em></p><p>&nbsp; &nbsp;&nbsp;<br></p>'
          -
            type: column
            content:
              -
                type: text
                text: '<p><strong>€1.795,84 incl. 21% BTW</strong></p>'
              -
                type: full_height_image
                full_height_image: /assets/img/image-coming-soon-1575722691.jpg
      -
        type: button_link
        link: de627bca-7595-429e-9b41-ad58703916d7
        button_text: 'Maak een afspraak'
hero-img: /assets/img/volkswagen-golf-r-mk7-milltek.JPG
banner_size: is-fullheight
subtitle: 'MILLTEK SPORT UITLAATSYSTEMEN VOOR'
has_cta: true
cta_title: 'Staat uw auto er niet tussen?'
cta_subtitle: 'VUL HET CONTACTFORMULIER IN EN WIJ NEMEN CONTACT MET U OP!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: 'Contactformulier invullen'
is_hidden: false
fieldset: default
id: b6d1cd66-49e6-4987-80da-179139a2dd30
