title: 'Eerste Autotechnicus'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: |
          <h1><strong>Eerste Auto Technicus </strong></h1><p><strong>Zelfstandige samenwerker met passie voor auto’s&nbsp;</strong><strong>&nbsp;</strong></p><h2><strong>Dit ga je doen</strong></h2><p>Als
          1<sup>ste </sup>Auto Technicus ben je verantwoordelijk voor het voorbereiden en
          uitvoeren van alle, vaak complexere, transmissie service van twee-persoon
          auto’s.&nbsp; &nbsp;</p><h2><strong>Je
          werkzaamheden omvatten onder andere</strong></h2><ul><li>Verrichten van transmissie service
               werkzaamheden zoals:</li><ul><li>Aan de hand van mondelinge opdracht demonteren,
                reinigen, spoelen, monteren en testen van de transmissie</li><li>Met klant en manager afstemmen van de
                voortgang van de werkzaamheden, gevonden technische problemen </li><li>Instrueren van de klant over het juiste
                gebruik en onderhoud van de automaat</li></ul><li>Opstellen planning van de eigen werkzaamheden</li><li>Bij afwezigheid van de
               manager contact met klanten onderhouden en telefoon beantwoorden<br><strong></strong></li></ul><h2><strong>Heb jij het juiste profiel?</strong></h2><p>Om je
          taken goed uit te oefenen, bezit jij de volgende ervaring en vaardigheden:</p><ul><li>Sleutelen
               zit in je bloed en auto’s zijn je passie;</li><li>Minimaal 5
               jaar werkervaring als allround autotechnicus en minimaal een afgeronde
               opleiding Eerste Auto Technicus (niveau 3);</li><li>Enige
               ervaring met transmissie is een pré;</li><li>Je werkt
               systematisch, bent proactief en probleemoplossend, service-gericht en hebt
               hart voor de zaak;</li><li>De klant staat voorop, je hebt een klant- en servicegerichte
               instelling;</li><li>Flexibel
               in werktijden, want je bent er ook wel eens op zaterdag of blijft langer
               als dat nodig is en als het rustig is, werk je wat minder uren die dag</li><li>Rijbewijs
               B (personenauto);<strong>&nbsp;</strong></li></ul><h2><strong>Dan bieden we jou</strong></h2><ul><li>Een goed
               salaris passend bij jouw ervaring</li><li>Een
               prettige werksfeer, met leuke collega’s in een dynamisch en groeiend
               bedrijf</li><li>De ruimte
               en vrijheid om zelfstandig je werkzaamheden uit te voeren en mee te denken
               over de verdere groei van TransTune</li><li>Een
               moderne, veilige en schone werkplaats met de nieuwste gereedschappen en
               middelen</li><li>Plezier
               vinden we belangrijk. Daarom doen we regelmatig leuke dingen met het team.</li><li>Fulltime
               dienstverband van 40 uur per week voor een langere periode</li><li>TransTune
               volgt de CAO Motorvoertuigen en Tweewielerbedrijven&nbsp;<strong>&nbsp;</strong></li></ul><h2><strong>Wie zijn
          wij?</strong></h2><p>TransTune in Deurne is dè specialist
          in transmissie service en het beste adres voor chiptuning.<br><br>Transtune
          is een jong en innovatief bedrijf in het Brabantse Deurne, vlakbij Eindhoven.
          Het is opgericht door Harm van Bussel, een van de jongste automotive
          ondernemers in Nederland. Maar aan technische expertise is er geen gebrek! Harm
          groeide op tussen de auto's, sleutelde al vroeg aan transmissies en maakte dit
          in combinatie met zijn kennis van auto-elektronica een bloeiend bedrijf.&nbsp;</p><p>&nbsp;<br></p><p>We willen
          je graag nog meer vertellen maar doen dat liever onder het genot van een kop
          koffie.</p><p><strong>Graag
          nodigen we je uit voor een 1ste kennismaking, durf jij het aan? Neem dan
          contact met Annerie van der Sterre op 06-51664784 en email je cv naar </strong><a href="mailto:info@atmaram.nl"><strong>info@atmaram.nl</strong></a><strong>.</strong></p><p><strong>Let op:
          Sollicitatiegesprekken vinden plaats op vrijdag 5 juli a.s. vanaf 13:00 uur in
          Deurne. Reserveer deze datum vast in je agenda!</strong></p><p>&nbsp;<br></p><p><strong>Heb je vragen naar aanleiding van deze
          vacature?</strong></p><p>Neem contact op met Harm van Bussel:
          0493-312658 </p><p>Kijk ook eens op onze website: www.TransTune.nl</p><p><em>&nbsp;<br></em></p><p><em>&nbsp;<br></em></p><p><em>&nbsp;<br></em></p><p><em>Acquisitie naar aanleiding van deze
          vacature wordt niet op prijs gesteld.</em></p>
has_cta: false
is_hidden: false
seo:
  title: 'Vacature Eerste Auto Technicus - Deurne - Transtune.nl'
  description: 'Eerste Auto Technicus. Wilt u werken bij een innovatief, jong en een vooral erg leuk bedrijf. Transtune is specialist in transmissie service en chiptuning. Lees meer over de vacature'
fieldset: default
id: e57009d2-f451-4f32-b633-4ff33ddeee37
