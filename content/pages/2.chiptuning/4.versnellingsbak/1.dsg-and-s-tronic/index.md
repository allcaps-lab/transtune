title: 'DSG & S-tronic'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>DSG tuning stage 1</h1><p>Geschikt voor alle DSG versnellingsbakken. Dit type tuning wordt toegepast in combinatie met&nbsp;chiptuning van de motor&nbsp;waarbij het koppel hoger wordt dan het maximum toelaatbare van de standaard DSG software.</p><h2>Werkwijze</h2><ul><li>Verhogen koppelbegrenzing en koppelingsdruk.</li><li>DSG6 (natte koppeling) van 350 Nm naar 450 Nm of meer</li><li>DSG7 (droge koppeling)van 275 Nm naar 330 Nm</li><li>DSG7 (natte koppeling) van 500 Nm naar 800 Nm of meer</li><li>Aanpassen maximaal schakeltoerental zodat dit beter aansluit op koppelkromme na chiptuning van de motor.</li></ul><h2>Prijzen</h2><p>€ 195,- incl btw&nbsp;in combinatie met chiptuning</p><p>€ 350,- incl btw losstaand</p>'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h1>DSG tuning stage 2</h1><p>Heeft u&nbsp;klachten over het schakelgedrag&nbsp;van uw auto? Kies dan voor DSG tuning stage 2. Dit is geschikt voor alle types DSG. Wij hebben veel ervaring in het ontwikkelen van verbeterde software voor uw specifieke DSG-versie.</p><h2>Werkwijze</h2><p>De software-aanpassing van uw DSG bestaat uit:</p><ul><li>Verhogen koppelbegrenzing en koppelingsdruk.</li><li>Optimaliseren schakeltoerentallen.</li><li>Aanpassen maximaal schakeltoerental.</li><li>Optimaliseren schakelsnelheid.</li><li>Kickdown aanpassen.</li><li>Manueel schakelen via flippers geen kickdown.</li><li>Optioneel kunnen we voor u ook de launch control activeren. Dit is zelfs mogelijk voor de DSG7 met een droge koppeling.</li></ul><h2>Prijzen</h2><p>€ 245,- incl btw&nbsp;in combinatie met chiptuning</p><p>€ 375,- incl btw losstaand</p>'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>DSG tuning stage 3</h1><p>Heeft u specifieke wensen welke niet te vervullen zijn met bovenstaande DSG tuning stages 1 of 2? Dan kunnen wij uw wensen toepassen in een op maat uitgevoerde DSG tuning. Dit is met name interessant voor de veeleisende en/of kritische rijder of bijvoorbeeld voor circuitgebruik.</p><h2>Werkwijze</h2><p>U maakt uw wensen aan ons kenbaar. Na een gedegen overleg tijdens diverse testritten passen we de DSG software aan totdat deze naar uw zin is.</p><p>Dit type DSG tuning bevat alle mogelijkheden van de stage 1 en stage 2 DSG tuningen, maar deze worden helemaal specifiek naar uw wens ingesteld. Extra mogelijkheden zijn er ook, denk hierbij aan:</p><ul><li>Schakelmomenten en snelheden D en/of S op maat afgesteld.</li><li>Launch control aangrijpmoment aanpassen.</li><li>Afremmen op motor in D en/of S.</li></ul><h2>Prijzen</h2><p>€ 695,- incl btw&nbsp;in combinatie met chiptuning</p><p>€ 775,- incl btw losstaand</p>'
hero-img: /assets/img/img_1348.jpg
banner_size: is-large
has_cta: true
cta_title: 'Wilt u ook meer uit uw auto halen?'
cta_subtitle: 'GEBRUIK ONZE INFO MODULE OF NEEM CONTACT OP'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: chiptuning
seo:
  title: 'DSG Tuning & S-tronic Tuning Specialist - Transtune.nl'
  description: 'Laat uw DSG versnellingsbak vakkundig tunen. Harm van Bussel is Specialist op gebied van DSG Tuning maatwerk. Maak nu een afspraak!'
fieldset: default
id: d995ac9e-7d2a-4a68-9ebc-a573ff493b06
