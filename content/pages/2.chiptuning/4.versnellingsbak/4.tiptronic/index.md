---
title: Tiptronic
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Tiptronic-tunig Stage 1</h1><p>Voor zowel de VAG-benzine- als de diesel-Tiptronic 6 (Bosch GS19) automaatbakken hebben wij softwaremodificaties ontwikkeld. Hiermee presteert uw auto beter en kunnen er zonder problemen hogere koppelwaarden behaald worden.</p><h2>Werkwijze</h2><p>In al onze tiptronic-optimalisaties worden koppelbegrenzers en koppeldrukken verhoogd. Hierdoor zijn in combinatie met een motortuning hogere koppelwaarden haalbaar.</p><p>Optioneel kan de D-stand van de tiptronic iets sportiever gemaakt worden. Tijdens deelbelast laten we de auto dan wat minder laag in toeren draaien.</p><p>Ook kunnen de maximale toerentallen iets verlegd worden, om zo meer potentie uit de motor te halen (bijvoorbeeld bij de RS6 V10 en A6 3.0 TFSI).</p><h2>Prijzen</h2><p>€ 325,- incl btw&nbsp;in combinatie met chiptuning</p><p>€ 425- incl btw losstaand</p>'
hero-img: /assets/img/img_1348.jpg
banner_size: is-large
has_cta: true
cta_title: 'Wilt u ook meer uit uw auto halen?'
cta_subtitle: 'GEBRUIK ONZE INFO MODULE OF NEEM CONTACT OP'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: chiptuning
fieldset: default
id: 336fdb22-9711-44bd-a22b-b5ce8eea42df
---
<h1><br></h1>