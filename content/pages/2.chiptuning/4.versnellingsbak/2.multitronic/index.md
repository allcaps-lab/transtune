title: Multitronic
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Multitronic tuning stage 1</h1><p>De Multitronic automatenbakken van deze tijd worden in de fabriek (té) economisch afgesteld. Daardoor draait de motor bij lagere snelheden en lagere belastingen rond de ca. 1.000 rpm. En hierdoor is er weer vaak een dreunend geluid onder de motorkap hoorbaar. En dat wilt u niet … Bovendien is de S(Sport)-stand té sportief afgesteld, waardoor deze in het gewone verkeer moeilijk bruikbaar is. Alle reden om te tunen dus!</p><h2>Werkwijze</h2><p>Voor al deze automaatbakken hebben wij een speciaal programma waarmee we de D-, S- én M-standen optimaliseren. Daardoor presteert de auto beter en rijdt vooral een stuk prettiger. En dát wilt u wel!</p><p>Wij zorgen ervoor dat de D- en S-standen straks veel beter op het motorkarakteristiek aansluiten.</p><p>·D-stand: tussen 1.500 – 2.000 rpm (i.p.v. 1.000 – 1. 400 rpm originele MT software).</p><p>·S stand: tussen 2.250 – 2. 750 rpm (i.p.v. 3.000 rpm origineel.</p><p>·Manueel schakelen via flippers geen kickdown</p><p>Bovendien is het bij de benzinemodellen heel nuttig om ook het maximale toerental bij volle acceleratie wat te verhogen.</p><h2>Prijzen</h2><p>€ 325,- incl. btw in combinatie met chiptuning</p><p>€ 425,- incl. btw zonder chiptuning</p>'
hero-img: /assets/img/img_1348.jpg
banner_size: is-large
has_cta: true
cta_title: 'Wilt u ook meer uit uw auto halen?'
cta_subtitle: 'GEBRUIK ONZE INFO MODULE OF NEEM CONTACT OP'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: chiptuning
fieldset: default
id: cd52ce60-f7ea-4cec-8125-6fdf9e0dfff6
