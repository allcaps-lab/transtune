title: 'wat is dual-clutch'
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Wat is een dual clutch transmissie?</h1><p><strong><em>of een dubbelekoppelingversnellingsbak?</em></strong></p><p><em>Een dual clutch transmissie (DCT) oftewel een&nbsp;<u>dubbelekoppelingversnellingsbak</u>&nbsp;<a href="https://nl.wikipedia.org/wiki/Dubbelekoppelingversnellingsbak">https://nl.wikipedia.org/wiki/Dubbelekoppelingvers...</a>&nbsp;is een type halfautomatische versnellingsbak die voorzien is van twee koppelingen. Op de ene koppeling bevinden zich de even versnellingen en op de andere koppeling de oneven versnellingen.</em></p><h2>Het voordeel van dual clutch transmissie</h2><p>Het grote voordeel van een versnellingsbak met twee koppelingen is dat men kan schakelen zonder krachtonderbreking. Er wordt namelijk niet direct van versnelling gewisseld, maar van koppeling; van oneven naar even, enz.</p><p>Als men vertrekt vanuit stilstand in de eerste versnelling draait de eerste koppeling; de tweede koppeling draait ondertussen al mee. Zodra wordt geschakeld, ontkoppelt de eerste koppeling. Gelijktijdig wordt de tweede koppeling gekoppeld. Op deze manier kan onder belasting worden geschakeld.</p>'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Slimme elektronica helpt daarbij</h2><p>Slimme elektronica zorgt er ook voor dat de volgende versnelling reeds wordt ingeschakeld op de ontkoppelde koppeling. De computer doet een voorspelling op basis van verschillende variabelen. Denk aan het gemeten toerental en de input van de bestuurder.</p><p>Hierdoor kan veel sneller van versnelling worden gewisseld dan bij een manuele versnellingsbak. Samen met het eerste voordeel (geen krachtonderbreking), zorgt dit voor een zeer lineaire en ononderbroken acceleratie.</p><p>De elektronica kan ook automatisch de koppelingen bedienen. De meeste dual clutch transmissies zijn dan ook een automatische versnellingsbak.</p><h2>Waarom Dual Clutch-tuning?</h2><p>Transtune kan diverse&nbsp;versnellingsbakken tunen. Hiervoor kunnen er twee aanleidingen zijn.</p><p>Bij chiptuning.&nbsp;Als u uw motor laat chiptunen kan het verwijderen van de koppelbegrenzing nodig zijn, zodat de versnellingsbak het extra koppel ook kan verwerwerken.</p><p>Bij klachten over schakelgedrag.&nbsp;Heeft u klachten over het schakelgedrag, dan kan Transtune de momenten waarop de bak schakelt aanpassen. Dit kan voor zowel het opschakelen als voor het terugschakelen. Tevens kunnen de schakelsnelheden worden geoptimaliseerd.</p>'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Geen oplossing voor een mechanisch probleem</h2><p>Is er een mechanisch probleem of een mechanische storing in dual clutch transmissie? Dan is een&nbsp;dual clutch tuning&nbsp;hiervoor niet de oplossing. Laat u hierover niets wijs maken. Neem bij problemen met uw transmissie altijd eerst&nbsp;contact&nbsp;met ons op voor een diagnose en voor een advies om het probleem op te lossen. Of … of maak meteen een&nbsp;afspraak.</p><blockquote><p>Tunen van een dual clutch transmissie kan slim zijn als uw motor is gechiptuned of als er problemen zijn met het schakelgedrag. Maar… tunen is nooit een oplossing voor een mechanisch probleem. Vraag ons altijd eerst om een diagnose en om een advies.</p></blockquote>'
hero-img: /assets/img/img_1348.jpg
banner_size: is-large
has_cta: true
cta_title: 'Wilt u ook meer uit uw auto halen?'
cta_subtitle: 'GEBRUIK ONZE INFO MODULE OF NEEM CONTACT OP'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: chiptuning
fieldset: default
id: 76e7a69d-472e-4d16-a891-3098f04bbe62
