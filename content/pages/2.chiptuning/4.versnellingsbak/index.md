title: Versnellingsbak
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Versnellingsbak</h1><p>Wij zijn specialist in transmissie-soft &amp; hardware. Wij zijn gespecialiseerd en gecertificeerd en bieden meer dan u verwacht. Wij bewijzen ons graag!</p>'
      -
        type: pages
        pages:
          - d995ac9e-7d2a-4a68-9ebc-a573ff493b06
          - cd52ce60-f7ea-4cec-8125-6fdf9e0dfff6
          - 9e095440-ab32-4fae-a2a7-f10f4f119de4
          - 336fdb22-9711-44bd-a22b-b5ce8eea42df
          - 76e7a69d-472e-4d16-a891-3098f04bbe62
hero-img: /assets/img/img_1348.jpg
banner_size: is-large
has_cta: true
cta_title: 'Wilt u ook meer uit uw auto halen?'
cta_subtitle: 'GEBRUIK ONZE INFO MODULE OF NEEM CONTACT OP'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: chiptuning
fieldset: default
id: f5d794de-e9b6-4a5a-9ade-2318e4da0869
