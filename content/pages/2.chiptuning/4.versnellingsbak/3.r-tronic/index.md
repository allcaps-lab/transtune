---
title: R-tronic
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h2>Lamborghini Huracan/Audi R8</h2><p>Deze auto is voorzien van 0BZ DL801 transmissie, de automaat is ontwikkeld voor een vermogen van maximaal 510NM koppel. Wij kunnen deze transmissie upgraden naar 1100 NM koppel! Deze upgrade is nodig bij intensief straat en circuit gebruik.</p><p>Bij het monteren van een supercharger is dit een zeer gewenste upgrade.</p><p>Deze transmissie hardware optimalisatie kit bestaat uit:</p><ul><li>10 x Grote frictieplaten voor zwaar gebruik</li><li>10 x Kleine frictieplaten voor zwaar gebruik</li><li>22 x geharde staalplaten voor zwaar gebruik</li><li>1 x Verzwaard koppelingshuis voor de oneven versnellingen (1.3.5.7</li><li>1 x Verzwaard koppelingshuis voor de even versnellingen ( 2.4.6)</li><li>1 x Verzwaard viton koppelingshuis</li><li>1 x Verzwaarde assemblage ringen</li><li>1 x Verzwaarde afdekplaten voorkant van de transmissie</li></ul><p>Dit koppelingssysteem kan alleen volledig gemonteerd worden gekocht. Hier voor maken we geen uitzonderingen.</p><p>Voor de montage van dit systeem moet de transmissie van de motor gescheiden worden.</p><p>13.000,- incl. btw in combinatie met supercharger tuning</p><p>14.000,- incl. btw losstaand</p>'
hero-img: /assets/img/img_1348.jpg
banner_size: is-large
has_cta: true
cta_title: 'Wilt u ook meer uit uw auto halen?'
cta_subtitle: 'GEBRUIK ONZE INFO MODULE OF NEEM CONTACT OP'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: chiptuning
fieldset: default
id: 66f86c61-1f22-4684-90dd-c701b9c86468
---
<p>test
</p>