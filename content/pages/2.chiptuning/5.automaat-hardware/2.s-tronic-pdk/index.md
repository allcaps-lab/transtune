---
title: 'PDK/S-tronic DQ500'
fieldset: default
id: 16ea9667-2d11-4a13-bb16-efdfeee423e3
---
<h2>VW/Audi DQ500</h2>
<p>Deze auto is voorzien van S-tronic 7 trapstransmissie, de automaat is ontwikkeld voor een vermogen van maximaal 500 NM koppel. Wij kunnen deze transmissie upgraden naar 700 NM koppel! Deze upgrade is nodig bij intensief straat en circuit gebruik.
</p>
<p>Bij het monteren van een hybride turbo/pully is dit een zeer gewenste upgrade.
</p>
<p>Stage 1 600 NM Spec-S
</p>
<p>2.600,- incl. btw in combinatie met tuning
</p>
<p>2.800,- incl. btw losstaand
</p>
<p><br>
</p>
<p>Stage 2 700 NM Spec-R
</p>
<p>2.950,- incl. btw in combinatie met tuning
</p>
<p>3.150,- incl. btw losstaand
</p>
<p>Dit koppelingssysteem kan alleen volledig gemonteerd worden gekocht. Hier voor maken we geen uitzonderingen.
</p>
<p>Voor de montage van dit systeem moet de transmissie van de motor gescheiden worden.
</p>