---
title: 'DSG/S-tronic DQ250'
fieldset: default
id: de9a33f3-d094-4766-af06-959b0223f443
---
<h2>VW/Audi DQ250</h2>
<p>Deze auto is voorzien van DSG 6 trapstransmissie, de automaat is ontwikkeld voor een vermogen van maximaal 250 NM koppel. Wij kunnen deze transmissie upgraden naar 1100 NM koppel! Deze upgrade is nodig bij intensief straat en circuit gebruik.
</p>
<p>Bij het monteren van een hybride turbo is dit een zeer gewenste upgrade.
</p>
<p>Stage 1 550 NM
</p>
<p>1.750,- incl. btw in combinatie met tuning
</p>
<p>1.950,- incl. btw losstaand
</p>
<p><br>
</p>
<p>Stage 2 600 NM + extra koeling pakket
</p>
<p>2.600,- incl. btw in combinatie met tuning
</p>
<p>2.800,- incl. btw losstaand
</p>
<p><br>
</p>
<p>Stage 3 700 NM + extra koeling pakket (Tot 500PK)
</p>
<p>3.900,- incl. btw in combinatie met tuning
</p>
<p>4.100,- incl. btw losstaand
</p>
<p><br>
</p>
<p>Stage 4 725 NM + extra koeling pakket (tot 550PK)
</p>
<p>4.800,- incl. btw in combinatie met tuning
</p>
<p>4.950,- incl. btw losstaand
</p>
<p><br>
</p>
<p>Stage 5 825 NM + extra koeling pakket (tot 600PK)
</p>
<p>5.200,- incl. btw in combinatie met tuning
</p>
<p>4.400,- incl. btw losstaand
</p>
<p><br>
</p>
<p>High performance koeling stage 1 (Golf R)
</p>
<p>2.400,- incl. btw in combinatie met clutch upgrade
</p>
<p>2.600,- incl. btw losstaand
</p>
<p>Dit koppelingssysteem kan alleen volledig gemonteerd worden gekocht. Hier voor maken we geen uitzonderingen.
</p>
<p>Voor de montage van dit systeem moet de transmissie van de motor gescheiden worden.
</p>