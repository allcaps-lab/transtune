---
title: DCT
fieldset: default
id: 4564ab71-a1e3-434c-ac39-b4910b39ee5d
---
<h2>BMW DCT M3/M4/M5/M6</h2>
<p>Deze auto is voorzien van Gertrag 7DCI700/600 powershift transmissie, de automaat is ontwikkeld voor een vermogen van maximaal 600 NM koppel. Wij kunnen deze transmissie upgraden naar 1100 NM koppel! Deze upgrade is nodig bij intensief straat en circuit gebruik.
</p>
<p>Bij het monteren van een supercharger is dit een zeer gewenste upgrade.
</p>
<p>Stage 1 950 NM
</p>
<p>3.600,- incl. btw in combinatie met tuning
</p>
<p>3.800,- incl. btw losstaand
</p>
<p>Stage 2 1000 NM
</p>
<p>5.600,- incl. btw in combinatie met tuning
</p>
<p>5.800,- incl. btw losstaand
</p>
<p>Stage 3 1100 NM
</p>
<p>6.300,- incl. btw in combinatie met tuning
</p>
<p>6.500,- incl. btw losstaand
</p>
<p>High performance koeling stage 1
</p>
<p>3.200,- incl. btw in combinatie met clutch upgrade
</p>
<p>3.400,- incl. btw losstaand
</p>
<p>Dit koppelingssysteem kan alleen volledig gemonteerd worden gekocht. Hier voor maken we geen uitzonderingen.
</p>
<p>Voor de montage van dit systeem moet de transmissie van de motor gescheiden worden.
</p>