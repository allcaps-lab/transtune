---
title: PDK
fieldset: default
id: 47bac55a-0c59-4736-946d-22dd8f10043d
---
<p>Dit type auto is voorzien van een ZF
dual clutch transmissie 7DT.
</p>
<p>Deze transmissie is beschikbaar in
meerdere varianten, het zijn allemaal 7 traps automaten maar het verschil zit
hem in de newton meters die de transmissie kan verwerken.
</p>
<p>De transmissie is leverbaar van 390NM
Koppel tot 750NM koppel afhankelijk van model.
</p>
<p>Wij kunnen deze transmissie upgraden
naar maar liefst 950 NM koppel.
</p>
<p>Deze upgrade is nodig bij intensief
straat en circuit gebruik.
</p>
<p>Bij tuning is dit een zeer gewenste
upgrade.
</p>
<p>Deze transmissie hardware
optimalisatie kit bestaat uit:
</p>
<ul>
	<li>10 x Grote frictieplaten voor zwaar gebruik</li>
	<li>10 x Kleine frictieplaten voor zwaar gebruik</li>
	<li>22 x geharde staalplaten voor zwaar gebruik</li>
	<li>1 x Verzwaard koppelingshuis voor de oneven versnellingen (1.3.5.7</li>
	<li>1 x Verzwaard koppelingshuis voor de even versnellingen ( 2.4.6)</li>
	<li>1 x Verzwaard viton koppelingshuis</li>
	<li>1 x Verzwaarde assemblage ringen </li>
	<li>1 x Verzwaarde afdekplaten voorkant van de transmissie</li>
	<li>1 x olie filter pan Verzwaard met extra koeling</li>
</ul>
<p>Dit koppelingssysteem kan alleen
volledig gemonteerd worden gekocht. Hier voor maken we geen uitzonderingen.
</p>
<p>Voor de montage van dit systeem moet
de transmissie van de motor gescheiden worden.
</p>
<p>13.000,- incl. btw in combinatie met
tuning
</p>
<p>14.000,- incl. btw losstaand
</p>