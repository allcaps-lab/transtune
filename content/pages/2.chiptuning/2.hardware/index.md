title: Hardware
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Motortuning hardware</h1><p>Wilt u nog net wat extra vermogen of koppel? Nog meer dan dat u met&nbsp;chiptuning&nbsp;kunt realiseren? Dan kunnen wij in vrijwel alle gevallen een stapje verder gaan door de hardware van uw auto aan te passen. Denk hierbij o.a. aan:</p><ul><li>·upgraden van uit- en inlaatdelen;</li><li>·upgraden van gemodificeerde turbo’s;</li><li>·upgraden van water-methanol systemen;</li><li>·verwijderen van het roetfilter.</li></ul><p>In combinatie met&nbsp;software tuning&nbsp;kunnen upgrades het vermogen en koppel van uw auto naar een nog hoger niveau stuwen.</p><p>Door toepassing van de nieuwe hardware kunnen we de mechanisch verantwoorde grenzen van uw auto verleggen en zodoende gecreëerde ruimte benutten in onze software tuning.</p>'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Werkwijze</h2><p>Wij zorgen graag voor een optimale hardware/software-combinatie van uw auto, ontwikkeld op de testbank. Dat doen we in onze speciaal hiervoor ingerichte werkplaats. Deze is voorzien van alle essentiële gereedschappen en machines. Van bruggen voor verlaagde auto’s, tot stoel- en stuurhoezen om uw auto te beschermen tegen vuil. Wij werken met&nbsp;hoog geschoold personeel&nbsp;met een passie voor auto’s.</p>'
hero-img: /assets/img/fd82e377-8f74-4a82-aaaf-c0ff4121664e.jpeg
banner_size: is-large
subtitle: 'Haal nog net dat extra stapje vermogen uit uw auto!'
has_cta: true
cta_title: 'Wilt u ook meer uit uw auto halen?'
cta_subtitle: 'GEBRUIK ONZE INFO MODULE OF NEEM CONTACT OP'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
is_hidden: false
selector: chiptuning
fieldset: default
id: dad91b29-a37b-4ec6-9c7d-331991d3eb9a
