---
title: Methodes
has_cta: false
is_hidden: true
fieldset: default
id: 6fd26ef5-daf0-4fcc-b0bf-1e224f2f970a
---
<h1>Wat is chiptuning?</h1>
<p><i>Met chiptuning of motortuning software zijn we in staat om een motor tot hogere prestaties te brengen. We grijpen als het ware in de software van het motormanagementsysteem in.</i>
</p>
<h2>Motormanagement en de ECU</h2>
<p>De meeste motoren in moderne auto’s worden door een Electronic Control Unit (ECU) aangestuurd. Deze regelt continu onder andere:
</p>
<p>de ingespoten hoeveelheid brandstof
</p>
<p>de turbodruk;
</p>
<p>en het ontstekingstijdstip.
</p>
<p>De chips in een ECU zijn geladen met software. Bij chiptuning bewerken we de programma’s die aan de hand van vele variabelen (parameters) het motormanagement vormen. Vaak zijn deze programma’s geschreven om de prestaties van een automotor ver binnen de maximale belasting van die betreffende motor te houden. Dat biedt dus ruimte en mogelijkheden om de automotor te chiptunen, zonder dat de motor onherstelbaar beschadigd raakt.
</p>
<h2>20 tot 40% prestatieverbetering bij motoren met drukvulling</h2>
<p>Met chiptuning stemmen we de parameters opnieuw en beter op elkaar af. Dit alles resulteert bij motoren met turbo of een andere vorm van laaddrukverhoging van 20 tot 40% prestatieverbetering.
</p>
<h2>6 tot 8% prestatieverbetering bij atmosferische motoren</h2>
<p>Bij atmosferische motoren zonder enige vorm van drukvulling (zoals een turbo, een compressor of een supercharger) is de winst van chiptuning een stuk lager: ca. 6 tot 8%. Atmosferische motoren komen echter steeds minder voor.
</p>
<h2>De gevolgen</h2>
<p>We verbeteren dus het koppel en het vermogen. Hierdoor kan de auto sneller optrekken en wordt de topsnelheid verhoogd.
</p>
<p>Motoren draaien tegenwoordig erg ‘arm’ in verband met de huidige milieueisen. Door te chiptunen wordt een motor vaak juist ‘rijker’ afgesteld. Door die rijkere afstelling is het rendement beter. Bij een onveranderde rijstijl is een dus zelfs een brandstofverbruikvermindering realiseerbaar omdat het rendement van de motor wordt verhoogd. De motor wordt bovendien levendiger en reageert sneller op gaspedaalbewegingen.
</p>
<p>Dus: chiptuning leidt tot een beter motorrendement met betere prestaties en gunstiger voor het verbruik (bij gelijk blijvende rijstijl).
</p>
<h2>Chiptuning in België</h2>
<p>In België worden auto’s zelfs andersom gechiptuned. Omdat daar het motorvermogen bepalend is voor de hoogte van de wegenbelasting, worden er regelmatig motorvermogens teruggebracht. Sterke motoren worden bij de aankoop al softwarematig teruggeregeld om deze aantrekkelijker te maken voor de markt. Chiptuning waarbij de prestaties worden verbeterd is bij wet in België trouwens verboden.
</p>
<h2>Drie soorten chiptuning</h2>
<p>Feitelijk zijn er drie manieren om te chiptunen:
</p>
<p>1.<strong>Externe ECU.</strong> In theorie bestaat er de mogelijkheid om een extra ECU te plaatsen die de originele ECU manipuleert. Wij adviseren en plaatsen zo’n box, soms ook wel foutcodebox/powerbox genoemd, niet. Onze ervaring is dat er altijd problemen mee zijn die niet of nauwelijks te verhelpen zijn.
</p>
<p>2.<strong>OBD-tuning.</strong> OBD staat voor <u>On-board diagnostics</u>. Hier worden de gegevens, ook wel mappen, van het motormanagement uitgelezen via de storingsdiagnosestekker en vervolgens in een programma gewijzigd. Dit is de meest eenvoudige manier van tunen.
</p>
<p>3.<strong>BDM-tuning.</strong> Tegenwoordig hebben steeds meer ECU’s géén externe toegang. Er ontbreekt een communicatiepoort. Bij <u>BDM-tuning (Background Debugging Mode)</u> wordt er dan eerst zo’n toegangspoort op de chip gesoldeerd.
</p>
<p><i><br></i>
</p>
<h2>Wat is het verschil tussen BDM-tuning en OBD-tuning?</h2>
<p><i>In feite bestaan er twee manieren om een motor te <u>chiptunen</u>: BDM-tunen en OBD-tunen. Wat is eigenlijk het grote verschil?</i>
</p>
<h2>BDM-tuning van het motormanagement</h2>
<p><strong>De ECU.</strong> Als er een elektronisch geregelde inspuiting en ontsteking aanwezig is, ook wel een motormanagement genoemd, is <u>chiptuning</u> mogelijk. De ECU (Electronic Control Unit) van het managementsysteem is de plaats waar alle informatie van de motor binnenkomt. De computer geeft aan de hand van de gemeten gegevens, de injectie en de turbo de opdrachten.
</p>
<p><strong>Chiptuning.</strong> In de motor zitten op diverse plaatsen sensoren die informatie naar het motormanagement sturen. Door middel van chiptuning grijpen we hierop in. De computer gebruikt het programma dat de constructeur (fabrikant) heeft ingevoerd om alles te sturen. Het programma bepaalt de hoeveelheid brandstof die mag worden ingespoten en op welk tijdstip. Bij chiptuning beïnvloeden we die software.
</p>
<p><strong>TPROT.</strong> De nieuwste ECU’s hebben echter vaak een anti-chiptuning beveiliging (TPROT - Tuning Protection). Er ontbreekt dan een toegankelijke communicatiepoort. De ECU dient dan voorzichtig te worden uitgebouwd om de TPROT te omzeilen en de chips rechtstreeks aan te kunnen spreken. Er wordt dan een speciale BDM-poort op de ECU gesoldeerd, waarna de ECU weer wordt teruggeplaatst.
</p>
<h2>OBD-tuning van het motormanagement</h2>
<p><strong>OBD staat voor On-board diagnostics.</strong> Hier worden de gegevens, ook wel mappen, van het motormanagement uitgelezen via de storingsdiagnosestekker en vervolgens in een programma gewijzigd. Bij OBD tuning hoeft dus niet gesoldeerd te worden in tegenstelling met chiptuning. OBD tuning is dus eenvoudiger.
</p>
<p><strong>Levensduur na chiptuning.</strong> De levensduur van een motor wordt bij verantwoorde tuning niet bekort. Maar … dan moet de tuner (garage) vooraf precies uitzoeken heeft wat de verschillen per motorvermogen zijn. TDI-motoren van respectievelijk 90 pk en 110 pk zien er bijvoorbeeld computertechnisch hetzelfde uit, maar er zijn wel degelijk verschillen in turbo, oliekoeler, oliepomp, intercooler. Daar moet de tuner goed rekening mee houden. Motorenfabrikanten bouwen wel een behoorlijke (mechanische) veiligheidsmarge in.
</p>
<p><strong>Oude motoren wel of niet tunen?</strong> De leeftijd of kilometerstand van de motor maakt daarentegen wél uit. Het opvoeren van een motor met meer dan 350.000 km op de teller is riskant vanwege de slijtage. Dit kan men dus beter niet doen, of we moeten vooraf door middel van een <u>vermogensmeting</u> vaststellen dat de motor geen slijtage kent.
</p>
<p><strong>TPROT (Tuning Protection).</strong> Tegen OBD-tuning wordt sinds 2010 steeds strengere maatregelen genomen door de fabrikanten. Door deze anti-tuning protectiemaatregelen moeten OBD-tuners en chiptuners de verzegeling van de ECU verbreken om in de data van de ECU te kunnen komen.
</p>
<p><i>OBD-tuning is de meest eenvoudige manier van chiptunen. BDM-tuning is nodig als er sprake is van een ECU met een ant-tuning protectiemaatregel (TPROT). De chip wordt dan uitgemonteerd, toegankelijk gemaakt en weer teruggeplaatst.</i>
</p>
<p><i><br></i>
</p>
<p><i>Wat voor uw auto de mogelijkheden zijn van chiptuning, hangt sterk af van het merk, type, en type motor. Neem gerust <u>contact</u> met ons op als u meer wilt weten, of maak meteen een <u>afspraak</u>.</i>
</p>