title: Garantie
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Aanvullende garantie chiptuning</h1><p>Chiptuning is wettelijk toegestaan en heeft dan ook geen gevolgen voor uw autoverzekering. Hoewel het niet verplicht of noodzakelijk is, is het desgewenst mogelijk een aanvullende verzekering voor uw motor af te sluiten. Transtune biedt u in samenwerking met Multipart de mogelijkheid een motorverzekering af te sluiten.</p><p>Multipart werkt met een geselecteerd aantal Europese bedrijven die chiptuning aanbieden en ziet toe op de kwaliteit van de chiptuning. In tegenstelling tot veel andere aangeboden motor-verzekeringen voldoet Multipart aan alle voorwaarden op grond van de Nederlandse wet- en regelgeving en staat onder toezicht van de officiële instanties. Bij schade wordt er derhalve daadwerkelijk ook uitgekeerd.</p><p>Multipart&nbsp;biedt een product aan, dat speciaal voor getunede voertuigen is ontwikkeld.</p><p>Ze gelden voor alle voertuigen, met uitzondering van taxi’s, huurauto’s en andere voertuigen voor het zakelijk vervoeren van producten (expeditie, pakket- , koerier- en expresdiensten en spoedservice) tot 3,5 ton totaalgewicht zonder leeftijdsgrens, echter met een maximale kilometerstand van 150.000km.&nbsp;</p><p>Een garantie met een looptijd van 24 maanden is alleen dan mogelijk, als het voertuig niet ouder is als 8 jaar en maximaal 80.000km gelopen heeft.<br></p>'
hero-img: /assets/img/4f8495bb-c8d3-468d-b726-2f1c69c675e2.jpeg
banner_size: is-large
has_cta: false
is_hidden: false
fieldset: default
id: 1923793a-2755-4832-a070-a2ea8a1ee84d
