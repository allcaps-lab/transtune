title: Lease
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Chiptuning voor leaserijders</h1><p><em>U rijdt een leaseauto en overweegt om deze te chiptunen? Dat kan! Maar lees eerst eens even dit artikel met ‘must knows’ …</em></p><h2>Toegestaan of niet?</h2><p>De meeste leasemaatschappijen hebben in hun voorwaarden opgenomen dat tunen niet is toegestaan. Bent u werknemer en rijdt u een auto van de zaak? Dan kan het zijn dat uw werkgever in zijn ‘auto van de zaak-voorwaarden’ heeft opgenomen dat tunen niet is toegestaan. Oók als dat niet door de leasemaatschappij verboden wordt.</p><h2>Als u perse toch wilt tunen</h2><p>Gebruik géén powerbox.&nbsp;Leveranciers van zogenaamde powerboxen adviseren om zo’n box te gebruiken als u een leaseauto rijdt. Die box wordt voor de&nbsp;ECU&nbsp;van het&nbsp;motormanagement&nbsp;geïnstalleerd en manipuleert deze ECU. Onze ervaring is echter dat dit boxen niet (goed) werken en altijd tot problemen leiden.</p><p>VAG of BMW/Mini-motor?&nbsp;Als u een auto van de VAG-groep ((Volkswagen AG - Audi, Seat, Volkswagen en Skoda) of van BMW/Mini-groep rijdt, kan Transtune hun software zo maken dat deze niet te herkennen is. De dealer en/of leasemaatschappij ziet dus niet dat de auto getuned is.<strong></strong></p><h2>Bij een software-update</h2><p>Krijgt u auto een software-update bij de dealer, dan gaat de tuning die door ons is gedaan verloren. Wij bewaren echter elke tuning en kunnen hem daarna weer over de update heen schrijven. De meerprijs hiervoor is € 25 incl. btw.</p><p><em>Leaserijders met een auto uit de VAG- of BMW-groep kunnen hun auto door Transtune laten tunen zónder dat dit door de leasemaatschappij en/of dealer te herkennen is. Deze tuning kan na een eventuele software-update door de dealer weer teruggezet worden.</em></p>'
hero-img: /assets/img/4f8495bb-c8d3-468d-b726-2f1c69c675e2.jpeg
banner_size: is-large
has_cta: false
is_hidden: false
fieldset: default
id: 553f2d64-5fc6-432c-8477-efb155a8cc79
