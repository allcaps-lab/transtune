---
title: 'Automaat software'
has_cta: false
is_hidden: true
selector: chiptuning
fieldset: default
id: 39ce7044-e0c9-4b94-b286-7b65462a0cfc
---
<h1>Automaat chiptuning (DSG,DCT,SMG,PDK,ZF en S-tronic)</h1>
<p>Bij automaat chiptuning wordt de software van een automaatbak aangepast. Daar kunnen verschillende aanleidingen voor zijn:
</p>
<p><strong>Verwijderen koppelbegrenzing.</strong> Dit kan nodig zijn in combinatie met chiptuning zodat de versnellingsbak het extra koppel kan verwerken.
</p>
<p><strong>Rijgedrag verbeteren.</strong> Heeft u klachten over het schakelgedrag, dan kunnen we de momenten waarop de bak schakelt aanpassen. Dit geldt zowel voor het op- als terugschakelen. Tevens kunnen de schakelsnelheden worden geoptimaliseerd.
</p>
<p>Belangrijk: automaat chiptuning is géén oplossing voor een mechanisch probleem of storing in de Dual clutch tranmissie. Heeft u <u>problemen met uw automaat</u>? Neem dan <u>contact</u> met ons op voor een diagnose.
</p>
<h2>Werkwijze</h2>
<p>Wij passen onderstaande automaatbakken aan. De mogelijkheden zijn per type verschillend. <u>Informeer</u> bij ons naar de specifieke mogelijkheden of klik op het type transmissie en de stage
</p>
<p><strong><u></u></strong><br>
</p>
<p>DSG7 DQ200 (droge koppeling)
</p>
<p>DSG6 DQ250 (natte koppeling)
</p>
<p>DSG6 DQ400 (natte koppeling)
</p>
<p>DSG7 DQ500 (natte koppeling)
</p>
<p>DSG7 DL501 (natte koppeling)
</p>
<p>DSG7 DQ381 (natte koppeling)
</p>
<p>DSG7 DL382 (natte koppeling)
</p>
<p>ZF AL551 (tiptronic)
</p>
<p>ZF AL951(mulititronic)
</p>
<p>ZF 8HP<br>
</p>
<p>PDK DL501 (natte koppeling)
</p>
<p>PDK DL501G2 (natte koppeling)
</p>
<p>SMG 2 2000-2006 M3
</p>
<p>SMG 3 2005-2010 M5/M6
</p>
<p>DCT 2007-2013 M3
</p>
<p>DCT 2014-2018 M3/M4/M5/M6
</p>