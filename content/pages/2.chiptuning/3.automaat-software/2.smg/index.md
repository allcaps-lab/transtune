---
title: SMG
fieldset: default
id: ead6ea56-de52-4f84-a916-9e7d77cf694d
---
<p><strong>SMG 2 tuning stage 1</strong>
</p>
<p><strong>M3 & M3CSLSMG 2 2000-2006</strong>
</p>
<p>Voor alle SMG transmissies hebben wij software modificaties ontwikkeld.
</p>
<p>Hiermee presteer de auto beter en ervaart u veel meer souplesse.
</p>
<p>Onze SMG Performance software bestaat uit :
</p>
<p>• vastere en snellere verschuivingen in hogere drivelogic-modi<br> • verhoogde afnamecontrole RPM voor betere lanceringen<strong></strong>
</p>
<p><strong></strong>
</p>
<p><strong>€ 895,- incl btw in combinatie met chiptuning</strong><strong></strong>
</p>
<p><strong>€ 995,- incl btw losstaand</strong>
</p>
<p><strong><br></strong>
</p>
<p><strong>SMG 3 Tuning stage 1</strong>
</p>
<p><strong>M5 & M6 SMG 2005-2010</strong>
</p>
<p>Voor alle SMG transmissies hebben wij software modificaties ontwikkeld.
</p>
<p>Hiermee presteer de auto beter en ervaart u veel meer souplesse.
</p>
<p>Onze SMG Performance software bestaat uit :
</p>
<p>• Verhoogde soepelheid in rij- en sportstanden<br> • 4.100 RPM Launch Control in tegenstelling tot 1900 RPM-standaard<br> • Sneller preciezere verschuivingen in alle drivelogic-modi<br> • Updates voor de nieuwste overdrachtssoftware
</p>
<p><strong>€ 895,- incl btw in combinatie met chiptuning</strong><strong></strong>
</p>
<p><strong>€ 995,- incl btw losstaand</strong>
</p>
<p><strong></strong><i></i><u></u><sub></sub><sup></sup><del></del><br>
</p>
<p><strong><i></i><u></u><sub></sub><sup></sup><del></del><br></strong>
</p>