---
title: DCT
fieldset: default
id: 3091c49a-d154-4cb2-97de-6d2d2d924655
---
<p><a><strong>DCT Tuning stage 1</strong></a>
</p>
<p>DCT (Dual Clutch Transmssion) tuning performance software van 2007-2018 M3 & M4
</p>
<p>Voor alle DCT transmissies hebben wij software modificaties ontwikkeld.
</p>
<p>Hiermee presteer de auto beter en ervaart u veel meer souplesse.
</p>
<p>Onze DCT Performance software Bestaat uit :
</p>
<p>* De nieuwste updates op uw transmissie
</p>
<p>* Het geheugen van Drivelogic in de sportmodes blijft behouden
</p>
<p>* De auto start in Drive in plaats van S
</p>
<p>* Launch control Parameters zijn geoptimaliseerd en het toerental is ingesteld op 3500 RPM
</p>
<p>* Schakelt agressiever in de hoger stand van de drivelogic modus
</p>
<p>* Houdt de versnellingen langer vast afhankelijk van de drivelogic modus
</p>
<p>* minder onhandig en ongemakkelijk terug schakelen bij het stoppen
</p>
<p>* Sneller, nauwkeuriger en soepeler terug schakelen.
</p>
<p>* minder vertraging bij het inschakelen van de achteruit rij versnelling
</p>
<p><strong>€ 895,- incl btw in combinatie met chiptuning</strong><strong></strong>
</p>
<p><strong>€ 995,- incl btw losstaand</strong>
</p>
<hr>
<p><br>
</p>