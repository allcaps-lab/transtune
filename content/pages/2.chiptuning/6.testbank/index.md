title: Testbank
page_content:
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>De vermogenstestbank: meten = weten</h1><p><em>Het is belangrijk om voor en na het tunen de prestaties van een motor goed te meten. Dan doen we met een speciale vermogenstestbank.</em>&nbsp;Zo’n vermogenstestbank wordt gebruikt voor de optimale afstelling van uw auto.</p><h2>Specificaties</h2><p>Transtune beschikt over een 4x4 vermogenstestbank van het merk&nbsp;MAHA: de MSR 1000. Op deze rollenbank kunnen we testen tot 300 km/h en tot maximaal 1.400 pk. De testruimte is voorzien 30 kW aan ventilatoren voor de juiste koeling.</p>'
  -
    type: blue_background
    rich_content:
      -
        type: text
        text: '<h2>Vermogen meten bij Transtune</h2><p>Transtune heeft een speciaal ingerichte high-tech ruimte, die voorzien is van een vermogenstestbank. Vanuit de wachtruimte kunt u meekijken met die vermogensmeting. Via grote beeldschermen kunt u direct zien hoeveel pk’s u aan boord heeft.</p><p>Voor de maximale koeling en rijwind te realiseren kunnen we maximaal 4 ventilatoren aan zetten. Die hebben een totaal vermogen van 65 kW. Zo kunnen we zowel auto’s met de motor voorin als achterin maximaal testen.</p>'
  -
    type: white_background
    rich_content:
      -
        type: text
        text: '<h1>Voorwaarden</h1><p>We kunnen uw auto alleen testen als deze:&nbsp;<br></p><ul><li>Volledig schoon is (geen modder, sneeuw, ijs, e.d.;</li><li>Is voorzien van goede zomerbanden;</li><li>Goed onderhouden is.</li></ul><p>Goed onderhoud (olie, distributieriem etc) is belangrijk omdat tijdens een vermogensmeting gedurende ongeveer 5 minuten het uiterste van uw auto wordt gevraagd. Wij zijn daarom niet aansprakelijk voor schade als gevolg van de vermogensmeting.</p>'
      -
        type: simple_video
        video: 'https://www.youtube.com/watch?v=1UCWbJh86iA'
hero-img: /assets/img/transtune_bas_fransen_27.jpg
banner_size: is-large
subtitle: 'Verbeter de prestaties van uw auto!'
has_cta: true
cta_title: 'Wilt u de prestaties van uw auto verbeteren?'
cta_subtitle: 'Neem contact met ons en maak een afspraak!'
cta_link: de627bca-7595-429e-9b41-ad58703916d7
cta_text: Contact
menu: main_menu
is_hidden: false
selector: chiptuning
seo:
  title: 'Testbank Transtune - Test en verbeter het vermogen van uw auto'
fieldset: default
id: 982111a9-b2ef-4b36-9a2b-50da901c0b10
