title: 'Chiptuning Stage 1'
stage_features:
  -
    stage_options: 39a933d8-9879-424b-8620-852079cdb138
    available: true
  -
    stage_options: d4510ddc-d0ce-407b-9570-3f86139d0088
    available: true
  -
    stage_options: 0ebf6f60-b265-457b-aaa3-06dd1248b8c1
    available: true
  -
    stage_options: 55a64c19-69db-4ff2-9da9-2f13da340007
    available: false
  -
    stage_options: 6e5e6c7a-8e6f-42e1-ae07-dc7078e6564e
    available: false
duration: 60
extra: ''
id: 058472fd-a4a6-92f3-1a41-2b59b7db9138
