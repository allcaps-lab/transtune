title: 'Chiptuning Stage 1 plus'
stage_features:
  -
    stage_options: 39a933d8-9879-424b-8620-852079cdb138
    available: true
  -
    stage_options: d4510ddc-d0ce-407b-9570-3f86139d0088
    available: true
  -
    stage_options: 0ebf6f60-b265-457b-aaa3-06dd1248b8c1
    available: true
  -
    stage_options: c427c303-1870-4ea2-86c0-92e804cbf7bc
    available: true
  -
    stage_options: 55a64c19-69db-4ff2-9da9-2f13da340007
    available: true
  -
    stage_options: 6e5e6c7a-8e6f-42e1-ae07-dc7078e6564e
    available: true
duration: 180
extra: ''
id: 45ace6fc-d53a-1d16-3889-333ef8f9b3a0
