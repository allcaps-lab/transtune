const mix = require('laravel-mix');
require('laravel-mix-purgecss');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('./');
mix.setResourceRoot('../');

mix.webpackConfig({
    resolve: {
        alias: {
            '@': path.resolve('resources'),
            'vue$': 'vue/dist/vue.runtime.js',
            'vars': path.resolve('./resources/sass/_variables.scss')
        }
    },
});

/*
 |--------------------------------------------------------------------------
 | Mix Production
 |--------------------------------------------------------------------------
 */
if (mix.config.production) {
    mix.js('resources/js/browser-update.js', 'js')
        .js('resources/js/transtune.js', 'js')
        .js('resources/js/selector.js', 'js')
        .js('resources/js/quotation.js', 'js')
        .js('resources/js/map.js', 'js')
        .sass('resources/sass/transtune.scss', 'css')
        .purgeCss({
            content: ['**/*.html', '**/*.vue'],
            folders: ['layouts', 'partials', 'templates', 'resources/js']
        }).version();
}

/*
 |--------------------------------------------------------------------------
 | Mix Development
 |--------------------------------------------------------------------------
 */
if (!mix.config.production) {
    mix.js('resources/js/transtune.js', 'js')
    .js('resources/js/selector.js', 'js')
    .js('resources/js/quotation.js', 'js')
    .js('resources/js/map.js', 'js')
    .sass('resources/sass/transtune.scss', 'css')
}
