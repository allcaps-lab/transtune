{!! $xmlHeader !!}
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
@foreach ($data as $car)
    <url>
        <loc>{!! htmlspecialchars($car['loc']) !!}</loc>
        <lastmod>{!! htmlspecialchars($car['lastmod']) !!}</lastmod>
        <changefreq>{!! htmlspecialchars($car['changefreq']) !!}</changefreq>
        <priority>{!! htmlspecialchars($car['priority']) !!}</priority>
    </url>
@endforeach
</urlset>