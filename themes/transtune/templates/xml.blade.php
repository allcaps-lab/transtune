{!! $xmlHeader !!}
<feed>
@foreach ($data as $car)
    <car>
        <brand>{!! htmlspecialchars($car['brand']) !!}</brand>
        <model>{!! htmlspecialchars($car['model']) !!}</model>
        <generation>{!! htmlspecialchars($car['generation']) !!}</generation>
        <type>{!! htmlspecialchars($car['type']) !!}</type>
        <url>{{ $car['url'] }}</url>
    </car>
@endforeach
</feed>