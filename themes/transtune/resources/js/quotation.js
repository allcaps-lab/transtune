import Vue from 'vue';
import Quotation from './quotation/Quotation';
import store from "./quotation/store";
import i18n from "./quotation/i18n";

if (process.env.NODE_ENV === 'production') {
  Vue.config.devtools = false
  Vue.config.debug = false
  Vue.config.silent = true
  Vue.config.productionTip = false;
}

new Vue({
  render: h => h(Quotation),
  store,
  i18n
}).$mount('#quotation-tool')