export function navMenu() {
  // Get all "navbar-burger" elements
  var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {
      // Add a click event on each of them
      $navbarBurgers.forEach(function ($el) {
          $el.addEventListener('click', function () {
              // Get the target from the "data-target" attribute
              var target = $el.dataset.target;
              var $target = document.getElementById(target);
              // Toggle the class on both the "navbar-burger" and the "navbar-menu"
              $el.classList.toggle('is-active');
              $target.classList.toggle('is-active');
              document.getElementsByTagName('html')[0].classList.toggle('is-expanded');

              // is there still a slideout active
              let folded = document.querySelector('.folded');
              if (folded && !$target.classList.contains('is-active')) {
                toggleSlideOut(folded);
              }
          });
      });
  }
}

function toggleSlideOut($target) {
  $target.classList.toggle("folded");
  $target.classList.toggle('slide-out');
  document.getElementsByClassName('navbar-top')[0].classList.toggle('has-children')
}

export function isMobile() {
  return window.innerWidth <= 1215;
}

export function mobileSlideOut() {
  const $slideOut = document.querySelectorAll('.has-children');
  if ($slideOut.length > 0 ) {
    $slideOut.forEach(function($el) {
      const target = $el.dataset.target;
      const $target = document.querySelector('div[data-parent='+target+']');
      $el.addEventListener('click', function(e) {
        if (isMobile()){
          e.preventDefault();
          toggleSlideOut($target);
        }
      });
    });
  }

  let $rollback = document.querySelectorAll('.rollback');
  if ($rollback.length > 0) {
    $rollback.forEach(function($el) {
      let $target = $el.parentNode;
      $el.addEventListener('click', function() {
        toggleSlideOut($target);
      })
    })
  }
}