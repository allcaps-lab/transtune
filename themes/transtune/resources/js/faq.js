export default function() {
  let $toggles = document.getElementsByClassName('toggle');
  for (let i = 0; i < $toggles.length; i++) {
    $toggles[i].addEventListener('click', function(e) {
      if (e.target.classList.contains('toggle-hit')) {
        if (this.classList.contains('is-active')) {
          this.classList.remove('is-active');
        } else {
          this.classList.add('is-active');
        }
      }
    })
  }
}