import { navMenu, mobileSlideOut } from './nav';
import faq from "./faq";
import Glide from '@glidejs/glide'

document.addEventListener('DOMContentLoaded', function () {
  navMenu();
  mobileSlideOut();
  faq();
});



// Home page slide
if (document.getElementById('home__glide')) {
  const timing = 5000;
  let start,
      timingEl,
      paused;

  const homeGlide = new Glide('#home__glide', {
    type: 'slider',
    focusAt: 'center',
    autoplay: timing,
    animationTimingFunc: 'ease-in-out',
    perView: 1
  })

  const homeGlideProgress = () => {
    if (!paused) {
      const current = new Date().getTime();
      const percentage = (current - start) / (timing / 100)
      if (percentage - timingEl['value'] >= 0.5) {
        timingEl['value'] = percentage;
      }
    }
    requestAnimationFrame(homeGlideProgress);
  }

  homeGlide.on('build.before', function() {
    timingEl = document.querySelector(`.glide__countdown[data-glide-countdown="${homeGlide.index}"]`);
    document.querySelector(`.glide__item[data-glide-item="${homeGlide.index}"]`).classList.add('is-active')
    timingEl.setAttribute('value', 0);
    start = new Date().getTime();
    requestAnimationFrame(homeGlideProgress)
  })

  homeGlide.on('run.before', function() {
    document.querySelector(`.glide__item[data-glide-item="${homeGlide.index}"]`).classList.remove('is-active');
  })

  homeGlide.on('run', function() {
    document.querySelector(`.glide__item[data-glide-item="${homeGlide.index}"]`).classList.add('is-active');
    timingEl = document.querySelector(`.glide__countdown[data-glide-countdown="${homeGlide.index}"]`);
    timingEl.setAttribute('value', 0);
    start = new Date().getTime();
    requestAnimationFrame(homeGlideProgress)
  });

  homeGlide.on('pause', function() {
    paused = true;
  })

  homeGlide.on('play', function() {
    paused = false;
  })

  homeGlide.mount();
}

const timing = 5000;

if (document.getElementsByClassName('carousel').length > 0) {
  const contentCarousel = new Glide('.carousel', {
    type: 'slider',
    focusAt: 'center',
    autoplay: timing,
    animationTimingFunc: 'ease-in-out',
    perView: 1
  })

  contentCarousel.mount();
}