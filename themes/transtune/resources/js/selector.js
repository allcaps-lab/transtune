import Vue from 'vue';
import Selector from './selector/Selector.vue'
import i18n from "./quotation/i18n";

if (process.env.NODE_ENV === 'production') {
  Vue.config.devtools = false
  Vue.config.debug = false
  Vue.config.silent = true
  Vue.config.productionTip = false;
}

const subject = document.getElementById('selector').dataset.subject;

new Vue({
  data: {
    subject: subject
  },
  i18n,
  render: h => h(Selector),
}).$mount('#selector')