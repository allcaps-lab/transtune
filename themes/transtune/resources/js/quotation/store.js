import Vue from 'vue';
import Vuex from 'vuex';
import moment from 'moment';
Vue.use(Vuex);

const store = new Vuex.Store({
  // Data
  state: {
    visible: false,
    checked: [],
    activities: [],
    states: {},
    appointment: false,
    carData: {
      foreign: false,
      license: undefined,
      mileage: undefined,
    },
    personData: {
      firstname: undefined,
      lastname: undefined,
      email: undefined,
      phone: undefined,
    },
    title: undefined,
    replacement: false,
    quote: false,
    replacements: [],
  },
  // Getters, see vuex
  getters: {
    options: state => {
      const options = JSON.parse(JSON.stringify(state.activities.filter(activity => activity.type === 'option')));
      return options.map(activity => {
        if (activity.included) {
          activity.price = 0;
          activity.auto_selected = true;
        }
        return activity;
      });
    },
    totalPrice: state => {
      return state.checked.reduce((accumulator, id) => {
        const activity = state.activities.find(activity => activity.id === id);
        const price = Number(activity.price);
        if (activity && Number.isInteger(price)) {
          return accumulator += price
        }
        return accumulator;
      }, 0)
    },
    activities: state => {
      return state.activities.filter(activity => activity.type === 'activity')
    },
    // These are the options for the first step
    step1Options: state => {
      return state.checked.filter(id => id.includes('option'));
    },
    checkedActivities: state => {
      return state.checked.filter(id => !id.includes('option'));
    },
    appointment: state => {
      return state.appointment ? moment(state.appointment).format("D MMMM YYYY") : false;
    }
  },
  // Mutations, see vuex site
  mutations: {
    setVisible(state, visibility) {
      state.visible = visibility;
    },
    addToChecked(state, id) {
      const index = state.checked.indexOf(id);
      if (index < 0) {
        state.checked.push(id);
      }
    },
    removeFromChecked(state, id) {
      const index = state.checked.indexOf(id);
      if (index > -1) {
        state.checked.splice(index, 1);
      }
    },
    setActivities(state, activities) {
      let auto_selected = activities
        .filter(activity => activity.auto_selected == true)

        state.activities = activities.map(activity => {
        activity.included = false;
        return activity;
      });

      auto_selected.forEach(activity => {
        state.checked.push(activity.id);
      })
    },
    setStates(state, states) {
      state.states = states;
    },
    setAppointment(state, value) {
      state.appointment = value;
    },
    setCarData(state, {type, data} ) {
      state.carData[type] = data;
    },
    setPersonData(state, {type, data} ) {
      state.personData[type] = data;
    },
    setReplacement(state, value) {
      state.replacement = value;
    },
    setTitle(state, value) {
      state.title = value;
    },
    setReplacements(state, value) {
      state.replacements = value;
    },
    setQuote(state, value) {
      state.quote = value;
    },
    setIncluded(state, activities) {
      // Restore all old free activities to non free
      state.activities = state.activities
        .map(activity => {
          activity.included = false;
          return activity;
        });

      // Only
      state.activities = state.activities
        .map(activity => {
          if (activities.indexOf(activity.id) > -1) {
            activity.included = true;
          }
          return activity;
        })
    },
    setStagePrice(state, price) {
      const activity = state.activities.find(activity => activity.id === 'chiptuning_options/stage-tuning')
      if (activity) {
        activity.price = price;
      }
    }
  },
});

export default store;