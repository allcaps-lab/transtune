import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

const messages = {
  en: {
    send: 'Send',
    next: 'Next',
    skip: "Skip",
    required: 'is required',
    carData: {
      license: 'License Plate | Ex:11-AAA-1',
      mileage: 'Mileage',
      foreign: "Not from the Netherlands?"
    },
    contact: {
      license: 'License Plate | Ex:11-AAA-1',
      mileage: 'Mileage',
      firstName: 'First Name | Your first name',
      lastname: 'Last Name | Your last name',
      email: 'Email | Your E-mail',
      phone: 'Phone | Your phone number',
      replacement: 'Replacement?',
      title: ''
    },
    selector: {
      brand: 'Brand',
      model: 'Model',
      generation: 'Generation',
      type: 'Type',
      action: 'Make Appointment',
      info: "Info"
    },
    quote: 'Receive quote',
    replacement: {
      transport: 'Replacement'
    }
  },
  nl: {
    send: 'Verstuur',
    next: 'Volgende',
    skip: "Sla over",
    required: 'is vereist',
    carData: {
      license: 'Kentekenplaat | Bijv.11-AAA-1',
      mileage: 'Kilometerstand | Stand',
      foreign: 'Niet uit Nederland?'
    },
    contact: {
      license: 'Kentekenplaat | 1-ABC-003',
      mileage: 'Kilometerstand | Stand',
      firstname: 'Voornaam | Uw voornaam',
      lastname: 'Achternaam | Uw achternaam',
      email: 'Email | Uw E-mail',
      phone: 'Telefoonnummer | Uw telefoonnummer',
      replacement: 'Vervangend vervoer?',
      title: 'Aanhef'
    },
    selector: {
      brand: 'Merk',
      model: 'Model',
      generation: 'Bouwjaar',
      type: 'Type',
      action: 'offerte/afspraak',
      info: "info"
    },
    quote: 'Offerte ontvangen',
    replacement: {
      transport: 'Vervangend vervoer'
    }
  }
}

export default new VueI18n({
  locale: window.ACwebsiteLocale,
  messages
});