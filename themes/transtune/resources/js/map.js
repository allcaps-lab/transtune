let map;

if (typeof window.mapboxgl == 'undefined') {
    console.error('Mapbox not loaded');
} else {
    const latLong = [5.811969,51.452175];
    mapboxgl.accessToken = 'pk.eyJ1IjoibWFyay1ob29nIiwiYSI6ImNqdTh1MTl5cTBhcDM0ZXBlNzAyazJhOGoifQ.z49mg38kRxeRAFmWBGKoHw';
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mark-hoog/cjqiibkez1slm2rrr4ilb3d0r',
        center: latLong,
        zoom: 15
    });

    let popup = new mapboxgl.Popup({closeOnClick: false})
        .setLngLat(latLong)
        .setHTML(`<h1>Transtune</h1><a href="https://maps.google.com/maps?daddr=${latLong[1]},${latLong[0]}&amp;ll=" id="mapOpenNavigation">Ga naar navigatie</a>`)
        .addTo(map);

    map.on('load', function() {
        document.getElementById("mapOpenNavigation").addEventListener('click', function(e) {
                e.preventDefault();
                if /* if we're on iOS, open in Apple Maps */
                    ((navigator.platform.indexOf("iPhone") != -1) ||
                    (navigator.platform.indexOf("iPad") != -1) ||
                    (navigator.platform.indexOf("iPod") != -1))
                    window.open(`maps://maps.google.com/maps?daddr=${latLong[1]},${latLong[0]}&amp;ll=`);

                else /* else use Google */
                    window.open(`https://maps.google.com/maps?daddr=${latLong[1]},${latLong[0]}&amp;ll=`);
        })
    })
}